import argparse

from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QComboBox
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtGui import QFont
from PyQt5.Qt import Qt, QSizePolicy, QLocale

import numpy as np

from treex.tree import Tree
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.visualization.options import assign_random_color_to_tree, assign_random_nodescale_to_tree, assign_random_linewidth_to_tree

from treex.app.options import all_node_variable_names
from treex.app.tree_editor import TreeEditor
from treex.app.probability_tree import valid_local_probas_sum_to_one, compute_global_probas


class PropbabilityTreeEditor(TreeEditor):

    def __init__(self, tree, parent=None):
        self.proba_layout = QVBoxLayout()
        self.proba_pane = QWidget()

        self.proba_attribute = ""

        self.proba_label = QLabel("Probability Attribute")
        self.proba_attribute_edit = QComboBox()
        self.proba_validity_button = QPushButton('Check local validity')
        self.proba_global_button = QPushButton('Compute global probabilities')

        super().__init__(tree, parent)

        self.initialize_proba_pane()

    def initialize_proba_pane(self):
        self.proba_layout.setContentsMargins(0, 0, 0, 0)

        font = QFont()
        self.proba_label.setFont(QFont(font.defaultFamily(), 16, QFont.Bold))
        self.proba_label.setAlignment(Qt.AlignCenter)
        self.proba_label.setVisible(True)
        self.proba_layout.addWidget(self.proba_label)

        self.proba_attribute_edit.clear()
        self.proba_attribute_edit.addItems([""])
        self.proba_attribute_edit.setCurrentText(self.proba_attribute)
        self.proba_attribute_edit.currentIndexChanged.connect(self.refresh_proba)
        self.proba_layout.addWidget(self.proba_attribute_edit)

        self.proba_validity_button.clicked.connect(self.check_proba_validity)
        self.proba_layout.addWidget(self.proba_validity_button)

        self.proba_global_button.clicked.connect(self.compute_global_proba)
        self.proba_layout.addWidget(self.proba_global_button)

        self.proba_pane.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.proba_pane.setLayout(self.proba_layout)
        self.visualization_layout.addWidget(self.proba_pane)

    def refresh_variables(self):
        super().refresh_variables()

        tree_scalar_attributes = all_node_variable_names(self.tree, variable_type=self.variable_type, type='scalar')
        tree_scalar_attributes = [v for v in tree_scalar_attributes if v not in self.hidden_attributes]

        self.proba_attribute_edit.blockSignals(True)
        self.proba_attribute_edit.clear()
        self.proba_attribute_edit.addItems([""] + tree_scalar_attributes)
        self.proba_attribute_edit.blockSignals(False)
        if self.proba_attribute in [""] + tree_scalar_attributes:
            self.proba_attribute_edit.setCurrentText(self.proba_attribute)
        else:
            self.proba_attribute_edit.setCurrentText("")
        self.proba_attribute_edit.setEnabled(len(tree_scalar_attributes) > 0)

        self.refresh_proba()

    def refresh_proba(self):
        self.proba_attribute = self.proba_attribute_edit.currentText()
        self.proba_validity_button.setEnabled(self.proba_attribute != "")
        if self.proba_attribute+"_valid" in self.tree.get_attribute().keys():
            all_valid = np.all([t.get_attribute(self.proba_attribute+"_valid")=="True" for t in self.tree.list_of_subtrees()])
            self.proba_global_button.setEnabled(all_valid)
        else:
            self.proba_global_button.setEnabled(False)

    def check_proba_validity(self):
        if self.proba_attribute != "":
            valid_local_probas_sum_to_one(self.tree,
                                          local_proba_name=self.proba_attribute,
                                          attribute_valid_name=self.proba_attribute+"_valid",
                                          exact_flag=False,
                                          epsilon=0.0001)

            if not self.proba_attribute+"_valid" in self.hidden_attributes:
                self.hidden_attributes.append(self.proba_attribute+"_valid")

            self.refresh_variables()
            self.refresh_plot()

            all_valid = np.all([t.get_attribute(self.proba_attribute+"_valid")=="True" for t in self.tree.list_of_subtrees()])
            self.proba_global_button.setEnabled(all_valid)

    def compute_global_proba(self):
        if self.proba_attribute != "":
            compute_global_probas(self.tree, self.proba_attribute)

            # if not "global_"+self.proba_attribute in self.hidden_attributes:
            #     self.hidden_attributes.append("global_"+self.proba_attribute)

            self.refresh_variables()
            self.refresh_plot()
            if self.node_id is not None:
                self.display_attributes()

    def add_child(self):
        super().add_child()
        self.check_proba_validity()
        self.refresh_plot()

    def edit_attribute(self):
        super().edit_attribute()
        self.check_proba_validity()
        self.refresh_plot()


def main():
    """
    Returns
    -------
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-H', '--hidden-attributes', default=['color_plotgraph','nodescale_plotgraph','linewidth_plotgraph'], nargs='+', help='List of attribute names to hide from the edition panel')
    parser.add_argument('-c', '--highlight-color', help='Color used to highlight the current node', default='chartreuse')

    args = parser.parse_args()

    tree = Tree()
    assign_random_color_to_tree(tree,['grey'])
    assign_random_nodescale_to_tree(tree,[1])
    assign_random_linewidth_to_tree(tree,[1])
    tree.get_property(compute_level=2)

    QLocale.setDefault(QLocale.c())

    app = QApplication([])

    window = QMainWindow()
    window.setWindowTitle("Probability Tree Editor")

    editor = PropbabilityTreeEditor(tree)
    editor.set_hidden_attributes(args.hidden_attributes)
    editor.highlight_color = args.highlight_color
    window.setCentralWidget(editor)

    window.show()

    app.exec_()


if __name__ == '__main__':
    main()

# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.tree
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#           Florian Ingels <florian.ingels@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
Called by this module: copy.deepcopy()

This module allows the manipulation of the Tree structure used in the
treex library

- File author(s): Romain Azais <romain.azais@inria.fr>
- File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>, Florian Ingels <florian.ingels@inria.fr>

"""

from copy import deepcopy
from collections import defaultdict
import itertools

class Tree(object):
    """
    The main structure of the treex library

    Attributes
    ----------
    my_id: int
        A unique id attributed to the tree

    my_children: list
        The Tree class is a recursive structure, children of a node as given
        as Tree class objects. Default is an empty list

    my_parent: Tree
        Default is None. If the tree is a substructure of another tree,
        my_parent is the Tree class object such that the original tree belongs
        to the larger tree's children
    """

    __bottomup_properties = ['height','size','outdegree','number_of_children','number_of_leaves','strahler_number']
    __topdown_properties = ['depth' , 'index_among_siblings']

    __slots__ = 'my_id', 'my_children', '__my_properties', '__my_attributes', 'my_parent'

    __new_id_iter__ = itertools.count()

    def __init__(self):
        self.my_id = next(Tree.__new_id_iter__)
        self.my_children = []
        self.__my_properties = {}
        self.__my_attributes = {}
        self.my_parent = None

# --------------------------------------------------------------

    def __str__(self):
        """
        This method simply allows the use of print(...) function for Tree class
        objects

        See Also
        --------
        treeprint(...)

        """
        return self.__string_tree_id()

# --------------------------------------------------------------

    def __string_tree_id(self,k=0):
        s=k*"\t"+str(self.my_id)+"\n"
        for c in self.my_children:
            s+=c.__string_tree_id(k+1)
        return s

    def __string_tree_one_property(self , prop = None , k=0):
        if prop in self.__my_properties.keys():
            s=k*"\t"+str(self.__my_properties[prop])+"\n"
        else:
            s=k*"\t"+'*'+"\n"
        for c in self.my_children:
            s+=c.__string_tree_one_property(prop,k+1)
        return s

    def __concatenate_string_tree_one_property(self):
        if self.__my_properties=={}:
            s=self.__string_tree_one_property(prop=None,k=0)
        else:
            s = ''
            for key in self.__my_properties.keys():
                s+=str(key)+'\n'
                s+=self.__string_tree_one_property(prop=key,k=0)+'\n'
        return s

    def __string_tree_all_properties(self,k=0):
        str_display=''
        compteur=0
        s=''
        for key in self.__my_properties.keys():
            if compteur>0:
                str_display+=' / '
            str_display+=str(self.__my_properties[key])
            compteur+=1
            s=k*"\t"+str_display+"\n"
        for c in self.my_children:
            s+=c.__string_tree_all_properties(k+1)
        return s

    def treeprint(self , ptype = '' , prop = None):
        """
        This method allows various ways to print a tree

        Parameters
        ----------
        ptype: str, optional
            - if 'all_properties':  each node in the print is symbolized with a list of values, that represents all of its properties
            - if 'one_property': only the property given with prop is printed
            - if 'concatenate': the tree is printed several times, each time according to a different property; all properties are printed that way
            - any other string works like the standard function print(...) Default case too

        prop: str, optional
            only used if ptype = 'one_property', and print the property
            named after prop. Default is None and print '*' symbols for
            the nodes

        Returns
        -------
        None

        """
        if ptype=='all_properties':
            s='Property order :\n'
            n=len(self.__my_properties.keys())
            i=0
            for key in self.__my_properties.keys():
                i+=1
                if i==n:
                    s+=str(key)
                else:
                    s+=str(key)+' / '
            s+='\n'
            print(s)
            print(self.__string_tree_all_properties())
        elif ptype=='concatenate':
            print(self.__concatenate_string_tree_one_property())
        elif ptype=='one_property':
            print(self.__string_tree_one_property(prop))
        else:
            print(self.__string_tree_id())

# --------------------------------------------------------------

    def get_attribute(self , attribute = None): ### Romain ### Jan 31 2020
        """
        This method allows access to the attributes of the tree.

        Parameters
        ----------
        attribute: str, optional
            Default is None. In this case, all previously existing
            properties of the tree are returned (see below)

        Returns
        -------
            - If attribute is set to None, a dictionary of all already existing
            properties is returned.

            - If a single property is provided, its value is returned.

        Warnings
        --------
        If a property is given, and if it does not exist in the tree, and if
        it is not one of the topological attributes discussed above, then the
        method raises an error

        See Also
        --------
        add_attribute_to_id(...), erase_attribute(...), erase_attribute_at_id(...)
        """

        if attribute == None:
            return self.__my_attributes

        elif attribute in self.__my_attributes.keys():
            return self.__my_attributes[attribute]
        
        else:
            return None

# --------------------------------------------------------------

    def __compute_bottomup_property(self , update_only = False):
        if self.my_children == []:
            self.__my_properties['number_of_children'] = 0
            self.__my_properties['number_of_leaves'] = 1
            self.__my_properties['height'] = 0
            self.__my_properties['size'] = 1
            self.__my_properties['outdegree'] = 0
            self.__my_properties['strahler_number'] = 1

        else:
            for child in self.my_children:
                child_prop = child.get_property(compute_level=0) # Read current properties of child
                if update_only and 'number_of_children' in child_prop and 'number_of_leaves' in child_prop and 'height' in child_prop and 'size' in child_prop and 'outdegree' in child_prop and 'strahler_number' in child_prop:
                    compute = False
                else:
                    compute = True

                if compute:
                    child.__compute_bottomup_property(update_only)

            children_prop = [c.get_property(compute_level=0) for c in self.my_children]
            self.__my_properties['number_of_children'] = len(self.my_children)
            self.__my_properties['number_of_leaves'] = sum([a['number_of_leaves'] for a in children_prop])
            self.__my_properties['height'] = max([a['height'] for a in children_prop]) + 1
            self.__my_properties['size'] = sum([a['size'] for a in children_prop]) + 1
            self.__my_properties['outdegree'] = max([a['outdegree'] for a in children_prop] + [len(self.my_children)])

            l_sn = [a['strahler_number'] for a in children_prop]
            if len([x for x in l_sn if x == max(l_sn)]) < 2:
                self.__my_properties['strahler_number'] = max(l_sn)
            else:
                self.__my_properties['strahler_number'] = max(l_sn) + 1

    def __compute_topdown_property(self , update_only = False):
        if self.my_parent is None:
            self.__my_properties['depth'] = 0
            self.__my_properties['index_among_siblings'] = None
        else:

            parent_prop = self.my_parent.get_property(compute_level=0)

            if update_only and 'depth' in parent_prop and 'index_among_siblings' in parent_prop:
                compute = False
            else:
                compute = True

            if compute:
                self.my_parent.__compute_topdown_property(update_only)

            self.__my_properties['depth'] = 1 + self.my_parent.get_property('depth', compute_level=0)
            self.__my_properties['index_among_siblings'] = [self.my_parent.my_children[i].my_id for i in range(len(self.my_parent.my_children))].index(self.my_id)

# --------------------------------------------------------------

    def __rebuild_increment_method(self , current_tree , mapping ):
        if current_tree == None:
            current_tree = Tree()
            current_tree._Tree__my_properties = deepcopy(self._Tree__my_properties)
            current_tree._Tree__my_attributes = deepcopy(self._Tree__my_attributes)
            mapping = {current_tree.my_id : self.my_id}

        for child in self.my_children:
            t = Tree()
            t._Tree__my_properties = deepcopy(child._Tree__my_properties)
            t._Tree__my_attributes = deepcopy(child._Tree__my_attributes)

            mapping[ t.my_id ] = child.my_id

            current_tree.add_subtree(t)

            child.__rebuild_increment_method(t , mapping)

        return {'tree':current_tree,'mapping':mapping}

    def rebuild(self):
        """
        This method rebuild the Tree with new ids and
        returns the mapping (old ids,new ids)
        """
        return self.__rebuild_increment_method(None , None)

    def copy(self):
        """
        This methods creates a new Tree class object, with exactly the same
        structure; if any properties and attributes were computed for the original
        tree, they are conserved. Only the ids of the tree and his subtrees are changed
        This is not a shallow copy

        Returns
        -------
        Tree
            A copy of the tree
        """
        out = self.rebuild()
        return out['tree']

# --------------------------------------------------------------

    def get_property(self, prop = None, compute_level = 1):
        """
        This method allows access to the properties of the tree.

        Parameters
        ----------
        prop: str, optional
            name of the property
            - Default is None. In this case, all properties of the tree
            are returned (see below)
            - topological properties are computed if compute_level is 2
            or if compute_level is 1 and all the topological_attributes
            have not been already computed

        compute_level: int
            if compute_level is 2, the property is computed even if existing
            if compute_level is 1, the property is computed only if not existing
            if compute_level is 0, the property is only read
            Default is 1

        Returns
        -------
            - If prop is set to None, a dictionary of all properties is returned.

            - If a single property is provided, its value is returned.

            - If the property does not belong to the tree properties,
            None is returned.

        """

        ### 1 ### Computation step
        if compute_level == 2:
            if prop in self.__bottomup_properties:
                self.__compute_bottomup_property(update_only=False)
            elif prop in self.__topdown_properties:
                self.__compute_topdown_property(update_only=False)
            elif prop == None:
                self.__compute_bottomup_property(update_only=False)
                self.__compute_topdown_property(update_only=False)

        elif compute_level == 1:
            if prop in self.__bottomup_properties:
                self.__compute_bottomup_property(update_only=True)
            elif prop in self.__topdown_properties:
                self.__compute_topdown_property(update_only=True)
            elif prop == None:
                self.__compute_bottomup_property(update_only=True)
                self.__compute_topdown_property(update_only=True)

        ### 2 ### Read step
        if prop == None:
            return self.__my_properties
        else:
            if prop in self.__my_properties.keys():
                return self.__my_properties[prop]
            else:
                print('Requested property does not belong to the tree properties')
                return None

# --------------------------------------------------------------

    def list_of_ids(self):
        """
        Returns the list of ids of the tree and all of its descendance
        """
        liste = [self.my_id]
        for child in self.my_children:
            liste += child.list_of_ids()
        return liste

    def dict_of_ids(self):
        """
        Returns a dictionary which keys are the ids and values are the corresponding properties
        and attributes together with parent_id and children_id
        """
        dico_node = {}
        if self.my_parent is None:
            dico_node.update({'parent_id' : None})
        else:
            dico_node.update({'parent_id' : self.my_parent.my_id})

        dico_node.update({'children_id' : [ self.my_children[i].my_id for i in range(len(self.my_children))]})

        dico_prop = self.get_property(compute_level = 1)
        dico_attr = self.get_attribute()

        dico_node.update({'properties':dico_prop})
        dico_node.update({'attributes':dico_attr})
        dico = {self.my_id : dico_node}
        
        for child in self.my_children:
            dico.update( child.dict_of_ids() )
        return dico

    def list_of_subtrees(self):
        """
        Returns the list of subtrees (as Tree class objects) of the tree and all of its descendance
        """
        liste=[self]
        for child in self.my_children:
            liste+=child.list_of_subtrees()
        return liste

    def subtree(self,node=None):
        """
        Returns the subtree rooted at node
        """
        if node==None:
            return self
        else:
            for s in self.list_of_subtrees():
                if s.my_id == node:
                    out = s
            return out

# --------------------------------------------------------------

    def subtrees_sorted_by(self, properties=None, attributes=None):
        """
        This method returns a nested dictionary of all subtrees of the tree, sorted
        by their value of the provided properties and attributes.

        :param properties: string or list of strings, optional
            Default is None. If one or several properties are provided, they must be chosen among the computable properties.
        :param attributes: string or list of strings, optional
            Default is None. If one or several attributes are provided, they must have been added in the tree before.
        :return: dict
        """

        # ----------------- To be rewritten in a more efficient way -----------------
        if properties is not None:
            if not isinstance(properties, list):
                props = [properties]
            else:
                props=properties
            for pro in props:
                if pro in self.__bottomup_properties:
                    self.__compute_bottomup_property(update_only=True)
                elif pro in self.__topdown_properties:
                    self.__compute_topdown_property(update_only=True)
        # ---------------------------------------------------------------------------

        else:
            props = []

        if attributes is not None:
            if not isinstance(attributes, list):
                attrs = [attributes]
            else:
                attrs=attributes
        else:
            attrs = []

        def recursive_defaultdict():
            return defaultdict(recursive_defaultdict)

        dico = recursive_defaultdict()

        ids = self.dict_of_ids()

        for id in ids:
            prop = ids[id]['properties']
            attr = ids[id]['attributes']

            my_command = 'dico'
            for pro in props:
                my_command += '[' + str(prop[pro]) + ']'
            for attribute in attrs:
                if isinstance(attr[attribute],str):
                    a='"{}"'.format(attr[attribute])
                else:
                    a=attr[attribute]
                my_command += '[' + str(a) + ']'
            if eval(my_command + '=={}'):
                my_command += '=[self.subtree(' + str(id) + ')]'
            else:
                my_command += '.append(self.subtree(' + str(id) + '))'

            exec(my_command)

        def default_to_regular(d):
            if isinstance(d, defaultdict):
                d = {k: default_to_regular(v) for k, v in d.items()}
            return d

        return default_to_regular(dico)

# --------------------------------------------------------------

    def is_isomorphic_to(self,tree,tree_type='unordered',mapping=False):
        """
        The methods returns a boolean and test if two tree are isomorphic,
        according to a specific relation of equivalence.

        Parameters
        ----------
        tree: Tree
            The tree to compare with

        tree_type: str, optional
            The relation of equivalence considered for comparing the trees
            Possibles values are 'ordered' and 'unordered'
            The latter is the default value

        mapping: bool
            If True, and if the trees are isomorphic, the function
            returns the mapping between self and tree

        Returns
        -------
        bool
            True if and only if the two trees are isomorphic

        tuple
            (bool, mapping dict) if mapping = True. If the trees are not isomorphic, the mapping dict is set to None.

        Notes
        -----
        Two trees are isomorphic, if, basically, one can map the nodes and
        edges of the first tree to the second. Two nodes are mapped if their
        children are the same; the order of appearance of children is either
        important (ordered class), either not important (unordered class)

        References
        ----------
        Example 3.2 and Theorem 3.3 in AHO, HOPCROFT, AND ULLMAN "The Design and Analysis of Computer Algorithms",
        1st ed. Addison-Wesley Longman Publishing Co., Inc., Boston, MA, USA, 1974
        """

        list_subtrees_self = self.subtrees_sorted_by('height')
        list_subtrees_tree = tree.subtrees_sorted_by('height')
        h_self = self.get_property('height')
        h_tree = tree.get_property('height')

        if h_self != h_tree:
            return False

        else:

            compteur=0

            # Height 0
            for leaf in list_subtrees_self[0]+list_subtrees_tree[0]:
                leaf.__my_attributes['isom_'+tree_type] = compteur

            compteur+=1

            # Height h
            for h in range(1,h_self+1):
                subtrees_h=list_subtrees_self[h] + list_subtrees_tree[h]

                seq=[]
                for i in range(len(subtrees_h)):
                    subtree_h_i=subtrees_h[i]
                    
                    list_labels_subtrees=[]
                    for child in subtree_h_i.my_children:
                        list_labels_subtrees.append(child.__my_attributes['isom_'+tree_type])

                    if tree_type=='unordered':
                        list_labels_subtrees.sort()

                    if list_labels_subtrees in seq:
                        indice=seq.index(list_labels_subtrees)
                        subtree_h_i.__my_attributes['isom_'+tree_type]=subtrees_h[indice].__my_attributes['isom_'+tree_type]

                    else:
                        subtree_h_i.__my_attributes['isom_'+tree_type]=compteur
                        compteur+=1

                    seq.append(list_labels_subtrees)

            bool_isom = self.get_attribute('isom_'+tree_type) == tree.get_attribute('isom_'+tree_type)
    
            if mapping:
                if bool_isom:
                    mapp = self.__mapping(tree,tree_type = tree_type)
                    return (True, mapp)
                else:
                    return (False, None)
            else:
                return bool_isom

    def __mapping(self,tree,tree_type):
        ### Assumes that self and tree are isomorphic
        ### and attributes 'isom_'+tree_type have been computed
        attr = 'isom_'+tree_type

        if tree_type == 'unordered':
            self.sort_subtrees_by_attribute(attr)
            tree.sort_subtrees_by_attribute(attr)

        dico = {}
        self.__build_mapping(tree,dico)

        return dico

    def __build_mapping(self,tree,dico):
        ### Recursive function to create the mapping between self and tree
        dico[self.my_id] = tree.my_id

        for i in range(len(self.my_children)):
            ch1 = self.my_children[i]
            ch2 = tree.my_children[i]

            ch1.__build_mapping(ch2,dico)


# --------------------------------------------------------------
#
# Basic Tree Edit Operations
#
# --------------------------------------------------------------

    def add_subtree(self,subtree,index=None):
        """
        Add the given tree as a child of the main tree

        Parameters
        ----------
        subtree: Tree

        index: int, optional
            Index of the subtree to be added in the list of the siblings.

        Returns
        -------
        None

        Warning
        -------
        The given subtree will not be copied and will therefore be a part of the main tree. If you want to modify later the subtree without modifying the main tree, you should consider to use add_subtree_to_id(...) instead.
        
        See Also
        --------
        add_subtree_to_id(...), delete_subtree(...), delete_subtree_at_id(...)

        """

        if index == None:
            self.my_children.append(subtree)
        else:
            self.my_children.insert(index , subtree)
        subtree.my_parent = self

    def delete_subtree(self,subtree):
        """
        Delete the given tree of the children of the main tree

        Parameters
        ----------
        subtree: Tree
            Must be a children of the tree

        Returns
        -------
        None

        See Also
        --------
        add_subtree_to_id(...), add_subtree(...), delete_subtree_at_id(...)
        
        """
        subtree.my_parent = None
        self.my_children.remove(subtree)

    def add_subtree_to_id(self,subtree,node_id=None,index=None):
        """
        Add a copy of the given tree as a child of the node (which id is given)
        of the main tree
        
        Parameters
        ----------
        subtree: Tree

        node_id: int, optional
            Must be from the list of ids of the main tree. If None is passed, the subtree will be added as a children of the root.

        index: int, optional
            Index of the subtree to be added in the list of the siblings.

        Returns
        -------
        None

        See Also
        --------
        add_subtree(...), delete_subtree_at_id(...), delete_subtree(...)
        """
        if node_id==None:
            node_id=self.my_id

        if self.my_id==node_id:
            subtree_add=subtree.copy()
            self.add_subtree(subtree_add,index)
        else:
            for child in self.my_children:
                child.add_subtree_to_id(subtree,node_id,index)

    def delete_subtree_at_id(self,node_id):
        """
        Delete the given tree of the children of the node (which id is given)
        of the main tree

        Parameters
        ----------
        subtree: Tree
            Subtree must be a children of the node (which id is given) in the
            main tree

        node_id: int
            Must be from the list of ids of the main tree

        Returns
        -------
        None

        See Also
        --------
        add_subtree(...), add_attribute_to_id(...), delete_subtree(...)
        """
        for child in self.my_children:
            if child.my_id == node_id:
                self.delete_subtree(child)
                return        
        for child in self.my_children:
            child.delete_subtree_at_id(node_id)

    def add_attribute_to_id(self,attribute_name,attribute_value,node_id=None):
        """
        Add an attribute to a node in the tree

        Parameters
        ----------
        attribute_name: str
            The name of the new attribute

        attribute_value:
            The value of the new attribute

        node_id: int or None, optional
            The id of the node where to give the new attribute. Default is
            None, and is interpreted as the id of the tree itself

        Returns
        -------
        None

        See Also
        --------
        get_attribute(...), erase_attribute(...), erase_attribute_at_id(...)
        
        """
        if self.my_id==node_id or node_id==None:
            if isinstance(attribute_name,list):
                for i in range(len(attribute_name)):
                    self.__my_attributes[attribute_name[i]]=attribute_value[i]
            else:
                self.__my_attributes[attribute_name]=attribute_value
        else:
            for child in self.my_children:
                child.add_attribute_to_id(attribute_name,attribute_value,node_id)

    def erase_attribute(self,attribute_name):
        """
        Erase recursively an attribute from all the tree and his descendants

        Parameters
        ----------
        attribute_name: str
            The name of the attribute to delete. It must exists for each node

        Returns
        -------
        None

        See Also
        --------
        get_attribute(...), add_attribute_to_id(...), erase_attribute_at_id(...)
        
        """
        del self.__my_attributes[attribute_name]
        for child in self.my_children:
            child.erase_attribute(attribute_name)

    def erase_attribute_at_id(self,attribute_name,node_id):
        """
        Erase an attribute from one node in the tree

        Parameters
        ----------
        attribute_name: str
            The name of the attribute to delete. It must exists for the node
            considered

        node_id: int
            The id of the node where to delete the attribute

        Returns
        -------
        None

        See Also
        --------
        get_attribute(...), erase_attribute(...), add_attribute_to_id(...)
        
        """
        if self.my_id == node_id:
            del self.__my_attributes[attribute_name]
        else:
            for child in self.my_children:
                child.erase_attribute_at_id(attribute_name,node_id)

# --------------------------------------------------------------

    def sort_subtrees_by_attribute(self , attribute_name , reverse=False):
        """
        This method recursively sorts the children of the tree according to
        their value in the attribute

        Parameters
        ----------
        attribute_name: str
            The attribute selected for sorting. Of course, the attribute must
            exist in the tree
        reverse: bool, optional
            Same as reverse argument in list method sort(). If False, sorting is increasing, if True, sorting is decreasing

        Returns
        -------
        None

        See Also
        --------
        subtrees_sorted_by(...)
        """
        self.my_children.sort(key=lambda child: child.get_attribute(attribute_name),reverse=reverse)
        for child in self.my_children:
            child.sort_subtrees_by_attribute(attribute_name,reverse)


    def sort_subtrees_by_property(self,prop,reverse=False):
        """
        This method recursively sorts the children of the tree according to
        their value in the property

        Parameters
        ----------
        prop: str
            The property selected for sorting
        reverse: bool, optional
            Same as reverse argument in list method sort(). If False, sorting is increasing, if True, sorting is decreasing

        Returns
        -------
        None

        See Also
        --------
        subtrees_sorted_by(...)
        """
        self.my_children.sort(key=lambda child: child.get_property(prop,compute_level = 1),reverse=reverse)
        for child in self.my_children:
            child.sort_subtrees_by_property(prop,reverse)
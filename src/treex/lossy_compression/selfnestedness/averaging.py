# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.lossy_compression.selfnestedness.averaging
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
    Called by this module: treex.lossy_compression.selfnested.heightprofile

    This module implements functions to approximate a Tree class object by a
    selfnested tree (of Tree class too) or a sequence of Tree class objects

    - File author(s): Romain Azais <romain.azais@inria.fr>

"""

from treex.lossy_compression.selfnestedness.heightprofile import *

# --------------------------------------------------------------

def averaging(tree,max_height=None):
    """
    This function computes a selfnested approximation of a tree

    Parameters
    ----------
    tree: Tree

    Returns
    -------
    Tree

    References
    ----------
    This function implements the averaging algorithm presented in
    AZAIS, DURAND, and GODIN "Approximation of trees by self-nested trees"
    ALENEX 2019, San Diego (Ca)

    See also
    --------
    sequential_averaging(...)

    """

    h_p = heightprofile(tree)

    if h_p.is_selfnested():
        return tree

    else:
        # -----------------------------------------------------
        mat=[]
        for i in range(len(h_p.profile)):
            mat_i=[]
            for j in range(i+1):
                moy=sum(h_p.profile[i][j])/len(h_p.profile[i][j])
                app_moy=int(round(moy))
                mat_i.append(app_moy)
            mat.append(mat_i)
        sn_hp = SelfNestedHeightProfile(len(h_p.profile))
        sn_hp.profile = mat
        # -----------------------------------------------------

        if max_height==None or tree.get_property('height',compute_level=1)<=max_height:
            return sn_hp.to_tree()
        
        else:
            approx_t = tree.copy()
            dict_subtrees = approx_t.subtrees_sorted_by('height')
            
            for h in range(max_height , 0,-1):
                list_subtrees_h = dict_subtrees[h]

                for subt in list_subtrees_h:
                    # ID parent
                    id_parent = subt.my_parent.my_id
                    # ID node
                    id_node = subt.my_id
                    # Delete subtree
                    approx_t.delete_subtree_at_id( id_node )
                    # Add self-nested subtree
                    approx_t.add_subtree_to_id( sn_hp.sub_heightprofile(h).to_tree() , id_parent )

            return approx_t

# --------------------------------------------------------------

def sequential_averaging(tree):
    """
    This function computes a sequence of approximations of a tree

    Parameters
    ----------
    tree: Tree

    Returns
    -------
    list
        a list of Tree class objects

    Notes
    -----
    Function averaging(...) computes the selfnested approximation of a tree
    Applying the approximation algorithm by height defines a sequence of trees
    that approximate the initial tree, the last one being the selfnested tree
    computed by function averaging(...)

    References
    ----------
    To read further about this approximation algorithm, see: to be completed (paper in progress)

    See also
    --------
    averaging(...)
    
    """
    h_p = heightprofile(tree)

    if h_p.is_selfnested():
        return [tree.copy()]
    else:
        # -----------------------------------------------------
        mat=[]
        for i in range(len(h_p.profile)):
            mat_i=[]
            for j in range(i+1):
                moy=sum(h_p.profile[i][j])/len(h_p.profile[i][j])
                app_moy=int(round(moy))
                mat_i.append(app_moy)
            mat.append(mat_i)
        sn_hp = SelfNestedHeightProfile(len(h_p.profile))
        sn_hp.profile = mat
        # -----------------------------------------------------

        cop_tree = tree.copy()

        liste_approx = [cop_tree]

        for max_height in range(1,tree.get_property('height',compute_level=1)):

            approx_t = cop_tree.copy()
            dict_subtrees = approx_t.subtrees_sorted_by('height')

            for h in range(max_height , 0,-1):
                list_subtrees_h = dict_subtrees[h]

                for subt in list_subtrees_h:
                    # ID parent
                    id_parent = subt.my_parent.my_id
                    # ID node
                    id_node = subt.my_id
                    # Delete subtree
                    approx_t.delete_subtree_at_id( id_node )
                    # Add self-nested subtree
                    approx_t.add_subtree_to_id( sn_hp.sub_heightprofile(h).to_tree() , id_parent )

            liste_approx.append(approx_t)

            if heightprofile(approx_t).is_selfnested():
                return liste_approx

# --------------------------------------------------------------
# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.converters.lpy.lpymodel
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       File contributor(s):
#           Christophe Godin <christophe.godin@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

try:
    from openalea import lpy
except ImportError:
    print("LPy should be installed to use these functionalities")
else:

    from treex.converters.lpy.lstring import *

    def lpymodel_to_tree(model_filename , translator_filename , header_filename):

        # Part 1: generate lstring
        lsysmod = lpy.Lsystem(model_filename)
        lsystrans = lpy.Lsystem(translator_filename)

        # Run LPy model
        for lstring in lsysmod:
            pass

        ilstring = str(lsystrans.derive(lstring))

        # Part 2: build header
        head_file = open(header_filename , 'r')
        header_contents = head_file.read()

        # init_modlist = -1
        # end_modlist = -1
        # init_comment = -1
        # end_comment = -1
        # for i in range(len(contents.split("\n"))):
            
        #     line = contents.split("\n")[i]
        #     if line == "begin modlist":
        #         init_modlist = i
        #     elif line == "end modlist":
        #         end_modlist = i
        #     elif line == "begin comment":
        #         init_comment = i
        #     elif line == "end comment":
        #         end_comment = i

        # header = ""
        # for i in range( init_modlist , end_modlist+1):
        #     header += contents.split("\n")[i] + "\n"

        # if init_comment != -1 and end_comment != -1:
        #     comm = ""
        #     for i in range( init_comment , end_comment):
        #         comm += contents.split("\n")[i]
        #     comm += contents.split("\n")[end_comment]

        #     header += comm
 
        # Part 3: construction of the tree
        t = lstring_to_tree(ilstring, header_contents)

        return t, ilstring
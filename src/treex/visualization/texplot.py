# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.visualization.texplot
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

""" 
    Called by this module : treex.tree, treex.dag, random, math, copy.copy()

    This module implements functions that transform a tree or dag structure to a .tex document

    - File author(s): Romain Azais <romain.azais@inria.fr>

"""

from treex.tree import *
from treex.lossless_compression.dag import *

import random
import math

from copy import copy

def __nb_to_word(nb):
    alpha = 'abcdefghij'
    str_nb = str(nb)
    s=''
    for char in str_nb:
        s+=alpha[int(char)]
    return s

# ---------------------------------------------------------------------------------

def __taille_rec(tree,s):
    if tree.my_children==[]:
        return 1
    else:
        a=copy(s)
        for child in tree.my_children:
            a+=__taille_rec(child,s)
        return a

def __coord_children(tree,generation,abscisse_racine,ordonnee_racine,dico,direction):

    dico[tree.my_id]=[abscisse_racine , ordonnee_racine , []]

    if 'color_plotgraph' in tree.get_attribute().keys():
        dico[tree.my_id].append( tree.get_attribute('color_plotgraph') )
    else:
        dico[tree.my_id].append('black')

    if 'nodescale_plotgraph' in tree.get_attribute().keys():
        dico[tree.my_id].append( tree.get_attribute('nodescale_plotgraph') )
    else:
        dico[tree.my_id].append(1)

    if 'linewidth_plotgraph' in tree.get_attribute().keys():
        dico[tree.my_id].append( tree.get_attribute('linewidth_plotgraph') )
    else:
        dico[tree.my_id].append(1)

    liste2=[]
    for i in range(len(tree.my_children)):
        liste2.append(__taille_rec(tree.my_children[i],0))

    h=1
    for i in range(len(tree.my_children)):

        if direction=='down':
            absi=abscisse_racine-sum(liste2)/2.0+sum(liste2[0:i])+liste2[i]/2.0 # abscisse du noeud
            ordi=ordonnee_racine-h/math.sqrt(math.sqrt(generation+1)) # ordonnee du noeud
        elif direction=='up':
            absi=abscisse_racine-sum(liste2)/2.0+sum(liste2[0:i])+liste2[i]/2.0 # abscisse du noeud
            ordi=ordonnee_racine+h/math.sqrt(math.sqrt(generation+1)) # ordonnee du noeud
        elif direction=='left':
            ordi=ordonnee_racine-sum(liste2)/2.0+sum(liste2[0:i])+liste2[i]/2.0 # abscisse du noeud
            absi=abscisse_racine-h/math.sqrt(math.sqrt(generation+1)) # ordonnee du noeud
        else:
            ordi=ordonnee_racine-sum(liste2)/2.0+sum(liste2[0:i])+liste2[i]/2.0 # abscisse du noeud
            absi=abscisse_racine+h/math.sqrt(math.sqrt(generation+1)) # ordonnee du noeud

        dico[tree.my_id][2].append(tree.my_children[i].my_id)

        __coord_children(tree.my_children[i],generation+1,absi,ordi,dico,direction)

def __coord_treegraph(tree,direction='down'):
    abs_racine=0
    ord_racine=0
    dico={}
    __coord_children(tree,0,abs_racine,ord_racine,dico,direction)
    return dico

def tree_to_tex(tree,path,xscale=1,yscale=1,nodescale=1,direction='down'):
    """
    This function creates a .tex document from a Tree class object

    Parameters
    ----------
    tree: Tree or list
        The tree might have color or nodescale attribute (see
        treex.vizualisation.options); if not, default is color black and
        nodescale 1. If a list of trees is provided, they will be drawn in the
        same document, in separate tikz pictures

    path: str
        The path where to save the document. Path must include the filename,
        ending by .tex

    xscale: int, optional
        This parameter controls the x-scale in the .tex document

    yscale: int, optional
        This parameter controls the y-scale in the .tex document

    nodescale: int, optional
        This parameter controls the scale of nodes in the .tex document

    direction: str, optional
        The direction where children should be placed relatively to their
        parent. Possibilities are 'up', 'down', 'left'. Any other string is
        interpreted as 'right'. Default is 'down'

    Returns
    -------
    None

    See Also
    --------
    treex.vizualisation.options(...)

    """

    fichier = open(path,"w")
    
    fichier.write("\\"+"documentclass[11pt]{article}"+"\n")
    fichier.write("\\"+"usepackage[T1]{fontenc}"+"\n")
    fichier.write("\\"+"usepackage[english]{babel}"+"\n")
    fichier.write("\\"+"usepackage{graphicx}"+"\n")
    fichier.write("\\"+"usepackage{tikz}"+"\n")
    fichier.write("\\"+"begin{document}"+"\n")

    fichier.write("\\"+"def"+"\\"+"xscale{"+str(xscale)+"}"+"\n")
    fichier.write("\\"+"def"+"\\"+"yscale{"+str(yscale)+"}"+"\n")
    fichier.write("\\"+"def"+"\\"+"nodescale{"+str(nodescale)+"}"+"\n")

    if isinstance(tree,list)==False:

        coordonnees=__coord_treegraph(tree,direction)

        liste_couleurs=[]
        liste_nodescales=[]
        for val in coordonnees.values():
            liste_couleurs.append(val[3])
            liste_nodescales.append(val[4])
            ### linewidth in position 5 ### Romain ### Feb 6 2020 ###

        liste_couleurs=list(set(liste_couleurs))
        liste_nodescales=list(set(liste_nodescales))

        list_coordonnees=[coordonnees]
        list_liste_couleurs=[liste_couleurs]
        list_liste_nodescales=[liste_nodescales]

    else:
        list_coordonnees=[]
        list_liste_couleurs=[]
        list_liste_nodescales=[]
        for t in tree:
            coordonnees=__coord_treegraph(t,direction)
            liste_couleurs=[]
            liste_nodescales=[]

            for val in coordonnees.values():
                liste_couleurs.append(val[3])
                liste_nodescales.append(val[4])

            liste_couleurs=list(set(liste_couleurs))
            liste_nodescales=list(set(liste_nodescales))

            list_coordonnees.append(coordonnees)
            list_liste_couleurs.append(liste_couleurs)
            list_liste_nodescales.append(liste_nodescales)

    for i in range(len(list_coordonnees)):

        coordonnees=list_coordonnees[i]
        liste_couleurs=list_liste_couleurs[i]

        fichier.write("\\"+"begin{tikzpicture}[xscale="+"\\"+"xscale"+",yscale="+"\\"+"yscale"+"]"+"\n")

        fichier.write("\\"+"tikzstyle{fleche}=[-,>=latex]"+"\n")

        for couleur in liste_couleurs:
            fichier.write("\\"+"tikzstyle{noeud"+couleur+"}=[draw,circle,fill="+couleur+",scale="+"\\"+"nodescale"+"]"+"\n")

        for nodescale in liste_nodescales:
            fichier.write("\\"+'def'+"\\"+"localnodescale"+__nb_to_word(liste_nodescales.index(nodescale))+"{"+str(nodescale)+"}"+"\n")

        # Affichage des noeuds
        for noeud in coordonnees.keys():
            label=noeud
            abscisse=coordonnees[noeud][0]
            ordonnee=coordonnees[noeud][1]
            couleur=coordonnees[noeud][3]
            nodescale=coordonnees[noeud][4]

            fichier.write("\\"+"node[noeud"+couleur+",scale="+"\\"+"localnodescale"+__nb_to_word(liste_nodescales.index(nodescale))+"] ("+str(label)+") at ({"+str(abscisse)+"},{"+str(ordonnee)+"}) {};"+"\n")

        # Affichage des liens
        for noeud in coordonnees.keys():
            label=noeud
            for destination in coordonnees[noeud][2]:
                fichier.write("\\"+"draw[fleche] ("+str(label)+")--("+str(destination)+") {};"+"\n")

        fichier.write("\\"+"end{tikzpicture}"+"\n")

    fichier.write("\\"+"end{document}")
    fichier.close()

# ---------------------------------------------------------------------------------

def __coord_dag(dag):

    nb_nodes=len(dag.nodes())
    nodes_sorted = dag.nodes_sorted_by('height')

    for h in nodes_sorted.keys():

        nb_node_h=len(nodes_sorted[h])

        cpt=0
        for u in nodes_sorted[h]:
            x=(cpt-0.5*nb_node_h)
            dag.add_attribute_to_node('coord_plotgraph',[x,h],u) # [x,h] = [abscissa , height]
            cpt+=1

def dag_to_tex(dag,path,xscale=1,yscale=1,nodescale=1,direction='down'):
    """
    This function creates a .tex document from a Dag class object

    Parameters
    ----------
    dag: Dag or list
        The dag might have color or nodescale attribute (see
        treex.vizualisation.options); if not, default is color black and
        nodescale 1. If a list of dags is provided, they will be drawn in the
        same document, in separate tikz pictures

    path: str
        The path where to save the document. Path must include the filename,
        ending by .tex

    xscale: int, optional
        This parameter controls the x-scale in the .tex document

    yscale: int, optional
        This parameter controls the y-scale in the .tex document

    nodescale: int, optional
        This parameter controls the scale of nodes in the .tex document

    direction: str, optional
        The direction where children should be placed relatively to their
        parent. Possibilities are 'up', 'down', 'left'. Any other string is
        interpreted as 'right'. Default is 'down'

    Returns
    -------
    None

    See Also
    --------
    treex.vizualisation.options(...)

    """

    fichier = open(path,"w")
    
    fichier.write("\\"+"documentclass[11pt]{article}"+"\n")
    fichier.write("\\"+"usepackage[T1]{fontenc}"+"\n")
    fichier.write("\\"+"usepackage[english]{babel}"+"\n")
    fichier.write("\\"+"usepackage{graphicx}"+"\n")
    fichier.write("\\"+"usepackage{tikz}"+"\n")
    fichier.write("\\"+"begin{document}"+"\n")

    fichier.write("\\"+"def"+"\\"+"xscale{"+str(xscale)+"}"+"\n")
    fichier.write("\\"+"def"+"\\"+"yscale{"+str(yscale)+"}"+"\n")
    fichier.write("\\"+"def"+"\\"+"nodescale{"+str(nodescale)+"}"+"\n")

    if isinstance(dag,list)==False:
        list_dag=[dag]
    else:
        list_dag=dag

    for dag_i in list_dag:

        __coord_dag(dag_i)

        fichier.write("\\"+"begin{tikzpicture}[xscale="+"\\"+"xscale,yscale="+"\\"+"yscale]"+"\n")

        fichier.write("\\"+"tikzstyle{fleche}=[-,>=latex]"+"\n")

        # Affichage des noeuds
        for noeud in dag_i.my_structure.keys():
            label=noeud

            if direction=='down':
                abscisse=dag_i.get_attribute(node=noeud , attribute = 'coord_plotgraph')[0]
                ordonnee=dag_i.get_attribute(node=noeud , attribute = 'coord_plotgraph')[1]
            elif direction=='up':
                abscisse=dag_i.get_attribute(node=noeud , attribute = 'coord_plotgraph')[0]
                ordonnee=-dag_i.get_attribute(node=noeud , attribute = 'coord_plotgraph')[1]
            elif direction=='right':
                ordonnee=dag_i.get_attribute(node=noeud , attribute = 'coord_plotgraph')[0]
                abscisse=-dag_i.get_attribute(node=noeud , attribute = 'coord_plotgraph')[1]
            else:
                ordonnee=-dag_i.get_attribute(node=noeud , attribute = 'coord_plotgraph')[0]
                abscisse=dag_i.get_attribute(node=noeud , attribute = 'coord_plotgraph')[1]

            if 'color_plotgraph' not in dag_i.get_attribute(node=noeud).keys():
                couleur_noeud='black'
            else:
                couleur_noeud=dag_i.get_attribute(attribute='color_plotgraph',node=noeud)

            if 'nodescale_plotgraph' not in dag_i.get_attribute(node=noeud).keys():
                localnodescale=1
            else:
                localnodescale=dag_i.get_attribute(node=noeud,attribute='nodescale_plotgraph')

            fichier.write("\\"+"tikzstyle{noeud}=[draw,circle,fill="+couleur_noeud+",scale="+"\\"+"nodescale*"+str(localnodescale)+"]"+"\n")

            fichier.write("\\"+"node[noeud] ("+str(label)+") at ({"+str(abscisse)+"},{"+str(ordonnee)+"}) {};"+"\n")

        # Affichage des liens
        for noeud in dag_i.nodes():
            label=noeud

            compteur=1
            for destination in dag_i.children(noeud):

                if dag_i.get_attribute(node=noeud , attribute = 'coord_plotgraph')[0]==dag_i.get_attribute(node=destination , attribute = 'coord_plotgraph')[0] and dag_i.get_attribute(node=noeud , attribute = 'coord_plotgraph')[1]!=dag_i.get_attribute(node=destination , attribute = 'coord_plotgraph')[1]+1:
                    compteur*=-1
                    if compteur==1:
                        fichier.write("\\"+"draw[fleche] ("+str(label)+") to [bend right=45]("+str(destination)+") {};"+"\n")
                    else:
                        fichier.write("\\"+"draw[fleche] ("+str(label)+") to [bend left=45]("+str(destination)+") {};"+"\n")
                else:
                    fichier.write("\\"+"draw[fleche] ("+str(label)+")--("+str(destination)+") {};"+"\n")

        fichier.write("\\"+"end{tikzpicture}"+"\n")

    fichier.write("\\"+"end{document}")
    fichier.close()

# ---------------------------------------------------------------------------------
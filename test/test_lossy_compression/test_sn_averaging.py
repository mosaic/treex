# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_lossy_compression/test_sn_averaging
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.lossy_compression.selfnestedness.averaging import *
from treex.analysis.edit_distance.add_del_leaf import constrained_unordered_edit_distance

def test_av1():
	t = gen_random_tree(200)
	s = averaging(t)
	hp_s = heightprofile(s)
	assert hp_s.is_selfnested()

def test_av2():
	t = gen_random_tree(30)
	h = int(t.get_property('height')/2.0)+1
	s1 = averaging(t,h)
	s2 = averaging(t)
	assert constrained_unordered_edit_distance(t,s2)>=constrained_unordered_edit_distance(t,s1)

def test_av3():
	t = gen_random_tree(2)
	s = averaging(t)
	assert t.is_isomorphic_to(s)

def test_seq_av1():
	t = gen_random_tree(200)
	s = averaging(t)
	seq_s = sequential_averaging(t)
	assert seq_s[-1].is_isomorphic_to(s)

def test_seq_av2():
	t = gen_random_tree(2)
	seq_s = sequential_averaging(t)
	assert seq_s[0].is_isomorphic_to(t)
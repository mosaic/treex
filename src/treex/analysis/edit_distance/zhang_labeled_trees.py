# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.analysis.edit_distance.zhang_labeled_trees
#
#       File author(s):
#           salah eddine habibeche <salah-eddine.habibeche@inria.fr>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#           Farah Ben Naoum <farahbennaoum@yahoo.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
#
# ------------------------------------------------------------------------------
"""
    Called by this module: treex.tree, networkx

    This module allows to compute a zhang_edit_distance between
    unordered trees

    - File author(s): Salah eddine habibeche <salah-eddine.habibeche@inria.fr>
    - File contributor(s): Romain Azais <romain.azais@inria.fr>, Farah Ben Naoum <farahbennaoum@yahoo.fr>

    References
    ----------

    This code is an implementation of the algorithm of tree edit distance
    presented in

    ZHANG, Kaizhong. A constrained edit distance between unordered labeled trees.
    Algorithmica, 1996, vol. 15, no 3, p. 205-222.

    This version allows the use of attributes on tree nodes

    Acknowledgment
    --------------

    We would like to thank Stefan Hahmann and Matthias Arzt for pointing out a bug in
    the computation of the distance and proposing us a fix
"""



from treex.tree import *
from treex.lossless_compression.dag import *
import networkx as nx

def __cost_function(tree1=None,tree2=None,label_attribute=None,local_distance=None):
    # call the local distance function to compute  distance between the vector associated to tree1 and the vector 
    # associated to tree2 
    # Parameters
    # ----------
    # tree1,tree2: treex tree
    # local_distance: function
    # Returns
    # -------    
    # the distance  between the 2 vectors 
    
    if tree2==None: # cost for creation of tree1
            return local_distance(tree1.get_attribute(label_attribute),None)
    else: 
            v1=tree1.get_attribute(label_attribute)
            v2=tree2.get_attribute(label_attribute)
            return local_distance(v1,v2)

def __calcul_cost_sup_ins_forest_tree(s,cost_tree_to_none=None): #private
    # Compute  the cost of deleting / inserting a forest/tree
    # cost of deleting / inserting a leaf is the cost of deleting / inserting its attribute
    # cost of deleting / inserting a tree is the cost of deleting / inserting  the
    # attribute of its source + the  cost of deleting / inserting of the forest
    # associated with that source
    # cost of deleting / inserting of a forest is the cost of deleting / inserting of
    # all trees belonging to it

    df={}
    dt={}
    if(len(s.my_children)==0):
        df[s]=0
        if(cost_tree_to_none==None):
            dt[s]=1
        else:
            dt[s]=cost_tree_to_none[s.my_id]
    else:
        v=0
        for child in s.my_children:
            dfi={}
            dti={}
            dfi,dti=__calcul_cost_sup_ins_forest_tree(child,cost_tree_to_none)
            v+=dti[child]
            df.update(dfi)
            dt.update(dti)

        df[s]=v
        if(cost_tree_to_none==None):
            dt[s]=v+1
        else:
            dt[s]=v+cost_tree_to_none[s.my_id]
    return df,dt


def __mincostmaxflot(forest1,forest2,local_distance=None,mt=None,l1=None,l2=None,dst=None,dit=None): # Private

    n1=len(forest1.my_children)
    n2=len(forest2.my_children)

    if(local_distance==None):
        dico1={}
        for tree1 in forest1.my_children:
            if tree1.get_attribute('unordered_equivalence_class') in dico1.keys():
                dico1[tree1.get_attribute('unordered_equivalence_class')].append(tree1)
            else:
                dico1[tree1.get_attribute('unordered_equivalence_class')]=[tree1]
        dico2={}
        for tree2 in forest2.my_children:
            if tree2.get_attribute('unordered_equivalence_class') in dico2.keys():
                dico2[tree2.get_attribute('unordered_equivalence_class')].append(tree2)
            else:
                dico2[tree2.get_attribute('unordered_equivalence_class')]=[tree2]

    else:
        dico1={}
        for tree1 in forest1.my_children:
            if tree1.get_attribute('unordered_equivalence_class_with_attribute') in dico1.keys():
                dico1[tree1.get_attribute('unordered_equivalence_class_with_attribute')].append(tree1)
            else:
                dico1[tree1.get_attribute('unordered_equivalence_class_with_attribute')]=[tree1]
        dico2={}
        for tree2 in forest2.my_children:
            if tree2.get_attribute('unordered_equivalence_class_with_attribute') in dico2.keys():
                dico2[tree2.get_attribute('unordered_equivalence_class_with_attribute')].append(tree2)
            else:
                dico2[tree2.get_attribute('unordered_equivalence_class_with_attribute')]=[tree2]

    n1=len(dico1.keys())
    n2=len(dico2.keys())

    N1=[len(dico1[list(dico1.keys())[i]]) for i in range(n1)]
    N1s=sum(N1)

    N2=[len(dico2[list(dico2.keys())[i]]) for i in range(n2)]
    N2s=sum(N2)

    # Construction of graph for max flow min cost algorithm
    # 0 source
    # (n1+n2+1) sink
    # (n1+n2+2) empty tree 1
    # (n1+n2+3) empty tree 2
    G = nx.DiGraph()
    G.add_edges_from([(0 , (n1+n2+2) ,{'capacity': N2s-min(N1s,N2s),'weight': 0})])  #source to ei
    G.add_edges_from([((n1+n2+3) , (n1+n2+1),{'capacity': N1s-min(N1s,N2s),'weight': 0})]) #ej to sink
    G.add_edges_from([((n1+n2+2) , (n1+n2+3),{'capacity': max(N1s,N2s)-min(N1s,N2s),'weight': 0})]) #ej to sink

    for i in range(n1):
        G.add_edges_from([(0, (i+1), {'capacity': N1[i], 'weight': 0})]) #source to ik
        for j in range(n2):
            s1=dico1[list(dico1.keys())[i]][0]
            s2=dico2[list(dico2.keys())[j]][0]


            distance_a_calculer=mt[l1.index(s1)][l2.index(s2)]
            G.add_edges_from([( (i+1) , (n1+j+1) , {'capacity': N1[i], 'weight': distance_a_calculer })]) #ik to jk
        G.add_edges_from([( (i+1) , (n1+n2+3) , {'capacity': N1[i], 'weight': dst[s1] })]) #ik to ej
    for j in range(n2):
        s2=dico2[list(dico2.keys())[j]][0]

        G.add_edges_from([((n1+n2+2),(n1+j+1),{'capacity': N2s-min(N1s,N2s),'weight':dit[s2]})]) #ei to jk
        G.add_edges_from([( (n1+j+1) , (n1+n2+1) , {'capacity': N2[j], 'weight': 0})]) #jk to sink

    mincostFlow = nx.max_flow_min_cost(G , 0 , (n1+n2+1))
    mincost = nx.cost_of_flow(G, mincostFlow)

    return mincost



def __distance_zhang_forest(forest1,forest2,local_distance,l1,l2,mf,mt,dif,dsf,dit,dst,cost_tree_to_none,cost_tree_to_tree): # Private
    # Calculate the  zhang edit distance between two subforests

    if(mf[l1.index(forest1)][l2.index(forest2)]!=-1):
        return mf[l1.index(forest1)][l2.index(forest2)]
    else:


        if(len(forest1.my_children)!=0 and len(forest2.my_children)==0):
            mf[l1.index(forest1)][l2.index(forest2)]=dsf[forest1]
            return dsf[forest1]

        if(len(forest2.my_children)!=0 and len(forest1.my_children)==0):
            mf[l1.index(forest1)][l2.index(forest2)]=dif[forest2]
            return dif[forest2]

        if(len(forest2.my_children)!=0 and len(forest1.my_children)!=0):
            a=dif[forest2]
            l=[]
            if(len(forest2.my_children)!=0):
                for child in forest2.my_children:
                    l.append(__distance_zhang_forest(forest1,child,local_distance,l1,l2,mf,mt,dif,dsf,dit,dst,cost_tree_to_none,cost_tree_to_tree)-dif[child])
                a+=min(l)
            b=dsf[forest1]
            l=[]
            if(len(forest1.my_children)!=0):
                for child in forest1.my_children:
                    l.append(__distance_zhang_forest(child,forest2,local_distance,l1,l2,mf,mt,dif,dsf,dit,dst,cost_tree_to_none,cost_tree_to_tree)-dsf[child])
                b+=min(l)
            c=__mincostmaxflot(forest1,forest2,local_distance,mt,l1,l2,dst,dit)
            mf[l1.index(forest1)][l2.index(forest2)]=min([a,b,c])
            return min([a,b,c])

def __distance_zhang_tree(tree1=None,tree2=None,local_distance=None,l1=None,l2=None,mt=None,mf=None,dit=None,dif=None,dst=None,dsf=None,dic_class=None,verbose=None,cost_tree_to_none=None,cost_tree_to_tree=None):# Private
    # Calculate the zhang edit distance between two sub-trees

    if(dic_class[tree1.my_id]==dic_class[tree2.my_id] and verbose==bool(False)):
        mt[l1.index(tree1)][l2.index(tree2)]=0
        mf[l1.index(tree1)][l2.index(tree2)]=0
        return 0;
    if(mt[l1.index(tree1)][l2.index(tree2)]!=-1):
        return mt[l1.index(tree1)][l2.index(tree2)]

    if(len(tree1.my_children)==0 and len(tree2.my_children)==0):

        if(local_distance==None):
            mt[l1.index(tree1)][l2.index(tree2)]=0
            mf[l2.index(tree2)][l1.index(tree1)]=0
            return 0
        else:


            mt[l1.index(tree1)][l2.index(tree2)]=cost_tree_to_tree[(tree1.my_id,tree2.my_id)]
            return cost_tree_to_tree[(tree1.my_id,tree2.my_id)]

    else:


        a=dit[tree2]
        l=[]
        if(len(tree2.my_children)!=0):
            for child in tree2.my_children:
                l.append(__distance_zhang_tree(tree1,child,local_distance,l1,l2,mt,mf,dit,dif,dst,dsf,dic_class,verbose,cost_tree_to_none,cost_tree_to_tree)-dit[child])
            a+=min(l)
        else:
                        a+=dst[tree1]
        b=dst[tree1]
        l=[]
        if(len(tree1.my_children)!=0):
            for child in tree1.my_children:
                l.append(__distance_zhang_tree(child,tree2,local_distance,l1,l2,mt,mf,dit,dif,dst,dsf,dic_class,verbose,cost_tree_to_none,cost_tree_to_tree)-dst[child])
            b+=min(l)
        else:
            b+=dit[tree2]

        if(local_distance==None):
            c=__distance_zhang_forest(tree1,tree2,local_distance,l1,l2,mf,mt,dif,dsf,dit,dst,cost_tree_to_none,cost_tree_to_tree)
        else:
            c=__distance_zhang_forest(tree1,tree2,local_distance,l1,l2,mf,mt,dif,dsf,dit,dst,cost_tree_to_none,cost_tree_to_tree)+cost_tree_to_tree[(tree1.my_id,tree2.my_id)]

        mt[l1.index(tree1)][l2.index(tree2)]=min([a,b,c])
        return min([a,b,c])

def zhang_edit_distance(tree1,tree2,label_attribute=None,local_distance=None,verbose=False):

    """
    Calculate the Zhang edit distance between two (labeled) unordered trees

    Parameters
    ----------
    t1: Tree
    t2: Tree
    local_distance: function, optional
        a cost function
    label_attribute: tuple

    Returns
    -------
    int
        the Zhang edit distance between tree1 and tree2
    """
    if(tree2==None and local_distance==None):
        return tree1.get_property('size',compute_level=1)
    if(tree2==None and local_distance!=None):
        s=0
        for subtree in tree1.list_of_subtrees():
            s+=__cost_function(subtree,None,label_attribute,local_distance)
        return s

    cost_tree_to_tree={}
    cost_tree_to_none={}
    dico_supp_forest = {}
    dico_supp_tree={}
    dico_ins_forest = {}
    dico_ins_tree={}
    dic_class={}
    if(local_distance==None):
        supertree=Tree()
        supertree.my_children.append(tree1)
        supertree.my_children.append(tree2)
        tree_to_dag(supertree,tree_type="unordered")
        l1=tree1.list_of_subtrees()
        l2=tree2.list_of_subtrees()
        for st in l1:
            dic_class[st.my_id]=st.get_attribute("unordered_equivalence_class")
        for st in l2:
            dic_class[st.my_id]=st.get_attribute("unordered_equivalence_class")
        dico_supp_forest,dico_supp_tree=__calcul_cost_sup_ins_forest_tree(tree1)
        dico_ins_forest,dico_ins_tree=__calcul_cost_sup_ins_forest_tree(tree2)

    else:


        supertree=Tree()
        supertree.add_attribute_to_id(label_attribute,1)

        supertree.my_children.append(tree1)
        supertree.my_children.append(tree2)
        tree_to_dag(supertree,attribute=label_attribute)
        l1=tree1.list_of_subtrees()
        l2=tree2.list_of_subtrees()

        for st in l1:
            cost_tree_to_none[st.my_id]=__cost_function(st,None,label_attribute,local_distance)
            dic_class[st.my_id]=st.get_attribute("unordered_equivalence_class_with_attribute")
        for st in l2:
            cost_tree_to_none[st.my_id]=__cost_function(st,None,label_attribute,local_distance)
            dic_class[st.my_id]=st.get_attribute("unordered_equivalence_class_with_attribute")
        for st in l1:
            for st2 in l2:
                cost_tree_to_tree[(st.my_id,st2.my_id)] =__cost_function(st,st2,label_attribute,local_distance)

        dico_supp_forest,dico_supp_tree=__calcul_cost_sup_ins_forest_tree(tree1,cost_tree_to_none)
        dico_ins_forest,dico_ins_tree=__calcul_cost_sup_ins_forest_tree(tree2,cost_tree_to_none)


    gridf = [[-1] * (len(l2)) for _ in range(len(l1))]
    gridt = [[-1] * (len(l2)) for _ in range(len(l1))]

    if(verbose == False):
        return __distance_zhang_tree(tree1,tree2,local_distance,l1,l2,gridt,gridf,dico_ins_tree,dico_ins_forest,dico_supp_tree,dico_supp_forest,dic_class,verbose,cost_tree_to_none,cost_tree_to_tree)
    else:

        dist= __distance_zhang_tree(tree1,tree2,local_distance,l1,l2,gridt,gridf,dico_ins_tree,dico_ins_forest,dico_supp_tree,dico_supp_forest,dic_class,verbose,cost_tree_to_none,cost_tree_to_tree)


        d_tree={}
        for i in range (0,len(l1)):
            for j in range (0,len(l2)):
                d_tree[(l1[i].my_id,l2[j].my_id)]=gridt[i][j]
        d_forest={}
        for i in range (0,len(l1)):
            for j in range (0,len(l2)):
                d_forest[(l1[i].my_id,l2[j].my_id)]=gridf[i][j]

        d_tree_sup={}
        for i in range (0,len(l1)):
            d_tree_sup[l1[i].my_id]=dico_supp_tree[l1[i]]
        d_forest_sup={}
        for i in range (0,len(l1)):
            d_forest_sup[l1[i].my_id]=dico_supp_forest[l1[i]]

        d_tree_ins={}
        for i in range (0,len(l2)):
            d_tree_ins[l2[i].my_id]=dico_ins_tree[l2[i]]
        d_forest_ins={}
        for i in range (0,len(l2)):
            d_forest_ins[l2[i].my_id]=dico_ins_forest[l2[i]]

        d={}
        d["distance"]=dist

        d["matrix_of_trees_distances"]=d_tree
        d["matrix_of_forests_distances"]=d_forest
        d["cost_of_trees_suppression"]=d_tree_sup
        d["cost_of_forests_suppression"]=d_forest_sup
        d["cost_of_trees_insertion"]=d_tree_ins
        d["cost_of_forests_insertion"]=d_forest_ins


        return(d)

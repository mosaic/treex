# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.kernels.support
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
This module introduce most of the functions that are meant to be used when computing a kernel from the other modules.

- File author(s): Florian Ingels <florian.ingels@inria.fr>
- File contributor(s): Romain Azais <romain.azais@inria.fr>
"""
import random
import copy
import math
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from treex.lossless_compression.dag import *

# ------------------------------------------------------------------------------

def merge_samples(sample_list):
    """
    Given a list of samples (dict of class - origin list), copy and merge them into one.
    """
    sample={}
    for s in sample_list:
        for k in s:
            if k in sample:
                sample[k]+=s[k].copy()
            else:
                sample[k]=s[k].copy()

    return sample

# ------------------------------------------------------------------------------

def exponential_weight(attributes, *args):
    """
    The classic weight for most tree kernels.

    Parameters
    ----------
    attributes : dict
        The attributes of the node for which one needs its weight, typically dag.get_attribute(node).

    *args :
        First passed is the name of the attribute, as a string. Second is either float or int and is used for :math:`\lambda`. Default is 'height' and 0.5


    :return: :math:`\lambda^\\text{attribute}`


    """
    if len(args) > 0:
        attribute = args[0]
        if len(args) > 1:
            mu = args[1]
        else:
            mu = 0.5
    else:
        attribute = 'height'
        mu = 0.5

    try:
        return mu ** attributes[attribute]
    except:
        print('Requested attribute does not exist')


# --------------------------------------------------------------------------------

def gaussian_kernel(i, j, *args):
    """
    Gaussian kernel on integers

    Parameters
    ----------
    i,j : int

    *args : float or int
        First passed is used for sigma, others are ignored. Default is 1




    :return: :math:`\exp( - (i-j)^2 / (2\sigma^2))`


    See Also
    --------
    polynomial_kernel(...), sigmoid_kernel(...)

    """

    if len(args) > 0:
        sigma = args[0]
    else:
        sigma = 1

    return math.exp(-(i - j) ** 2 / 2 / sigma ** 2)


def polynomial_kernel(i, j, *args):
    """
    Polynomial kernel on integers

    Parameters
    ----------

    i,j : int

    *args : float or int
        First passed is d, second is c. Default is d=1, c=0.


    :return: :math:`(ij+c)^d`


    See Also
    --------
    gaussian_kernel(...), sigmoid_kernel(...)

    """
    if len(args) > 0:
        d = args[0]
        if len(args) > 1:
            c = args[1]
        else:
            c = 0
    else:
        d = 1
        c = 0

    return (i * j + c) ** d


def sigmoid_kernel(i, j, *args):
    """
    Sigmoid kernel on integers

    Parameters
    ----------
    i,j : int or float

    *args : float or int
        First passed is c, second is theta. Default is c=1, theta=0.


    :return: :math:`\\text{tanh}(cij-\\theta)`


    See Also
    --------
    polynomial_kernel(...), gaussian_kernel(...)

    """
    if len(args) > 0:
        c = args[0]
        if len(args) > 1:
            theta = args[1]
        else:
            theta = 0
    else:
        c = 1
        theta = 0
    return math.tanh(c * i * j - theta)


# --------------------------------------------------------------------------------

def smoothstep(x, *args):
    """
    Returns :math:`3x^2 - 2x^3`.
    """
    return 3 * (x) ** 2 - 2 * (x) ** 3


def threshold(x, *args):
    """
    With :math:`\epsilon` being the first optional argument given (if none is given, :math:`\epsilon=1`), returns

    .. math::

        \\begin{cases} 1 & \\text{if } x\geq \epsilon\\\ 0 & \\text{otherwise}\end{cases}
    """
    if len(args) > 0:
        epsilon = args[0]
    else:
        epsilon = 1

    return 1 if x >= epsilon else 0


def discriminance_weight(attributes,func=smoothstep,*args):
    """

    This function calculates the effective weight, given by the
    'discriminance' attribute, which must have been computed before.

    Parameters
    ----------
    attributes : dictionary
        The attributes of the node for which one needs its weight. Only 'discriminance' is of interest for this function.

    func : function, optional
        The function to apply to 'discriminance' attribute. Let's say that :math:`x` stands for the value of this attribute. Default is ``smoothstep(...)``.

    *args :
        The optional arguments given to ``func``.



    :rtype: float


    See Also
    ---------
    smoothstep(...), threshold(...)

    Notes
    -----
    All functions designed to be used for this function must accept optional parameters and therefore should be of the form ``foo(x,*args)``.


    """
    try:
        x = attributes['discriminance']

        if callable(func):
            return func(x, *args)
        else:
            return x
    except:
        print('Discriminance has not been calculated yet')


# --------------------------------------------------------------------------------

def view_heatmap(matrix, sample=None, ax=None):
    """
    Given a gram kernel matrix, returns a matplotlib heatmap figure.

    If an optional ``sample`` argument is passed, corresponding to a dictionary of classes - origins, red squares will nest the matrix, highlighting the values computed between elements of the same class.

    If an optional ``ax`` argument is passed, the figure will be embedded in an outer matplotlib figure which axes is ``ax``.

    """
    if ax is None:
        fig, ax = plt.subplots()
    else:
        fig, ax = ax.figure, ax

    cax = ax.imshow(matrix, cmap='viridis', interpolation='nearest')
    divider = make_axes_locatable(ax)
    dax = divider.append_axes("right", size="5%", pad=0.05)

    cbar = fig.colorbar(cax, cax=dax)

    if sample is not None:
        class_vec = []
        for c in sample.keys():
            class_vec += [c] * len(sample[c])

    if sample is not None:
        indices = [0] * (len(set(class_vec)) - 1)
        i = 0
        n_class = 0
        while n_class < len(set(class_vec)) - 1:
            curr_class = class_vec[i]
            while class_vec[i] == curr_class:
                i += 1
            indices[n_class] = i
            n_class += 1

        lim = [0] + indices + [len(class_vec)]

        ax.hlines([i - 0.5 for i in lim[1:len(lim)]], colors=['red'], xmin=[i - 0.5 for i in lim[0:len(lim) - 1]],
                  xmax=[i - 0.5 for i in lim[1:len(lim)]])
        ax.hlines([i - 0.5 for i in lim[0:len(lim) - 1]], colors=['red'], xmin=[i - 0.5 for i in lim[0:len(lim) - 1]],
                  xmax=[i - 0.5 for i in lim[1:len(lim)]])
        ax.vlines([i - 0.5 for i in lim[1:len(lim)]], colors=['red'], ymin=[i - 0.5 for i in lim[0:len(lim) - 1]],
                  ymax=[i - 0.5 for i in lim[1:len(lim)]])
        ax.vlines([i - 0.5 for i in lim[0:len(lim) - 1]], colors=['red'], ymin=[i - 0.5 for i in lim[0:len(lim) - 1]],
                  ymax=[i - 0.5 for i in lim[1:len(lim)]])

    return fig


# --------------------------------------------------------------------------------

def _points_of_interest(classes):
    # points of interests are vectors (0,..,0,1,0,..0) and (1,..,1,0,1,..,1) where n is the number of classes studied
    # these points are discrimining subtrees along the classes : either a subtree is present in only one class, either it is absent in only one class

    presence = {}
    absence = {}

    for key in classes:
        presence[key] = 0
        absence[key] = 1

    p = []
    a = []

    for key in classes:
        vec1 = deepcopy(presence)
        vec1[key] = 1
        vec2 = deepcopy(absence)
        vec2[key] = 0
        p.append(vec1)
        a.append(vec2)

    if len(classes) == 2:
        return p
    else:
        return p + a


def _dist_to_points(vector, list_of_points):
    # return the minimum distance of each subtree to the points of interest. If 0, the subtree is discrimining, if 1, it is not

    min = float('inf')
    pointmin = None

    for point in list_of_points:
        tp = 0
        for key in point.keys():
            tp += (vector[key] - point[key]) ** 2
        v = math.sqrt(tp)
        if v < min:
            min = v
            pointmin = point

    return min, pointmin

# --------------------------------------------------------------------------------

def svm_predict(gram_train,gram_pred,sample_train,sample_pred):
    """
    This function computes SVM from two gram matrices, and return a dictionary of predicted class - origins.

    Parameters
    ----------

    gram_train: array
        The gram matrix used to train the SVM. It should have been constructed with `compute_kernel(sample_train,sample_train)``.

    gram_pred: array
        The gram matrix used for prediction. It should have been constructed with `compute_kernel(sample_pred,sample_train)``.

    sample_train: dict
        Dictionary of class - origins, used for computing the `gram_train` matrix and the `gram_pred` matrix

    sample_pred: dict
        Dictionary of class - origins, used for computing the `gram_pred` matrix

    Returns
    -------
    dict

    Notes
    -----

    For multiclass problems, the SVM is computed using the 'one vs one' strategy. For further details, we refer the user to the documentation of scikit-learn:

    https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html (Consulted September 2019)

    """

    clf = svm.SVC(kernel='precomputed', decision_function_shape='ovo')

    class_train = []
    for c in sample_train.keys():
        class_train+=[c]*len(sample_train[c])

    clf.fit(gram_train, class_train)

    class_pred = list(clf.predict(gram_pred))

    sample_predicted = {}
    i=0
    for c in sample_pred.keys():
        for o in sample_pred[c]:
            cp = class_pred[i]
            i+=1
            if cp in sample_predicted.keys():
                sample_predicted[cp].append(o)
            else:
                sample_predicted[cp]=[o]

    return sample_predicted


# --------------------------------------------------------------------------------

def performance_measure(sample_true, sample_pred, average=None):
    """

    Calling functions `accuracy_score` and `precision_recall_fscore_support` from `sklearn.metrics`, this function returns a set of different metrics measuring the classification performance.

    Parameters
    ----------

        sample_true:

            The ground truth dictionary of class - origins

        class_pred:

            The dictionary of predicted class - origins returned by the function `svm_predict`.

        average: str, optional (default=None)

            Averaging method used in a case of multiclass problem. Use either 'micro' or 'macro'.

    Returns
    -------
    tuple
        (accuracy, precision, recall, fscore)

    Notes
    -----
    Documentation of sklearn.metrics for a description of the different metrics:

    https://scikit-learn.org/stable/modules/model_evaluation.html#classification-metrics (consulted September 2019)

    """

    origins={}

    for c in sample_true.keys():
        for o in sample_true[c]:
            origins[o]=[c]

    for c in sample_pred.keys():
        for o in sample_pred[c]:
            origins[o].append(c)

    class_true=[]
    class_pred=[]

    for o in origins.keys():
        class_true.append(origins[o][0])
        class_pred.append(origins[o][1])

    accuracy = accuracy_score(class_true, class_pred)

    precision, recall, fscore, support = precision_recall_fscore_support(class_true, class_pred, average=average,warn_for=tuple())

    return (accuracy, precision, recall, fscore)

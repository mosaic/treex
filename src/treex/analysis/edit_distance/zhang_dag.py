# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.analysis.edit_distance.zhang_dag
#
#       File author(s):
#           Farah Ben-Naoum <farah.bennaoum@univ-sba.dz>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
    Called by this module: treex.tree, treex.dag, networkx, copy

    This module allows to compute Zhang edit distance between unordered trees

    - File author(s): Farah Ben-Naoum <farah.bennaoum@univ-sba.dz>
    - File contributor(s): Romain Azais <romain.azais@inria.fr>
    
"""

from treex.tree import *
from treex.lossless_compression.dag import *

import networkx as nx
from copy import copy, deepcopy
   
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

def __network_simplex_global(G , demand='demand', capacity='capacity', weight='weight', flow='flow'): # Private

    S=[]
    V=[]
    T={}
    for (i,j) in G.edges:
        G.add_edge(i, j, flow=0)
        V.append((i,j))
    
    inf=100000 ### Warning ###
    NOD=list(G.nodes)
    for i in NOD:
        if G.nodes[i]['demand']>=0:
            G.add_edge(i,-1, capacity=inf, weight=inf, flow=G.nodes[i]['demand'])
            T[(i,-1)]=G.nodes[i]['demand']
        else:
            G.add_edge(-1,i, capacity=inf, weight=inf, flow=0-G.nodes[i]['demand'])
            T[(-1,i)]=0-G.nodes[i]['demand']
            
    mincost,EG,pivot,T,S,V = __network_simplex_incremental(G,T,S,V)

    R=[]
    for (i,j) in V:
        if i==-1 or j==-1:
            R.append((i,j))
    for (i,j) in R:
            V.remove((i,j))
    R=[]
    for (i,j) in S:
        if i==-1 or j==-1:
            R.append((i,j))
    for (i,j) in R:
            S.remove((i,j))
    for (i,j) in list(T.keys()):
        if i==-1 or j==-1:
            T.pop((i,j))
    
    return mincost, EG, pivot, T, S, V
    

def __network_simplex_incremental(EG , T, S, V, demand='demand', capacity='capacity', weight='weight', flow='flow'): # Private
    # Finds a minimum cost flow satisfying all demands in digraph G.

    if len(list(EG.nodes)) == 0:
        raise nx.NetworkXError('graph has no nodes')

    N=list(EG.nodes)

    def __node_potentials(EG, T):
        Pi={}
        UI = []
        UJ = []
        CIJ=[]

        for (i,j) in T.keys():
            UI.append(i)
            UJ.append(j)
            CIJ.append(EG[i][j]['weight'])

        k=0
        while UI.count(UJ[k])>0 and k<len(UJ):
            k=k+1
        if UI.count(UJ[k])==0:
            Pi[UJ[k]]=0
        cpt=1
        k=0
        while cpt<len(N):
            if UI[k] in Pi.keys()and UJ[k] not in Pi.keys():
                Pi[UJ[k]]=Pi[UI[k]]-CIJ[k]
                cpt=cpt+1
            elif UI[k] not in Pi.keys()and UJ[k] in Pi.keys():
                Pi[UI[k]]=CIJ[k]+Pi[UJ[k]]
                cpt=cpt+1
            k=k+1
            if k==len(UI):
                k=0
        return Pi

    def __find_entering_edge(S, V, Pi, EG):
        opt=True
        i=0
        while opt and i<len(S):
            s=S[i]
            if Pi[s[0]]-Pi[s[1]]<EG[s[0]][s[1]]['weight']:
                opt=False
            else:
                i=i+1
        if not opt:
            return opt, S[i]
        else:
            i=0
            while opt and i<len(V):
                v=V[i]
                if Pi[v[0]]-Pi[v[1]]>EG[v[0]][v[1]]['weight']:
                    opt=False
                else:
                    i=i+1
            if not opt:
                return opt, V[i]
            else:
                return opt, ('-','-')

    def __find_cycle(e, T):
        L=[[e[0]]]
        stop=False
        while not stop:
            L2=[]
            for i in range(len(L)):
                c=L[i][len(L[i])-1]
                for j in T.keys():
                    if j[0]==c and j!=e and j[1] not in L[i]:
                        Li=L[i]+[j[1]]
                        L2.append(Li)
                        if j[1]==e[1]:
                            stop=True
                            return Li
                    elif j[1]==c and j!=e and j[0] not in L[i]:
                        Li=L[i]+[j[0]]
                        L2.append(Li)
                        if j[0]==e[1]:
                            stop=True
                            return Li
            L=L2   
                            

    def __find_leaving_edge(e, T, S, V, EG):
        C=__find_cycle(e, T)
        if e in S:
            d=[e]
            r=[]
            for i in range(len(C)-1):
                if (C[i],C[i+1]) in list(T.keys()):
                    r.append((C[i],C[i+1]))
                else:
                    d.append((C[i+1],C[i])) 
            
        elif e in V:
            r=[e]
            d=[]
            for i in range(len(C)-1):
                if (C[i],C[i+1]) in list(T.keys()):
                    d.append((C[i],C[i+1]))
                else:
                    r.append((C[i+1],C[i]))
        if len(r)==0:
            raise nx.NetworkXUnbounded('negative cycle with infinite capacity found')
        v=[]
        for i in range(len(d)):
            if d[i]!=e:
                v.append((EG[d[i][0]][d[i][1]]['flow'], d[i]))
        for i in range(len(r)):
            if r[i]!=e:
                v.append((EG[r[i][0]][r[i][1]]['capacity']-EG[r[i][0]][r[i][1]]['flow'], r[i]))
        v.sort()

        U=d[:]
        d=r[:]
        r=U[:]
                
        return v[0][0], v[0][1], d, r

    def __update_spanning_tree(val, e, ent, d, r, T, S, V, EG):
        T[ent]=EG[ent[0]][ent[1]]['flow']
        if ent in S:
            S.remove(ent)
        if ent in V:
            V.remove(ent)        
        
        for i in range(len(d)):
            T[d[i]]=T[d[i]]+val
            EG[d[i][0]][d[i][1]]['flow']=T[d[i]]
        
        for i in range(len(r)):
            T[r[i]]=T[r[i]]-val
            EG[r[i][0]][r[i][1]]['flow']=T[r[i]]

        a=T.pop(e)
        if a==0:
            V.append(e)
        elif a==EG[e[0]][e[1]]['capacity']:
            S.append(e)
        return T, S, V, EG

    OPT=False
    pivot=0
    while not OPT:
        pivot=pivot+1
        if T=={}:
            OPT=True
        else:
            Pi=__node_potentials(EG, T)
            OPT, ent = __find_entering_edge(S, V, Pi, EG)
            if not OPT:
                val, e, d, r = __find_leaving_edge(ent, T,S, V, EG)
                Tprec=T.copy()
                T, S, V, EG= __update_spanning_tree(val, e, ent, d, r, T, S, V, EG)               
                if T==Tprec:
                    break
    
    flow_cost=0
    E=list(EG.edges)
    for (i,j) in E:
        if i==-1 or j==-1:
            EG.remove_edge(i,j)
        else:
            a=EG[i][j]['flow']
            b=EG[i][j]['weight']
            flow_cost += a * b
    
    return flow_cost, EG, pivot, T, S, V

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

def __topological_order(G): # Private
    Marq=[]
    l=G.nodes()
    for n in G.nodes():
        if len(G.my_structure[n]['children'])==0:
            Marq.append(n)
            l.remove(n)
    while len(l)>0:
        lev=[]
        for n in l:
            S=G.my_structure[n]['children']
            j=0
            trouv=True
            while j<len(S) and trouv:
                if S[j] not in Marq:
                    trouv=False
                j=j+1
            if trouv:
                lev.append(n)
        Marq=Marq+lev
        for j in range(len(lev)):
            l.remove(lev[j])
    return Marq


def __size_tree(G,Ord): # Private
    i=0
    T=[]
    while i<len(Ord):
        s=1
        for n in G.my_structure[Ord[i]]['children']:
            j=Ord.index(n)
            s=s+T[j]
        T.append(s)
        i=i+1
    T.reverse()
    return T
    
def __dist_DAG_VIDE(T, sup_cost): # Private
    D=[]
    for i in range(len(T)):
        D=D+[T[i]*sup_cost]
    return D


def __dist_VIDE_DAG(T, adj_cost): # Private
    D=[]
    for i in range(len(T)):
        D=D+[T[i]*adj_cost]
    return D

def __dist_Forest_VIDE(T, sup_cost): # Private
    DF=[]
    for i in range(len(T)):
        DF=DF+[(T[i]-1)*sup_cost]
    return DF

def __dist_VIDE_Forest(T, adj_cost): # Private
    D=[]
    for i in range(len(T)):
        D=D+[(T[i]-1)*adj_cost]
    return D

def __init_matrix_distances(l1, l2): # Private
    # l1 and l2 are the length of the two dags
    M=[]
    for i in range(l1):
        l=[]
        for j in range(l2):
            l=l+[0]
        M=M+[l]
    return M

def __init_forest_distances(l1, l2): # Private
    # l1 and l2 are the length of the two dags
    F=[]
    for i in range(l1):
        l=[]
        for j in range(l2):
            l=l+[0]
        F=F+[l]
    return F

def __init_flow_graphs(l1, l2): # Private
    # l1 and l2 are the length of the two dags
    Flow=[]
    for i in range(l1):
        l=[]
        for j in range(l2):
            #l=l+[{}]
            l=l+[[]]
        Flow=Flow+[l]
    return Flow

def __matching_nodes_EK(D1, D2, DV1, DV2, Top1, Top2, M, i1, i2): # Private
    # D1 and D2 are two dags
    # i1 and i2 are two nodes in D1 and D2 respectively
    # M the distance matrix
    # DV1 and DV2 vectors of vertices distance between the empty tree and subtrees of D1 and D2
    
    G = nx.DiGraph()
    ind1 = {}
    ind2 = {}
    ind1[i1] = 1
    vide1 = 0
    vide2 = 0
    last_i = 0
    last_j = 0

    ch1=[]
    for n in D1.my_structure[i1]['children']:
        if n not in ch1:
            ch1.append(n)

    ch2=[]
    for n in D2.my_structure[i2]['children']:
        if n not in ch2:
            ch2.append(n)
    
    size = 2+len(ch1)+len(ch2)
           
    ni1 = len(D2.my_structure[i2]['children'])
    ni2 = len(D1.my_structure[i1]['children'])

    if ni1!=ni2:
        size=size+1

    ind2[size]=i2 
      
    i = 2
    for j in ch1:
        G.add_edge(1,i, capacity=D1.my_structure[i1]['children'].count(j), weight=0)
        ind1[i]=j
        last_i=i
        i=i+1
    
    if ni2<ni1:    
        G.add_edge(1,i, capacity=ni1-ni2, weight=0)
        vide1 = i
        i = i+1

    for j in ch2:
        G.add_edge(i,size, capacity=D2.my_structure[i2]['children'].count(j), weight=0)
        ind2[i] = j
        last_j = i
        i = i+1
    
    if ni1<ni2:
        G.add_edge(i,size, capacity=ni2-ni1, weight=0)
        vide2 = i
    
    i = 2
    while i<=last_i:
        if vide1==0:
            j=last_i+1
        else:
            j=vide1+1
        while j<=last_j:
            a=G[1][i].get('capacity')
            b=G[j][size].get('capacity')
            if a<b:
                c=a
            else:
                c=b            
            G.add_edge(i,j, capacity=c, weight=M[Top1.index(ind1[i])][Top2.index(ind2[j])])
            j=j+1
        i=i+1

    if vide1!=0:
        i=vide1
        j=i+1
        while j<=last_j:
            a=G[j][size].get('capacity')
            
            b=G[1][i].get('capacity')
            if a<b:
                c=a
            else:
                c=b            
            
            G.add_edge(i,j, capacity=c, weight=DV2[Top2.index(ind2[j])])
            j=j+1

    if vide2!=0:
        i=2
        j=vide2
        while i<=last_i:
            a=G[1][i].get('capacity')
            
            b=G[j][size].get('capacity')
            if a<b:
                c=a
            else:
                c=b            
            
            G.add_edge(i,j, capacity=c, weight=DV1[Top1.index(ind1[i])])
            i=i+1

    par = max(ni1,ni2)
    p = -par
    for i in G.nodes:
        if i==1:
            G.add_node(i, demand=p)
        elif i==size:
            G.add_node(i, demand=par)
        else:
            G.add_node(i, demand=0)

    if ni1==0 and ni2==0:
        flowCost=0
    else:
        flowCost, flowDict = nx.capacity_scaling(G)
                
    return  flowCost
        
def __matching_nodes_NS(D1, D2, DV1, DV2, Top1, Top2, M, Flow, i1, i2): # Private
    # D1 and D2 are two dags
    # i1 and i2 are two nodes in D1 and D2 respectively
    # M the distance matrix
    # DV1 and DV2 vectors of vertices distance between the empty tree and subtrees of D1 and D2
    G = nx.DiGraph()
    ind1 = {}
    ind2 = {}
    ind1[i1] = 1
    vide1 = 0
    vide2 = 0
    last_i = 0
    last_j = 0

    ch1=[]
    for n in D1.my_structure[i1]['children']:
        if n not in ch1:
            ch1.append(n)

    ch2=[]
    for n in D2.my_structure[i2]['children']:
        if n not in ch2:
            ch2.append(n)
    
    size = 2+len(ch1)+len(ch2)
           
    ni1 = len(D2.my_structure[i2]['children'])
    ni2 = len(D1.my_structure[i1]['children'])

    if ni1!=ni2:
        size=size+1

    ind2[size]=i2 
      
    i = 2
    for j in ch1:
        G.add_edge(1,i, capacity=D1.my_structure[i1]['children'].count(j), weight=0)
        ind1[i]=j
        last_i=i
        i=i+1
    
    if ni2<ni1:    
        G.add_edge(1,i, capacity=ni1-ni2, weight=0)
        vide1 = i
        i = i+1

    for j in ch2:
        G.add_edge(i,size, capacity=D2.my_structure[i2]['children'].count(j), weight=0)
        ind2[i] = j
        last_j = i
        i = i+1
    
    if ni1<ni2:
        G.add_edge(i,size, capacity=ni2-ni1, weight=0)
        vide2 = i
    
    i = 2
    while i<=last_i:
        if vide1==0:
            j=last_i+1
        else:
            j=vide1+1
        while j<=last_j:
            a=G[1][i].get('capacity')
            b=G[j][size].get('capacity')
            if a<b:
                c=a
            else:
                c=b            
            G.add_edge(i,j, capacity=c, weight=M[Top1.index(ind1[i])][Top2.index(ind2[j])])
            j=j+1
        i=i+1

    if vide1!=0:
        i=vide1
        j=i+1
        while j<=last_j:
            a=G[j][size].get('capacity')
            
            b=G[1][i].get('capacity')
            if a<b:
                c=a
            else:
                c=b            
            
            G.add_edge(i,j, capacity=c, weight=DV2[Top2.index(ind2[j])])
            j=j+1

    if vide2!=0:
        i=2
        j=vide2
        while i<=last_i:
            a=G[1][i].get('capacity')
            
            b=G[j][size].get('capacity')
            if a<b:
                c=a
            else:
                c=b            
            
            G.add_edge(i,j, capacity=c, weight=DV1[Top1.index(ind1[i])])
            i=i+1

    par = max(ni1,ni2)
    p = -par
    for i in G.nodes:
        if i==1:
            G.add_node(i, demand=par)
        elif i==size:
            G.add_node(i, demand=p)
        else:
            G.add_node(i, demand=0)
       
    if ni1==0 and ni2==0:
        mincost=0
        pivot=0
    else:
        mincost,EG,pivot,T,S,V = __network_simplex_global(G)
        
        g=[]
        for (i,j) in EG.edges:
            if i!=0 and j!=0:
                g.append((i,j, EG[i][j]['flow']))
        g.append(vide1)
        g.append(vide2)
        g.append((T,S,V))
        g.append(ind2)
        Flow[Top1.index(i1)][Top2.index(i2)]=g
                
    return Flow, mincost,pivot
        
def __dist_forest_nodes(D1, D2, DFV1, DFV2, Top1, Top2, F, mincost, i1, i2): # Private

    ch1=[]
    for n in D1.my_structure[i1]['children']:
        if n not in ch1:
            ch1.append(n)

    ch2=[]
    for n in D2.my_structure[i2]['children']:
        if n not in ch2:
            ch2.append(n)
    
    V = []
    for n in ch2:
        V.append(F[Top1.index(i1)][Top2.index(n)]-DFV2[Top2.index(n)])
    if V==[]:
        Cas1=DFV1[Top1.index(i1)]
    else:
        Cas1=DFV2[Top2.index(i2)]+min(V)

    V = []
    for n in ch1:
        V.append(F[Top1.index(n)][Top2.index(i2)]-DFV1[Top1.index(n)])
    if V==[]:
        Cas2=DFV2[Top2.index(i2)]
    else:
        Cas2=DFV1[Top1.index(i1)]+min(V)

    F[Top1.index(i1)][Top2.index(i2)]=min(Cas1, Cas2, mincost)

    return F

def __dist_nodes(D1, D2, DV1, DV2, Top1, Top2, M, F, i1, i2, cost_sup, cost_adj): # Private

    ch1=[]
    for n in D1.my_structure[i1]['children']:
        if n not in ch1:
            ch1.append(n)

    ch2=[]
    for n in D2.my_structure[i2]['children']:
        if n not in ch2:
            ch2.append(n)
    
    V = []
    for n in ch2:
        V.append(M[Top1.index(i1)][Top2.index(n)]-DV2[Top2.index(n)])
    if V==[]:
        Cas1=DV1[Top1.index(i1)]+cost_adj
    else:
        Cas1=DV2[Top2.index(i2)]+min(V)
    
    V = []
    for n in ch1:
        V.append(M[Top1.index(n)][Top2.index(i2)]-DV1[Top1.index(n)])
    if V==[]:
        Cas2=DV2[Top2.index(i2)]+cost_sup
    else:
        Cas2=DV1[Top1.index(i1)]+min(V)
    
    M[Top1.index(i1)][Top2.index(i2)]=min(Cas1, Cas2, F[Top1.index(i1)][Top2.index(i2)])

    return M

def __dag_matching(D1, D2, algo, cost_sup, cost_adj): # Private
    # Returns the distance matrix between D1 and D2
    # cost_sup: cost for deletion / cost_adj: cost for addition
    # algo: min-cost-flow resolution method, choose 'NS' for network simplex and 'EK' for Edmonds and Karp algorithm

    M = __init_matrix_distances(len(D1.nodes()), len(D2.nodes()))
    F = __init_forest_distances(len(D1.nodes()), len(D2.nodes()))
    Flow = __init_flow_graphs(len(D1.nodes()), len(D2.nodes()))
    
    Ord1 = __topological_order(D1)       
    Ord2 = __topological_order(D2)
        
    T1 = __size_tree(D1, Ord1)
    T2 = __size_tree(D2, Ord2)
    DV1 = __dist_DAG_VIDE(T1, cost_sup)
    DV2 = __dist_VIDE_DAG(T2, cost_adj)
    DFV1 = __dist_Forest_VIDE(T1, cost_sup)
    DFV2 = __dist_VIDE_Forest(T2, cost_adj)

    top1 = Ord1[:]
    top1.reverse()
   
    top2 = Ord2[:]
    top2.reverse()

    if algo=='NS':
        s_LL=0
        for n1 in Ord1:
            for n2 in Ord2:
                Flow,mincost,pivot = __matching_nodes_NS(D1, D2, DV1, DV2, top1, top2, M, Flow, n1, n2)
                s_LL += pivot
                F = __dist_forest_nodes(D1, D2, DFV1, DFV2, top1, top2, F, mincost, n1, n2)
                M = __dist_nodes(D1, D2, DV1, DV2, top1, top2, M, F, n1,n2, cost_sup, cost_adj)
                  
        return M, F, Flow, DV1, DFV1, DV2, Ord1, Ord2
    elif algo=='EK':
        for n1 in Ord1:
            for n2 in Ord2:
                mincost = __matching_nodes_EK(D1, D2, DV1, DV2, top1, top2, M, n1, n2)
                F = __dist_forest_nodes(D1, D2, DFV1, DFV2, top1, top2, F, mincost, n1, n2)
                M = __dist_nodes(D1, D2, DV1, DV2, top1, top2, M, F, n1,n2, cost_sup, cost_adj)
                  
        return M, F, DV1, DFV1, DV2, Ord1, Ord2

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

def zhang_edit_distance_dags(d1, d2, algo = 'NS', all_info = False):
    """
    Computes Zhang edit distance between obj1 and obj2

    Parameters
    ----------

    d1: Dag
        The first argument

    d2: Dag
        The second argument

    algo: str
        - 'NS': Network Simplex algorithm
	    - 'EK': Edmonds-Karp algorithm

    Returns
    -------

        - If all_info is set to False, the distance as an integer

        - If all_info is set to True, a dictionnary with main key 'distance'.
        The other keys correspond to the optimum flow that has been computed

    References
    ----------
    
    ZHANG "A constrained edit distance between unordered labeled trees"
    Algorithmica 1996
    
    """

    V = __dag_matching(d1, d2, algo, 1, 1)

    if all_info == False:
        out = V[0][0][0]

    else:
        if algo=='NS':
            out = {'distance': V[0][0][0], 'M':V[0], 'F':V[1], 'Flow':V[2], 'DV1':V[3],'DFV1':V[4], 'DV2':V[5], 'Ord1':V[6],'Ord2':V[7]}
        elif algo=='EK':
            out = {'distance': V[0][0][0], 'M':V[0], 'F':V[1], 'DV1':V[2],'DFV1':V[3], 'DV2':V[4], 'Ord1':V[5],'Ord2':V[6]}
    
    return out

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

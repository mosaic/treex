# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/height_process
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to compute the height process of a tree (and how to go back to the tree)
"""

from treex.tree import *
from treex.simulation import gen_random_tree
from treex.coding_process.heightprocess import tree_to_height_process, height_process_to_tree

######################################################################
def display_processus (aDictionaryFunction,aTitle='Display function'):
	# This function needs matplotlib and displays a dictionary that
	# represents a function
	from matplotlib import pyplot as plt
	plt.plot(aDictionaryFunction['x'],aDictionaryFunction['y'],'ro-')
	plt.title(aTitle)
	plt.show()

######################################################################

t=gen_random_tree(15)

print('-------------------------------------')

print("initial tree")
t.get_attribute()
t.treeprint('one attribute','height')

print('-------------------------------------')

print("Height process")
print("Kill the display window to continue")
heightprocess=tree_to_height_process(t)
print ("x  : ",heightprocess['x'])
print ("y  : ",heightprocess['y'])

display_processus(heightprocess,"Height process")

print('-------------------------------------')

print("Tree from previous Height process")
atree=height_process_to_tree(heightprocess)
atree.get_attribute()
atree.treeprint('one attribute','height')

# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.utils.kmedoid_clustering
#
#       File author(s):
#           salah eddine habibeche <salah-eddine.habibeche@inria.fr>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
#
# ------------------------------------------------------------------------------
"""
    for a set of data this appendix allows to:
         - calculate the medoid of cluster
         - sort the elements of a set by their centrality 
         - searches for the optimal k for a clustering which  maximizes the extra
           cluster variance and minimizes the intra cluster variance.
         - groups data  into k cluster 

    - File author(s): Salah eddine habibeche <salah-eddine.habibeche@inria.fr>
	- File contributor(s): Romain Azais <romain.azais@inria.fr>

    References
    ----------

    This code is an implementation of the k-medoids algorithm presented in
    presented in:

    Newling J, Fleuret F. A sub-quadratic exact medoid algorithm. In Artificial 
    Intelligence and Statistics 2017 Apr 10 (pp. 185-193).

    This version allows to sort the elements of a cluster by their centrality in this cluster
"""

import random
import math

def get_optimal_kmedoid_clustering(data,distance_matrix,kmaxratio=0.1):
    """
	This function cluster the data in k clusters with k varying between 1 and kmaxratio*size(data)
        then provide the optimal k and the clustering of data in optimal k clusters  

	Parameters
	----------
	data: list
	distance_matrix: dictionary
	kmaxratio: float

	Returns
	-------
	int
		the optimal k
        list
                the list of clusters

    """
    kmax=int(len(data)*kmaxratio)
    if (kmax<=1):
        return 1,kmedoid_clustering(1,data,distance_matrix)
 
    x=[]
    z=[]
    c=[]
    for k in range(1,kmax+1):
       
        x.append(k)
        clu=kmedoid_clustering(k,data,distance_matrix) 
        if(clu==False):
            break
        s=__evaluation_pseudo_f_index(clu,data,distance_matrix)
        c.append(clu)
        z.append(s[1]) 
  
    dif=[]  
    for i in range(0,len(x)-1):
        dif.append(z[i+1]-z[i])
    m=max(dif)
  
    
    return(x[dif.index(m)+1],c[dif.index(m)+1])

def kmedoid_clustering(k,data,distance_matrix):
         """
	    This function cluster the data in k clusters with k varying between 1 and kmaxratio*size(data)
            then provide the optimal k and the clustering of data in optimal k clusters  

       	    Parameters 
	    ----------
	    data: list
	    distance_matrix: dictionary
	    kmaxratio: float

	    Returns
	    -------
	    int
		the optimal k
            list
                the list of clusters

         """
  
         medoids=[]  

         
         
         while(len(medoids)<k):
            ti=random.choice(data)
            if ti not in medoids :                   
                 medoids.append(ti) 
        
         stop=bool(False) 
         it=0
         
         while(not stop and it<10): 
            
             clusters=[]   
             for i in range(k):   
                 clusters.append([]) 
             for i in  range(0,len(medoids)):  
                     clusters[i].append(medoids[i]) 

             for tr in data:
                  if(tr in medoids):
                        continue
                  li=[]   
                  for med in medoids:                
                      
                       li.append(distance_matrix[(tr,med)])    
                  if tr not in clusters[li.index(min(li))]: 
                      clusters[li.index(min(li))].append(tr)  
           
             mi=[]
             mj=[]
             for cl in clusters: 
                
                 mi.append(get_medoid_of_cluster(cl,distance_matrix)[0]) 
                 
                    
             if not __equiv_list(mi,medoids): 
                 medoids=mi 
                
                 it+=1
             else: 
                 stop=bool(True)
       
         return clusters,medoids

  
def get_medoid_of_cluster(cluster,distance_matrix):  
         #this function calculate the most central element of cluster and sort all the cluster elements by their centrallity in the cluster
         
         dicl={}  
         
         for tr in cluster:  
             dicl[tr]=0  
         mcl=-1  
         ecl=math.inf  
         for treei in cluster:  
             dj={}  
             somme=0  
             if dicl[treei]<ecl:  
                 for treej in cluster:  
                    dj[treej]=distance_matrix[(treei,treej)] 
                    somme+=dj[treej]  
                 dli={treei:somme/len(cluster)}  
                 dicl.update(dli)  
                 if(dicl[treei]<ecl):  
                     mcl=cluster.index(treei)  
                     ecl=dicl[treei]  
                 for treej in cluster:  
                     dicl[treej]=max(dicl[treej],dicl[treei]-dj[treej])  
         return cluster[mcl],dicl
def __equiv_list(u,v): 
         eq = bool(True) 
         for element in u: 
             if element not in v : 
                 eq=bool(False) 
                 break 
         return eq


def __evaluation_pseudo_f_index(clu,data,distance_matrix): 
        # this function evaluate the quality of clustering using the pseudo f-index mesure
        med=get_medoid_of_cluster(data,distance_matrix)[0] 
        medoids_list=clu[1] 
        cluster_list=clu[0] 
     
        
        ssb=0
        
        for cluster in cluster_list: 
            pi=(len(cluster))
            medoid=medoids_list[cluster_list.index(cluster)] 
            
            s=distance_matrix[(medoid,med)]
            ssb+=pi*(s**2) 
        ssw=0 

        for cluster in cluster_list: 
            
            s=0 
            medoid=medoids_list[cluster_list.index(cluster)]  
            for tree in cluster: 
                s+=distance_matrix[(medoid,tree)]**2 
            ssw+=s*(len(cluster)-1) 
        if(ssb==0 and ssw==0):
            return 1,1  
        r=ssb/(ssb+ssw)
     
        
        a=(len(clu)-1)
        b=(len(data)-len(clu))

        f=(ssb/a+1)/(ssw/b+1)

        return f,r



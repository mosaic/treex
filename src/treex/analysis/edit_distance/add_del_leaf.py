# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.analysis.edit_distance.add_del_leaf
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
    Called by this module: treex.tree, math, networkx

    This module allows to compute a constrained_edit_distance between
    unordered trees

    - File author(s): Romain Azais <romain.azais@inria.fr>

    References
    ----------

    This code is an implementation of the algorithm of edit distance
    presented in

    AZAIS, DURAND, and GODIN "Approximation of trees by self-nested trees"
    ALENEX 2019, San Diego (Ca)
    
"""

# --------------------------------------------------------------

from treex.tree import *
from treex.lossless_compression.dag import *

import math
import networkx as nx # Networkx # python3 -m pip install networkx

# --------------------------------------------------------------

def __const_un_ed(tree1,tree2): # Private
    # Requires that tree1 and tree2 have been labeled as subtrees of the same supertree
    if tree1.get_attribute('unordered_equivalence_class')==tree2.get_attribute('unordered_equivalence_class'):
        return 0
    else:
        dico1={}
        for child1 in tree1.my_children:
            if child1.get_attribute('unordered_equivalence_class') in dico1.keys():
                dico1[child1.get_attribute('unordered_equivalence_class')].append(child1)
            else:
                dico1[child1.get_attribute('unordered_equivalence_class')]=[child1]
        dico2={}
        for child2 in tree2.my_children:
            if child2.get_attribute('unordered_equivalence_class') in dico2.keys():
                dico2[child2.get_attribute('unordered_equivalence_class')].append(child2)
            else:
                dico2[child2.get_attribute('unordered_equivalence_class')]=[child2]

        n1=len(dico1.keys())
        n2=len(dico2.keys())

        N1=[len(dico1[list(dico1.keys())[i]]) for i in range(n1)]
        N1s=sum(N1)

        N2=[len(dico2[list(dico2.keys())[i]]) for i in range(n2)]
        N2s=sum(N2)

        # Construction of graph for max flow min cost algorithm
        # 0 source
        # (n1+n2+1) sink
        # (n1+n2+2) empty tree 1
        # (n1+n2+3) empty tree 2
        G = nx.DiGraph()
        G.add_edges_from([(0 , (n1+n2+2) ,{'capacity': N2s-min(N1s,N2s),'weight': 0})])
        G.add_edges_from([((n1+n2+3) , (n1+n2+1),{'capacity': N1s-min(N1s,N2s),'weight': 0})])

        for i in range(n1):
            G.add_edges_from([(0, (i+1), {'capacity': N1[i], 'weight': 0})])
            for j in range(n2):
                distance_a_calculer=__const_un_ed( dico1[list(dico1.keys())[i]][0] , dico2[list(dico2.keys())[j]][0] )
                G.add_edges_from([( (i+1) , (n1+j+1) , {'capacity': N1[i], 'weight': distance_a_calculer })])
            G.add_edges_from([( (i+1) , (n1+n2+3) , {'capacity': N1[i], 'weight': dico1[list(dico1.keys())[i]][0].get_property('size',compute_level=1) })])

        for j in range(n2):
            G.add_edges_from([( (n1+n2+2) , (n1+j+1) , {'capacity': N2s-min(N1s,N2s), 'weight': dico2[list(dico2.keys())[j]][0].get_property('size',compute_level=1) })])
            G.add_edges_from([( (n1+j+1) , (n1+n2+1) , {'capacity': N2[j], 'weight': 0})])

        mincostFlow = nx.max_flow_min_cost(G , 0 , (n1+n2+1))
        mincost = nx.cost_of_flow(G, mincostFlow)
        return mincost

# --------------------------------------------------------------

def __const_un_ed_costnot1(tree1,tree2,cost_function): # Private

    n1=len(tree1.my_children)
    n2=len(tree2.my_children)

    if n1==0 and n2==0:
        return cost_function(tree1,tree2)

    else:

        # Construction of graph for max flow min cost algorithm
        # 0 source
        # (n1+n2+1) sink
        # (n1+n2+2) empty tree 1
        # (n1+n2+3) empty tree 2
        G = nx.DiGraph()
        G.add_edges_from([(0 , (n1+n2+2) ,{'capacity': n2-min(n1,n2),'weight': 0})])
        G.add_edges_from([((n1+n2+3) , (n1+n2+1),{'capacity': n1-min(n1,n2),'weight': 0})])

        for i in range(n1):
            G.add_edges_from([(0, (i+1), {'capacity': 1, 'weight': 0})])
            for j in range(n2):
                distance_a_calculer=__const_un_ed_costnot1( tree1.my_children[i] , tree2.my_children[j] ,cost_function)

                G.add_edges_from([( (i+1) , (n1+j+1) , {'capacity': 1, 'weight': distance_a_calculer })])
            G.add_edges_from([( (i+1) , (n1+n2+3) , {'capacity': 1, 'weight': cost_function(tree1.my_children[i],None) })])

        for j in range(n2):
            G.add_edges_from([( (n1+n2+2) , (n1+j+1) , {'capacity': n2-min(n1,n2), 'weight': cost_function(tree2.my_children[j],None) })])
            G.add_edges_from([( (n1+j+1) , (n1+n2+1) , {'capacity': 1, 'weight': 0})])

        mincostFlow = nx.max_flow_min_cost(G , 0 , (n1+n2+1))
        mincost = nx.cost_of_flow(G, mincostFlow)

        return mincost

# --------------------------------------------------------------

def constrained_unordered_edit_distance(tree1,tree2,cost_function=None):
    """
    Compute the edit distance between two Tree class objects
    This edit distance is constrained because the only edit operations are
    add and delete leaves

    Parameters
    ----------
    tree1: Tree

    tree2: Tree

    cost_function: function, optional
        a cost function

    Returns
    -------
    int
        Edit distance between tree1 and tree2

    References
    ----------

    This code is an implementation of the algorithm of edit distance
    presented in AZAIS, DURAND, and GODIN "Approximation of trees by self-nested trees"
    ALENEX 2019, San Diego (Ca)

    See also
    --------
    constrained_tree_matching(...)

    """
    if cost_function==None:

        supertree=Tree()

        if isinstance(tree1,list):
            for t in tree1:
                supertree.my_children.append(t)
        else:
            supertree.my_children.append(tree1)

        if isinstance(tree2,list):
            for t in tree2:
                supertree.my_children.append(t)
        else:
            supertree.my_children.append(tree2)

        tree_to_dag(supertree)

        if isinstance(tree1,list)==False and isinstance(tree2,list)==False:
            return __const_un_ed(tree1,tree2)
        else:
            dico={}

            if isinstance(tree1,list)==True:

                if isinstance(tree2,list)==False:

                    for t1 in tree1:
                        dico[(t1.my_id,tree2.my_id)]=__const_un_ed(t1,tree2)

                else:
                    for t1 in tree1:
                        for t2 in tree2:
                            dico[(t1.my_id,t2.my_id)]=__const_un_ed(t1,t2)

            else:

                for t2 in tree2:
                    dico[(tree1.my_id,t2.my_id)]=__const_un_ed(tree1,t2)

            return dico

    else:

        if isinstance(tree1,list)==False and isinstance(tree2,list)==False:
            return __const_un_ed_costnot1(tree1,tree2,cost_function)

        else:

            dico={}

            if isinstance(tree1,list)==False:
                tree1_list=[tree1]
            else:
                tree1_list=tree1

            if isinstance(tree2,list)==False:
                tree2_list=[tree2]
            else:
                tree2_list=tree2

            for t1 in tree1_list:
                for t2 in tree2_list:
                    dico[(t1.my_id,t2.my_id)]=__const_un_ed_costnot1(t1,t2,cost_function)

        return dico

# --------------------------------------------------------------

def constrained_tree_matching(tree1,tree2,dico_matching={}):
    """
    Compute the matching between two Tree class objects
    This matching is constrained because the only edit operations are
    add and delete leaves

    Parameters
    ----------
    tree1: Tree

    tree2: Tree

    Returns
    -------
    dict
        A dictionary (id of nodes of tree1 - id of nodes of tree2)

    References
    ----------
    This code is an implementation of the algorithm of edit distance
    presented in AZAIS, DURAND, and GODIN "Approximation of trees by self-nested trees"
    ALENEX 2019, San Diego (Ca)

    See also
    --------
    constrained_edit_distance(...)

    """
    
    # Maps root to root
    dico_matching[tree1.my_id]=tree2.my_id

    matrice_distances=[]
    labels_enfants_1=[]
    labels_enfants_2=[]
    nnodes_tree1=[]
    nnodes_tree2=[]

    for ch1 in tree1.my_children:
        labels_enfants_1.append(ch1.my_id)
        nnodes_tree1.append(ch1.get_property('size',compute_level=1))

        ed_ch1_tree2_ch=constrained_unordered_edit_distance(ch1,tree2.my_children)

        liste_dist=[]
        for ch2 in tree2.my_children:
            liste_dist.append( ed_ch1_tree2_ch[(ch1.my_id , ch2.my_id)] )
        matrice_distances.append(liste_dist)

    for ch2 in tree2.my_children:
        labels_enfants_2.append(ch2.my_id)
        nnodes_tree2.append(ch2.get_property('size',compute_level=1))

    n1=len(tree1.my_children)
    n2=len(tree2.my_children)
    
    G = nx.DiGraph()

    for i in range(n1):

        G.add_edges_from([(0 , i+1 ,{'capacity': 1,'weight': 0})])

        for j in range(n2):

            G.add_edges_from([(i+1 , n1+1+j ,{'capacity': 1,'weight': matrice_distances[i][j]})])

    for j in range(n2):
        G.add_edges_from([(n1+1+j , 1+n1+n2 ,{'capacity': 1,'weight': 0})])

    G.add_edges_from([(0,n1+n2+2,{'capacity':max(n1,n2)-n1,'weight':0})])
    for j in range(n2):
        G.add_edges_from([(n1+n2+2,n1+1+j,{'capacity':1,'weight':nnodes_tree2[j]})])

    G.add_edges_from([(n1+n2+3,n1+n2+1,{'capacity':max(n1,n2)-n2,'weight':0})])
    for i in range(n1):
        G.add_edges_from([(i+1,n1+n2+3,{'capacity':1,'weight':nnodes_tree1[i]})])

    mincostFlow = nx.max_flow_min_cost(G , 0 , n1+n2+1)

    for i in range(n1):
        dic_i=mincostFlow[i+1]

        for j in dic_i.keys():
            if dic_i[j]==1:
                if j!=n1+n2+3:
                    dico_matching[labels_enfants_1[i]]=labels_enfants_2[j-n1-1]


    for ch1 in tree1.my_children:

        for ch2 in tree2.my_children:

            if ch1.my_id in dico_matching.keys():

                if dico_matching[ch1.my_id]==ch2.my_id:

                    constrained_tree_matching(ch1,ch2,dico_matching)

    return dico_matching

# --------------------------------------------------------------
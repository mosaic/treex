Welcome
====================================================

.. image:: https://anaconda.org/mosaic/treex/badges/version.svg
   :target: https://anaconda.org/mosaic/treex

.. image:: https://anaconda.org/mosaic/treex/badges/latest_release_date.svg
   :target: https://anaconda.org/mosaic/treex

.. image:: http://joss.theoj.org/papers/0800c3bd9442b5e21b607776d7df666a/status.svg
   :target: http://joss.theoj.org/papers/0800c3bd9442b5e21b607776d7df666a
   
.. image:: https://anaconda.org/mosaic/treex/badges/platforms.svg
   :target: https://anaconda.org/mosaic/treex

.. image:: https://anaconda.org/mosaic/treex/badges/license.svg
   :target: https://anaconda.org/mosaic/treex

.. image:: https://anaconda.org/mosaic/treex/badges/installer/conda.svg
   :target: https://conda.anaconda.org/mosaic

.. image:: https://anaconda.org/mosaic/treex/badges/downloads.svg
   :target: https://conda.anaconda.org/mosaic

.. image:: https://gitlab.inria.fr/mosaic/treex/badges/master/pipeline.svg
  :target: https://gitlab.inria.fr/mosaic/treex/commits/master

.. image:: https://gitlab.inria.fr/mosaic/treex/badges/master/coverage.svg
  :target: https://gitlab.inria.fr/mosaic/treex/commits/master

Contents
*****************

.. toctree::
    :maxdepth: 2

    readme
    installation
    example
    authors
    history

Indices and tables
***********************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_simulation/test_random
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.simulation.ranrut import __random_pair,__recursive_number_of_unordered_trees, gen_ranrut_tree
from treex.simulation.random_attributes import assign_gaussian_attribute_to_tree, add_gaussian_noise_to_attribute

from random import seed
import math
from copy import deepcopy

seed(1234)

def test_random_tree():
    t = gen_random_tree(100,4)
    assert t.get_property('size') == 100
    assert t.get_property('outdegree') <= 4

def test_random_tree():
    t = gen_random_tree(100,4,5)
    assert t.get_property('size') == 100
    assert t.get_property('outdegree') <= 4
    assert t.get_property('height')<=5

# --------------------------------------------------------------

def test_ranrut_random_pair():
    dico = {1: 1}
    n=6
    __recursive_number_of_unordered_trees(n, dico)

    dico_proba = {}

    for d in range(n-1,0,-1):
        for j in range(1,math.floor((n-1)/d)+1):
            dico_proba[(j,d)]=d*dico[n-j*d]*dico[d]/(dico[n]*(n-1))

    dico_freq = {}

    nsimu=10000

    for i in range(nsimu):
        tup = __random_pair(n,dico)
        if tup not in dico_freq.keys():
            dico_freq[tup]=1/nsimu
        else:
            dico_freq[tup]+=1/nsimu

    mc=0
    for tup in dico_proba.keys():
        mc+=(dico_proba[tup]-dico_freq[tup])**2

    assert mc<1e-3

def test_ranrut_tree():
    t = gen_ranrut_tree(100)
    assert t.get_property('size')==100

    n=5
    N=9
    nsimu= 1000
    dico_trees = {}
    dico_freq = {}
    i=0
    for simu in range(nsimu):
        t = gen_ranrut_tree(n)
        test=True
        for k in dico_trees.keys():
            if t.is_isomorphic_to(dico_trees[k]):
                dico_freq[k]+=1/nsimu
                test=False
        if test:
            dico_trees[i]=t.copy()
            dico_freq[i]=1/nsimu
            i+=1

    assert len(dico_freq)==N

    mc=0
    for k in dico_freq.keys():
        mc+=(dico_freq[k]-1/N)**2

    assert mc<1e-3


def test_ranrut_tree_1_or_2():
    t = gen_ranrut_tree(1)
    s = gen_ranrut_tree(2)
    assert t.get_property('size')+1 == s.get_property('size')
    
# --------------------------------------------------------------

def test_assign_gaussian_attribute_to_tree():
    t = gen_random_tree(10)
    assign_gaussian_attribute_to_tree(t , 'test1' , mean = 0 , sd=0 , dim = 1)
    assign_gaussian_attribute_to_tree(t , 'test2' , mean = 0 , sd=1 , dim = 2)
    assert t.get_attribute('test1') == 0
    assert len(t.get_attribute('test2')) == 2

def test_add_gaussian_noise_to_attribute_1():
    t = gen_random_tree(10)
    assign_gaussian_attribute_to_tree(t , 'test' , mean = 0 , sd=1 , dim = 1)
    u = deepcopy( t.get_attribute('test') )
    add_gaussian_noise_to_attribute(t , 'test' , pos = None , sd = 1)
    assert t.get_attribute('test') != u

def test_add_gaussian_noise_to_attribute_2():
    t = gen_random_tree(10)
    assign_gaussian_attribute_to_tree(t , 'test' , mean = 0 , sd=1 , dim = 3)
    u = deepcopy( t.get_attribute('test') )
    add_gaussian_noise_to_attribute(t , 'test' , pos = None , sd = 2)
    assert t.get_attribute('test') != u

def test_add_gaussian_noise_to_attribute_3():
    t = gen_random_tree(10)
    assign_gaussian_attribute_to_tree(t , 'test' , mean = 0 , sd=1 , dim = 4)
    u = deepcopy( t.get_attribute('test') )
    add_gaussian_noise_to_attribute(t , 'test' , pos = 0 , sd = 1)
    assert t.get_attribute('test')[0] != u[0] and t.get_attribute('test')[3] == u[3]
# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/create_read_tree

#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to construct and explore a tree
"""

from treex.tree import *

# Example 1
print('Example 1')
t = Tree()
s = Tree()
t.add_subtree(s)
print(t)
print(t.my_parent)
print(s.my_parent.my_id)

print('-------------------------------------')

# Example 2
print('Example 2')
t = Tree({'length':5})
s = Tree({'length':3})
t.add_subtree(s)

t.treeprint(ptype = 'one attribute',display = 'length')
t.treeprint(ptype = 'one attribute',display = 'width')

print('-------------------------------------')

# Example 3
print('Example 3')
t = Tree({'length':5,'width':3})
s = Tree({'length':3,'width':3})
t.add_subtree(s)
print(t)
t.treeprint(ptype = 'one attribute',display = 'length')
t.treeprint(ptype = 'one attribute',display = 'width')
t.treeprint(ptype = 'all attributes')

print('-------------------------------------')

# Example 4
print('Example 4')
t = Tree()
s = Tree()
t.add_subtree(s)
t.get_attribute()
t.treeprint(ptype = 'all attributes')

print('-------------------------------------')

# Example 5
print('Example 5')
t = Tree()
s1 = Tree()
s2 = Tree()
u11 = Tree()
u12 = Tree()
v21 = Tree()

t.add_subtree(s1)
t.add_subtree(s2)
s1.add_subtree(u11)
s1.add_subtree(u12)
s2.add_subtree(v21)

print(t)

t.delete_subtree(s2)

print(t)


s3 = Tree()
u31 = Tree()
s3.add_subtree(u31)

t.add_subtree_to_id(s3,t.my_id)

print(s3)
print(t)

t.delete_subtree_at_id( t.my_children[1].my_id )

print(t)

ids_t = t.list_of_ids()
for i in range(len(ids_t)):
	t.add_attribute_to_id('length',i,ids_t[i])
t.treeprint('all attributes')

print('New IDs:')
s = t.copy()
print(s)

print('-------------------------------------')

# Example 6
print('Example 6')

t = Tree()
s1 = Tree()
s2 = Tree()
u11 = Tree()
u12 = Tree()
v21 = Tree()
t.add_subtree(s1)
t.add_subtree(s2)
s1.add_subtree(u11)
s1.add_subtree(u12)
s2.add_subtree(v21)

a = Tree()
b1 = Tree()
b2 = Tree()
c11 = Tree()
c21 = Tree()
c22 = Tree()
a.add_subtree(b1)
a.add_subtree(b2)
b1.add_subtree(c11)
b2.add_subtree(c21)
b2.add_subtree(c22)

print(t)
print(a)

print(t.is_isomorphic_to(a,tree_type='ordered'))
print(t.is_isomorphic_to(a,tree_type='unordered'))

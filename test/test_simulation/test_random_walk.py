# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_simulation/test_random_walk
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.tree import *
from treex.lossless_compression.dag import *

from treex.simulation.random_recursive_trees import gen_random_tree

from treex.analysis.edit_distance.zhang_dag import *
from treex.analysis.edit_distance.incremental_zhang import *
from treex.simulation.random_walk import random_walk, back_edit_operation, random_edit_operation

import math

####################################################################################

def test_inc_add_leaf():
    t = Tree()
    t.add_subtree_to_id(Tree())
    t.add_subtree_to_id(Tree())

    d_t = tree_to_dag(t,compression = 'no_compression')

    zed = zhang_edit_distance_dags(d_t,d_t,algo='NS',all_info=True)
    inc_zed = incremental_zhang_edit_distance(d_t,d_t,{'operation_name':'add_leaf','node':2},zed,copy=False)

    assert len(inc_zed['edited_tree'].nodes()) == 4

def test_inc_del_leaf():
    t = Tree()
    t.add_subtree_to_id(Tree())
    t.add_subtree_to_id(Tree())
    t.add_subtree_to_id(Tree())

    d_t = tree_to_dag(t,compression = 'no_compression')
    zed = zhang_edit_distance_dags(d_t,d_t,algo='NS',all_info=True)
    inc_zed = incremental_zhang_edit_distance(d_t,d_t,{'operation_name':'del_leaf','node':0},zed,copy=False)

    assert len(inc_zed['edited_tree'].nodes()) == 3

####################################################################################

def test_invalid_operation():
    t = Tree()
    t.add_subtree_to_id(Tree())
    t.add_subtree_to_id(Tree())

    d_t = tree_to_dag(t,compression = 'no_compression')

    zed = zhang_edit_distance_dags(d_t,d_t,algo='NS',all_info=True)
    inc_zed = incremental_zhang_edit_distance(d_t,d_t,{'operation_name':'add_node','node':2},zed,copy=False)

    d_t2 = inc_zed['edited_tree']
    t2 = d_t2.to_tree()
    assert t.is_isomorphic_to(t2)

####################################################################################

def test_rw_1():
    t = gen_random_tree(10)
    rw = random_walk(t,t,10, [0.5,[0.6,0.4]] ,radius = math.inf,check = True)
    assert rw['errors'] == 0

def test_rw_2():
    t = gen_random_tree(10)
    rw = random_walk(t,t,10, 'uniform' ,radius = 3,check = True)
    assert rw['errors'] == 0
    for d in rw['distances']:
        assert d<=4

####################################################################################

def test_back_edit_operation():
    for i in range(25):
        t = gen_random_tree(15)
        s = gen_random_tree(15)

        d_t = tree_to_dag(t , compression = 'no_compression')
        d_s = tree_to_dag(s , compression = 'no_compression')
        zed_t_s = zhang_edit_distance_dags(d_t,d_s,algo='NS',all_info=True)

        rop = random_edit_operation(d_s,'uniform')
        zed_t_s = incremental_zhang_edit_distance(d_t,d_s,rop,zed_t_s,copy=False)

        d_s = zed_t_s['edited_tree']
        info = zed_t_s['info']

        bop = back_edit_operation(info)
        zed_t_s = incremental_zhang_edit_distance(d_t,d_s,bop,zed_t_s,copy=False)

        d_s = zed_t_s['edited_tree']
        s2 = d_s.to_tree()

        assert s.is_isomorphic_to(s2)

####################################################################################

def test_back_edit_operation():

    for i in range(10):
        t = gen_random_tree(10)
        s = gen_random_tree(10)

        d_t = tree_to_dag(t , compression = 'no_compression')
        d_s = tree_to_dag(s , compression = 'no_compression')
        zed_t_s = zhang_edit_distance_dags(d_t,d_s,algo='NS',all_info=True)

        rop = random_edit_operation(d_s,'uniform')
        zed_t_s = incremental_zhang_edit_distance(d_t,d_s,rop,zed_t_s,copy=False)

        d_s = zed_t_s['edited_tree']
        info = zed_t_s['info']

        bop = back_edit_operation(info)
        zed_t_s = incremental_zhang_edit_distance(d_t,d_s,bop,zed_t_s,copy=False)

        d_s = zed_t_s['edited_tree']
        s2 = d_s.to_tree()

        assert s.is_isomorphic_to(s2)

####################################################################################

def test_add_info():
    t = gen_random_tree(10)
    rw = random_walk(t,t,10, 'uniform' ,radius = math.inf, check = False)
    for i in range(10):
        d_s = rw['walk'][i+1]
        op = rw['editions'][i]
        if op['operation_name'] in ['add_leaf' , 'add_internal_node']:
            assert d_s.get_attribute(node = op['node'] , attribute = 'random_walk_info') == 'added_node'

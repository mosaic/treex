# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_visu_textplot
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.visualization.options import *
from treex.visualization.texplot import *

import treex
import os

def test_tree_to_tex():
    t = gen_random_tree(50)
    data_dir = 'tmp/test/'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    tree_to_tex(t,data_dir+"tree.tex",xscale=1,yscale=1,nodescale=1,direction='down')    
    assert os.path.getsize(data_dir+"tree.tex")

def test_tree_to_tex_list():
    t = gen_random_tree(20)
    s = gen_random_tree(20)

    data_dir = 'tmp/test/'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    tree_to_tex([t,s],data_dir+"tree.tex",xscale=1,yscale=1,nodescale=1,direction='down')    
    assert os.path.getsize(data_dir+"tree.tex")

    for file in os.listdir(data_dir):
        os.remove(data_dir+file)
    os.removedirs(data_dir)

def test_dag_to_tex1():
    t = gen_random_tree(20)
    d = tree_to_dag(t)

    data_dir = 'tmp/test/'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    dag_to_tex(d,data_dir+"dag.tex",xscale=1,yscale=1,nodescale=1,direction='down')    
    assert os.path.getsize(data_dir+"dag.tex")

    for file in os.listdir(data_dir):
        os.remove(data_dir+file)
    os.removedirs(data_dir)

def test_dag_to_tex2():
    t = gen_random_tree(20)
    d = tree_to_dag(t)
    assign_color_to_dag(d)
    assign_nodescale_to_dag(d)

    data_dir = 'tmp/test/'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    dag_to_tex(d,data_dir+"dag.tex",xscale=1,yscale=1,nodescale=1,direction='down')
    assert os.path.getsize(data_dir+"dag.tex")

    for file in os.listdir(data_dir):
        os.remove(data_dir+file)
    os.removedirs(data_dir)

def test_dag_to_tex_list():
    t = gen_random_tree(20)
    d = tree_to_dag(t)

    s = gen_random_tree(20)
    d2 = tree_to_dag(s)

    data_dir = 'tmp/test/'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    dag_to_tex([d,d2],data_dir+"dag.tex",xscale=1,yscale=1,nodescale=1,direction='down')    
    assert os.path.getsize(data_dir+"dag.tex")

    for file in os.listdir(data_dir):
        os.remove(data_dir+file)
    os.removedirs(data_dir)

def test_dag_to_tex_dir():
    t = gen_random_tree(20)
    d = tree_to_dag(t)

    data_dir = 'tmp/test/'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    dag_to_tex(d,data_dir+"dag_up.tex",xscale=1,yscale=1,nodescale=1,direction='up')
    dag_to_tex(d,data_dir+"dag_left.tex",xscale=1,yscale=1,nodescale=1,direction='left')
    dag_to_tex(d,data_dir+"dag_right.tex",xscale=1,yscale=1,nodescale=1,direction='right')

    assert os.path.getsize(data_dir+"dag_up.tex")
    assert os.path.getsize(data_dir+"dag_left.tex")
    assert os.path.getsize(data_dir+"dag_right.tex")

    for file in os.listdir(data_dir):
        os.remove(data_dir+file)
    os.removedirs(data_dir)
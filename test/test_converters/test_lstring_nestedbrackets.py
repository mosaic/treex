# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_converters/test_lstring_nestedbrackets
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.converters.lpy.lstring import *
from treex.converters.nestedbrackets import *
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.visualization.options import assign_random_color_to_tree

def test_lstr_to_tree1():
	lstr = '(A)[(B)[(C)]](A)'
	t = lstring_to_tree(lstr, header=None, flag_eval=False,node_par='()')
	assert t.get_property('size')==4

def test_lstr_to_tree2():
	lstr = '({"var":1})[({"var":2})[({"var":3})]]({"var":4})'
	t = lstring_to_tree(lstr, header=None, flag_eval=True,node_par='()')
	assert t.get_attribute('lstring_attribute')=={"var":1}

def test_lstr_to_tree3():
	lstr = '{"var":1}[{"var":2}[{"var":3}]]{"var":4}'
	t = lstring_to_tree(lstr, header=None, flag_eval=True,node_par='{}',include_node_par_in_attribute=True)
	assert t.get_attribute('lstring_attribute')=={"var":1}

def test_lstr_to_tree4():
	lstr = '{"var":1}[{"var":2}[{"var":3}]]{"var":4}'
	t = lstring_to_tree(lstr, header=None, flag_eval=False,node_par='{}',include_node_par_in_attribute=False)
	assert t.get_attribute('lstring_attribute')=='"var":1'

def test_lstr_to_tree5():
	modules = "begin modlist"+"\n"+"BB:Name(string),Diam(float)"+"\n"+"BBB1:"+"\n"+"Slash:divergence_angle(int)"+"\n"+"end modlist"
	lstr = "BB(2,7.2)[BB(3,8.0)BB(0,0.1)[BB(1,1.1)]BB(3,4.5)BB(8,3.0)[BB(5,7.3)BBB1]BB(1,2.7)]"
	t = lstring_to_tree(lstr, modules)

def test_lstr_to_tree6():
	modules = "begin modlist"+"\n"+"BB:Name(string),Diam(float)"+"\n"+"Slash:divergence_angle(int)"+"\n"+"end modlist"
	lstr = "[AAA(2,7.2)[BB(3)BB(0,0.1)[BB(1,1.1)]BB(3,4.5)U(3,2,1)BB(8,3.0)[BB(5,7.3)BBB1]BB(1,2.7)]]"
	t = lstring_to_tree(lstr, modules)

def test_lstr_to_tree7():
	modules = "begin modlist"+"\n"+"BB:Name(string),Diam(float)"+"\n"+"Slash:divergence_angle(int)"+"\n"+"end modlist"
	lstr = "[AAA(2,7.2)[BB(3)BB(0,0.1)[BB(1,1.1,4)]BB(3,4.5)U(3,2,1)BB(8,3.0)[BB(5,7.3)BBB1]BB(1,2.7)]]"
	t = lstring_to_tree(lstr, modules)

# --------------------------------------------------------

def test_tree_to_lstr1():
	t1 = gen_random_tree(15)
	assign_axis_to_tree(t1 , 'lstring_successor')
	s1 = tree_to_lstring(t1,node_par='{}',attribute_name=None)
	t2 = lstring_to_tree(s1)
	assert t1.is_isomorphic_to(t2)

def test_tree_to_lstr2():
	t1 = gen_random_tree(15)
	assign_axis_to_tree(t1 , 'lstring_successor' )
	s1 = tree_to_lstring(t1,node_par='{}',attribute_name='hello')
	t2 = lstring_to_tree(s1)
	assert t1.is_isomorphic_to(t2)

def test_tree_to_lstr3():
	t1 = Tree()
	t1.add_attribute_to_id('lstring_successor',True)
	s1 = tree_to_lstring(t1,node_par='{}',attribute_name='size')
	t2 = lstring_to_tree(s1)
	s2 = tree_to_lstring(t2,node_par=None)
	t3 = lstring_to_tree(s2)
	assert t1.is_isomorphic_to(t2) and t1.is_isomorphic_to(t3)

def test_tree_to_lstr4():
	t = Tree()
	t.add_attribute_to_id('lstring_successor',True)
	t.add_attribute_to_id('lstring_attribute','{}')

	t.add_subtree(Tree())
	t.my_children[0].add_attribute_to_id('lstring_successor',True)
	t.my_children[0].add_attribute_to_id('lstring_attribute','{}')

	s = tree_to_lstring(t,node_par=None)
	t2 = lstring_to_tree(s)
	assert t.is_isomorphic_to(t2)

def test_tree_to_lstr5():
	t = Tree()
	t.add_attribute_to_id('lstring_successor',True)
	t.add_attribute_to_id('lstring_attribute','{}')

	t.add_subtree(Tree())
	t.my_children[0].add_attribute_to_id('lstring_successor',False)
	t.my_children[0].add_attribute_to_id('lstring_attribute','{}')
	
	s = tree_to_lstring(t,node_par=None)
	t2 = lstring_to_tree(s)
	assert t.is_isomorphic_to(t2)

def test_tree_to_lstr6():
	header = "begin modlist"+"\n"+"BB:Name(),Diam(float)"+"\n"+"AA:divergence_angle(int)"+"\n"+"end modlist"
	header += "\n"+"begin comment"+"\n"+"This is a test."+"\n"+"end comment"
	lstr = "AA(2)[BB(3,2.0)BB(0,0.1)[BB(1,1.1)]AA(4.5)BB(3,2)BB(8,3.0)[BB(5,7.3)BB(0.0,0.1)]BB(1,2.7)]"
	t = lstring_to_tree(lstr, header)
	lstr_back = tree_to_lstring(t , header = header)
	assert lstr == lstr_back
	assert 'lstring_comment' in t.get_attribute()

# --------------------------------------------------------

def test_parstr_to_tree1():
	parstr = '[A[B[D]][C]]'
	t = nestedbrackets_to_tree(parstr,flag_eval=False)
	assert t.get_property('size')==4

def test_parstr_to_tree2():
	parstr = '[{"var":2} [{"var":5}][{"var":5}[{"var":3}]]]'
	t = nestedbrackets_to_tree(parstr,flag_eval=True)
	assert t.get_attribute('nestedbrackets_attribute')=={"var":2}

# --------------------------------------------------------

def test_tree_to_parstr1():
	t = gen_random_tree(15)
	s = tree_to_nestedbrackets(t,attribute_name=None)
	t2 = nestedbrackets_to_tree(s)
	assert t.is_isomorphic_to(t2,tree_type='ordered')

def test_tree_to_parstr2():
	t = gen_random_tree(15)
	assign_random_color_to_tree(t,['blue','red'])
	s = tree_to_nestedbrackets(t,attribute_name='color_plotgraph')
	t2 = nestedbrackets_to_tree(s,flag_eval=False)
	assert t.is_isomorphic_to(t2,tree_type='ordered')

def test_tree_to_parstr3():
	t = gen_random_tree(15)
	s = tree_to_nestedbrackets(t,attribute_name='size')
	t2 = nestedbrackets_to_tree(s)
	assert t.is_isomorphic_to(t2,tree_type='ordered')
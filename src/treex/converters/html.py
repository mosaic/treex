# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.converters.html
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
- File author(s): Romain Azais <romain.azais@inria.fr>
"""

from treex.tree import *
from urllib.request import Request, urlopen
from copy import copy

def __html_operation_contour(mem,indic,current_tag): # Private
    if indic=='add':
        t = Tree()
        t.add_attribute_to_id('HTML_tag',current_tag)
        mem[-1].add_subtree(t)
        mem.append(mem[-1].my_children[-1])
    else:
        mem.pop()

def __html_contour_to_tree(contour , list_tags):
    if list_tags != None:
        t = Tree()
        t.add_attribute_to_id('HTML_tag',list_tags[0])
    else:
        t = Tree()

    memlist=[t]
    compteur = 1
    for k in range(2,len(contour)):
        if contour[k]-contour[k-1]>0:
            if list_tags != None:
                current_tag = list_tags[compteur]
            else:
                current_tag = None
            compteur += 1
            __html_operation_contour(memlist,'add',current_tag)
        else:
            __html_operation_contour(memlist,'pass',None)
    return t

# ------------------------------------------------------------------------------

def htmlstring_to_tree(string , correction = True):

    data = string

    # Learn HTML tags
    liste_balise=[]
    for i in range(len(data)-1):
        if data[i:i+2]=='</':
            index=data[i+2:].find('>')
            balise=data[i:i+3+index]
            balise=balise.replace('</','')
            balise=balise.replace('>','')
            if balise not in liste_balise:
                liste_balise.append(balise)

    # Build contour function
    contour=[0]
    list_tags = []

    for i in range(len(data)-1):
        if data[i]=='<':
            index1=data[i:].find('>')
            candidate=data[i:i+1+index1]

            if candidate[1]!='/':
                if ' ' in candidate:
                    index2=candidate.find(' ')
                else:
                    index2=len(candidate)-1
                texte=candidate[1:index2]   
                if texte in liste_balise:
                    contour.append(contour[-1]+1)
                    list_tags.append(texte)
            else:
                texte=candidate.replace('</','')
                texte=texte.replace('>','')
                if texte in liste_balise:
                    contour.append(contour[-1]-1)

    correction_done = False
    if correction: # correction is allowed

        ### Missing closing brackets can not be seen by the algo above ###
        # if contour[-1]>0: # missing closing brackets
        #     correction_done = True
        #     for i in range(contour[-1]):
        #         contour.append(contour[-1]-1)

        if contour[-1]<0: # missing opening brackets
            correction_done = True

            new_contour=list(range(-contour[-1]))
            for i in range(len(contour)):
                contour[i]-=contour[-1]

            new_contour.extend(contour)
            contour=new_contour

    if correction_done:
        print('A correction of the structure has been applied')
        return __html_contour_to_tree(contour , None)
    else:
        return __html_contour_to_tree(contour , list_tags)

# ------------------------------------------------------------------------------

def url_to_tree(url , correction = True):
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    page = urlopen(req)
    data=str(page.read())
    return htmlstring_to_tree(data , correction)

# ------------------------------------------------------------------------------
# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_lossless_compression/test_dag
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.lossless_compression.dag import *

def test_tree_to_treedag():
    t = gen_random_tree(100,4)
    d = tree_to_dag(t,compression='no_compression')
    assert t.get_property('size') == d.get_property(d.my_roots[0],'size')

def test_tree_to_optimaldag():
    t = gen_random_tree(100,4)
    d = tree_to_dag(t)
    assert t.get_property('size') >= len( d.nodes() )

def test_tree_to_optimaldag_o_vs_uno():
    t = gen_random_tree(100,4)
    d_uno = tree_to_dag(t,tree_type='unordered')
    d_o = tree_to_dag(t,tree_type='ordered')
    assert len(d_uno.nodes()) <= len( d_o.nodes() )

def test_to_tree_uno():
    t = gen_random_tree(100,4)
    d = tree_to_dag(t,tree_type='unordered')
    t_bis = d.to_tree()
    assert t.is_isomorphic_to(t_bis)

def test_to_tree_o():
    t = gen_random_tree(50,7)
    d = tree_to_dag(t,tree_type='ordered')
    t_bis = d.to_tree()

    assert t.is_isomorphic_to(t_bis,'ordered')

def test_compress():
	t = gen_random_tree(50,5)
	d = tree_to_dag(t,compression='none')
	d.compress()
	assert len(d.nodes())<=t.get_property('size')

def test_nodes_sorted_by1():
	t = gen_random_tree(50,7)
	d = tree_to_dag(t,tree_type='ordered')
	dico = d.nodes_sorted_by('height')
	assert isinstance(dico,dict)

def test_nodes_sorted_by2():
	t = gen_random_tree(50)
	d = tree_to_dag(t)
	for node in d.nodes():
		d.add_attribute_to_node('label','a',node)
	dico=d.nodes_sorted_by(attributes='label')
	assert len(dico['a'])==len(d.nodes())

def test_copy():
	t = gen_random_tree(50,7)
	d = tree_to_dag(t,tree_type='unordered')
	d2 = d.copy()
	assert len(d2.nodes())==len(d.nodes())

def test_get_property():
	t = gen_random_tree(50,7)
	d = tree_to_dag(t,tree_type='unordered')
	assert 'height' in d.get_property()
	a = d.get_property(100)
	assert a==None
	a= d.get_property(prop='coucou')
	assert a==None

def test_get_attribute2():
	t = gen_random_tree(50,7)
	d = tree_to_dag(t,tree_type='ordered')
	a = d.get_attribute(attribute = 'attr')
	assert a==None

def test_assign_root():
	t = gen_random_tree(50,7)
	d = tree_to_dag(t,tree_type='unordered')
	r = d.my_roots[0]
	d.assign_roots()
	assert r == d.my_roots[0]

def test_descendants():
	t = gen_random_tree(50,7)
	d = tree_to_dag(t,tree_type='unordered')
	assert len(d.descendants(d.my_roots[0])) == len(d.nodes())-1

def test_sub_dag():
	t = gen_random_tree(50,7)
	d = tree_to_dag(t,tree_type='unordered')
	sd = d.sub_dag(d.nodes()[int(len(d.nodes())/2)])
	assert len(sd.nodes())<=len(d.nodes())

def test_add_attribute():
	t = gen_random_tree(50,7)
	d = tree_to_dag(t,tree_type='unordered')

	d.add_attribute_to_node('attr',-1,d.my_leaves[0])

	assert d.get_attribute(d.my_leaves[0],'attr')==-1

def test_erase_attribute():
	t = gen_random_tree(50,7)
	d = tree_to_dag(t,tree_type='unordered')

	d.add_attribute_to_node('attr',-1,d.my_leaves[0])
	d.erase_attribute('attr')
	assert 'attr' not in d.get_attribute(node = d.my_leaves[0])
	a=d.erase_attribute_at_node('attr', 100)
	assert a==None
	a=d.add_attribute_to_node('attr',1,100)
	assert a==None

def test_string_dag():
	t = gen_random_tree(50,7)
	d = tree_to_dag(t,tree_type='unordered')
	assert isinstance( d._Dag__string_dag() , str)

def test_node_not_in_dag():
	t = gen_random_tree(10)
	d = tree_to_dag(t)
	a = d._Dag__my_attributes(-1)
	assert a==None

def test_renumber1():
	t = gen_random_tree(10)
	d = tree_to_dag(t)
	nodes=d.nodes()
	numbers=[i+15 for i in nodes]
	d.renumber_nodes([nodes,numbers])
	assert d.nodes()==numbers

def test_renumber2():
	t = gen_random_tree(10)
	d = tree_to_dag(t)
	nodes=d.nodes()
	d.renumber_nodes('canonical')
	assert d.nodes()==nodes

def test_renumber3():
	t = gen_random_tree(10)
	d = tree_to_dag(t)
	nodes=d.nodes()
	numbers=[i+15 for i in nodes]
	d.renumber_nodes([nodes,numbers])
	d.renumber_nodes()
	assert d.nodes()==nodes

def test_concatenate():
	forest=[gen_random_tree(10) for i in range(3)]
	d=Dag()
	d.concatenate(forest)
	assert len(d.my_roots)==3

def test_concatenate2():
	forest=[gen_random_tree(50) for i in range(3)]
	t=gen_random_tree(15)
	dag=tree_to_dag(t)
	forest.append(dag)
	forest.append(t.copy())
	d=Dag()
	d.concatenate(forest)
	d.compress()
	assert len(d.my_roots)==4 and len(d.my_origins.keys())==5

def test_concatenate3():
	t1=gen_random_tree(10)
	t2=gen_random_tree(10)
	d1 = tree_to_dag(t1)
	n=len(d1.nodes())
	d2=tree_to_dag(t2)
	m=len(d2.nodes())
	d1.concatenate(d2)
	assert len(d1.nodes())==n+m

def test_compress_with_attribute():
	t1=gen_random_tree(10)
	t2=gen_random_tree(10)
	d1=tree_to_dag(t1)
	for node in d1.nodes():
		d1.add_attribute_to_node('label','a',node)
	d2=tree_to_dag(t2)
	for node in d2.nodes():
		d2.add_attribute_to_node('label','b',node)
	d=Dag()
	d.concatenate([d1,d2])
	d.compress(attribute='label')
	assert len(d.nodes())==len(d1.nodes())+len(d2.nodes())

def test_propagate_origins():
	forest=[gen_random_tree(10) for i in range(3)]
	forest.append(forest[0].copy())
	d=Dag()
	d.concatenate(forest)
	d.compress()
	assert set(d.get_attribute(d.my_leaves[0],'origin'))==set(d.my_origins.keys())

def test_get_attribute3():
	forest=[gen_random_tree(10) for i in range(3)]
	d=Dag()
	d.concatenate(forest)
	assert len(d.get_attribute().keys())==3

def test_origin():
	t = gen_random_tree(10)
	d = tree_to_dag(t,origin='test')
	assert 'test' in d.my_origins.keys()

def test_assign_origin():
	t = gen_random_tree(10)
	d = tree_to_dag(t)
	d.assign_origin('test')
	assert 'test' in d.my_origins.keys() and str(t.my_id) not in d.my_origins.keys()

def test_compute_presence():
	t = gen_random_tree(50)
	d = tree_to_dag(t)
	s =0
	for node in d.nodes():
		s+=d.get_attribute(node,'origin')[str(t.my_id)]
	assert s==50

def test_assign_ids_from_tree():
	t = gen_random_tree(50)
	d = tree_to_dag(t)
	d.assign_ids_from_tree(t)
	assert sum([len(d.get_attribute(node, 'tree_ids')) for node in d.nodes()]) == t.get_property('size')
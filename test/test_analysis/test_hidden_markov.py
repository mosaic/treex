# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_analysis/test_hidden_markov
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex import *
from treex.simulation.hidden_markov import *
from treex.analysis.hidden_markov import viterbi
import math

initial_distribution = [0.3,0.4,0.3]
transition_matrix = [[0.7,0.2,0.1],[0.1,0.1,0.8],[0.1,0.8,0.1]]

parameters = [[0,0.5],[1,1],[3,1.5]]
def gen_emission(k , parameters): # Gaussian emission
 	return random.gauss(parameters[k][0],parameters[k][1])
 
def pdf_emission(x , k , parameters): # Gaussian emission
	return 1.0/(parameters[k][1]*math.sqrt(2*math.pi))*math.exp(-1.0/(2*parameters[k][1]**2)*(x-parameters[k][0])**2)


def test_hmt_viterbi():
	t = gen_hidden_markov_tree(50 , initial_distribution , transition_matrix , gen_emission , parameters , type_name = 'A' , obs_name = 'B')
	viterbi(t , 'B' , initial_distribution , transition_matrix , pdf_emission , parameters)

	assert t.get_attribute('viterbi_probability')>=0
	assert 'viterbi_type' in t.get_attribute()
	assert 'estimated_type_obs_only' in t.get_attribute()
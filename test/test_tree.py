# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_tree
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.tree import *
from treex.simulation.random_recursive_trees import gen_random_tree

import random

def test_get_property():
    t = gen_random_tree(10)
    t.get_property()
    assert t.get_property('size') == 10
    assert t.get_property('dog') == None

def test_rebuild():
    t = gen_random_tree(10)
    s = t.rebuild()['tree']
    assert t.is_isomorphic_to(s)

def test_copy():
    t = gen_random_tree(50,4)
    s = t.copy()
    assert t.get_property(compute_level=1) == s.get_property(compute_level=1)

def test_isomorphism():
    t1 = gen_random_tree(50)
    t2 = t1.copy()
    t3 = gen_random_tree(50)
    assert t1.is_isomorphic_to(t2)
    assert not t1.is_isomorphic_to(t3)

    bool, mapp = t1.is_isomorphic_to(t2,mapping=True)
    assert len(mapp)==50

def test_sort_subtrees_by():
    t = gen_random_tree(30,3)
    s = t.copy()
    t.sort_subtrees_by_property('height')
    assert t.is_isomorphic_to(s)

def test_add_delete_subtree():
    t = Tree()
    s = gen_random_tree(10)
    t.add_subtree(s)
    t.delete_subtree(s)
    assert t.get_property('size')==1

def test_list_of_subtrees():
    t = Tree()
    assert len( t.list_of_subtrees() ) == t.get_property('size')

def test_del_subtree_at_id():
    t = gen_random_tree(50)
    l_id = t.list_of_ids()
    n_id = l_id[ int( len(l_id)/2 ) ]
    t.delete_subtree_at_id(n_id)
    assert t.get_property('size')<=50

def test_add_subtree_to_id():
    t = gen_random_tree(50)
    l_id = t.list_of_ids()
    n_id = l_id[ int( len(l_id)/2 ) ]
    t.add_subtree_to_id(Tree() , n_id)
    assert t.get_property('size')==51

def test_add_attribute_to_id1():
    t = gen_random_tree(20)
    l_id = t.list_of_ids()
    n_id = l_id[ int( len(l_id)/2 ) ]

    t.add_attribute_to_id('attr',3,n_id)

    u=t.subtree(node = n_id)
    assert u.get_attribute('attr')==3

def test_add_attribute_to_id2():
    t = gen_random_tree(20)
    t.add_attribute_to_id(['attr1','attr2'],[3,4])
    assert t.get_attribute('attr1')==3
    assert t.get_attribute('attr2') == 4

def test_erase_attribute_at_id():
    t = gen_random_tree(20)
    l_id = t.list_of_ids()
    n_id = l_id[ int( len(l_id)/2 ) ]

    t.add_attribute_to_id('attr',3,n_id)
    t.erase_attribute_at_id('attr',n_id)

    u=t.subtree(node = n_id)
    assert 'attr' not in u.get_attribute()

def test_subtrees_sorted_by1():
    t = gen_random_tree(30)
    dic = t.subtrees_sorted_by('height')
    assert len(dic) == t.get_property('height')+1

def test_subtrees_sorted_by2():
    t = gen_random_tree(30)
    dic = t.subtrees_sorted_by(['height','size'])
    assert isinstance(dic[0],dict)

# ------------------------------------------------------------------------------

def give_random_label(tree,label_name):
    tree.add_attribute_to_id(label_name,random.randint(0,2))
    for child in tree.my_children:
        give_random_label(child,label_name)

def test_subtrees_sorted_by3():
    t = gen_random_tree(30)
    give_random_label(t,'label')
    dic = t.subtrees_sorted_by(attributes='label')
    s=t.get_property('size')
    assert len(dic) == 3
    t.sort_subtrees_by_attribute('label')
    assert t.get_property('size',True)==s

def test_subtrees_sorted_by4():
    t = gen_random_tree(30)
    give_random_label(t,'label1')
    give_random_label(t,'label2')
    dic = t.subtrees_sorted_by(attributes=['label1','label2'])
    assert isinstance(dic[0],dict)

def test_subtrees_sorted_by5():
    t = gen_random_tree(30)
    give_random_label(t,'label')
    dic = t.subtrees_sorted_by('height','label')
    assert isinstance(dic[0],dict)

# ------------------------------------------------------------------------------

def test_depth():
    t = gen_random_tree(20)
    dic = t.subtrees_sorted_by('height')
    s = dic[0][0]
    assert s.get_property('depth')>0

def test_string_tree1():
    t = gen_random_tree(20)
    t.get_property('height')
    print(t)

    t.treeprint('all_properties')
    s1 = t._Tree__string_tree_all_properties()

    t.treeprint('concatenate')
    s2 = t._Tree__concatenate_string_tree_one_property()

    t.treeprint('one_property')
    s3 = t._Tree__string_tree_one_property()

    t.treeprint('')
    s4 = t._Tree__string_tree_id()
    assert isinstance(s1+s2+s3+s4,str)

def test_string_tree2():
    t = gen_random_tree(20)
    t.treeprint('concatenate')
    s = t._Tree__concatenate_string_tree_one_property()
    assert isinstance(s,str)

# ------------------------------------------------------------------------------

def test_subtree():
    t = gen_random_tree(20)
    s = t.subtree(node=None)
    assert t.is_isomorphic_to(s)

# ------------------------------------------------------------------------------

def test_ids():

    lst_ids = []
    for i in range(100):
        t = gen_random_tree(100)
    
        for st in t.list_of_subtrees():
            lst_ids.append(st.my_id)

    assert len(lst_ids)==len(set(lst_ids))
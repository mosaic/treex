# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.visualization.matplotlib_plot
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

""" 
    Called by this module : treex.tree, treex.dag, random

    This module implements functions that represent a tree structure in a Matplotlib figure.

    - File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>

"""

from treex.visualization.texplot import __coord_treegraph, __coord_dag

import warnings
import math

try:
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import cm
    import matplotlib.patches as patch
    from matplotlib.path import Path
    from matplotlib.colors import Normalize
except:
    warnings.warn("Warning: Matplotlib and NumPy should be installed for visualization functionalities")

else:

    def __matplotlib_figure(figure=None,bg='white'): # Private
        if figure is None:
            figure = plt.figure()
            figure.clf()
            if bg!='white':
                figure.patch.set_facecolor('black')
            else:
                figure.patch.set_facecolor('w')
        return figure


    def view_tree(tree, figure=None, direction='down', edgecolor='k', node_scale=1, linewidth=1, alpha=1, bg='white', print_attributes=False):
        """
        This function displays a Tree object in a Matplotlib figure

        Parameters
        ----------
        tree: Tree

        figure: Matplotlib figure object, optional
            The figure in which to display the tree

        direction: str, optional
            The direction where children should be placed relatively to their
            parent. Possibilities are 'up', 'down', 'left'. Any other string is
            interpreted as 'right'. Default is 'down'

        edgecolor: str or hexadecimal color code or ...
            The lines color

        node_scale: float

        linewidth: float

        alpha: float

        bg: string

        Returns
        -------
        Matplotlib figure

        See Also
        --------
        treex.vizualisation.texplot.tree_to_tex

        """

        graph_coords = __coord_treegraph(tree, direction)

        figure = __matplotlib_figure(figure,bg)

        node_points = dict([(node_id,graph_coords[node_id][:2]) for node_id in graph_coords.keys()])
        node_colors = dict([(node_id,graph_coords[node_id][3]) for node_id in graph_coords.keys()])
        node_sizes = dict([(node_id,graph_coords[node_id][4]) for node_id in graph_coords.keys()])
        node_linewidths = dict([(node_id,graph_coords[node_id][5]) for node_id in graph_coords.keys()]) ### Feb 5 2020 # add linewidths ###

        figure.gca().scatter([node_points[node_id][0] for node_id in graph_coords.keys()],
                             [node_points[node_id][1] for node_id in graph_coords.keys()],
                             c=[node_colors[node_id] for node_id in graph_coords.keys()],
                             s=[node_scale*node_sizes[node_id] for node_id in graph_coords.keys()],
                             edgecolor=edgecolor,
                             linewidth=linewidth,
                             zorder=5,
                             picker=True)

        if len(node_points)>1:
            graph_edges = np.array([(node_points[node_id],node_points[child_id]) for node_id in graph_coords.keys() if len(graph_coords[node_id][2])>0 for child_id in graph_coords[node_id][2]])
            width_edges = [linewidth*node_linewidths[child_id] for node_id in graph_coords.keys() if len(graph_coords[node_id][2])>0 for child_id in graph_coords[node_id][2]]

            for l, w in zip(graph_edges,width_edges):
                figure.gca().plot(l[:,0],l[:,1],color=edgecolor, linewidth=w, alpha=alpha, zorder=4)

        if print_attributes:
            def on_pick(event):
                node_id = list(graph_coords.keys())[event.ind[0]]
                t = tree.subtree(node_id)
                print(t.get_attribute())

            figure.canvas.mpl_connect('pick_event', on_pick)


        figure.gca().axis('off')

        return figure

    def view_dag(dag, figure=None, direction='down', edgecolor='k', node_scale=1, linewidth=1, alpha=1, bg='white'):
        """
        This function displays a Dag object in a Matplotlib figure

        Parameters
        ----------
        dag: Dag

        figure: Matplotlib figure object, optional
            The figure in which to display the tree

        direction: str, optional
            The direction where children should be placed relatively to their
            parent. Possibilities are 'up', 'down', 'left'. Any other string is
            interpreted as 'right'. Default is 'down'

        edgecolor: str or hexadecimal color code or ...
            The lines color

        node_scale: float

        linewidth: float

        alpha: float

        bg: string

        Returns
        -------
        Matplotlib figure

        See Also
        --------
        treex.vizualisation.texplot.dag_to_tex

        """

        __coord_dag(dag)

        figure = __matplotlib_figure(figure,bg)

        node_points = np.transpose([dag.get_attribute(node=node_id , attribute='coord_plotgraph') for node_id in dag.nodes()])
        if direction == 'down':
            direction_matrix = np.diag(np.ones(2))
        elif direction == 'up':
            direction_matrix = np.diag(-np.ones(2))
        elif direction == 'right':
            direction_matrix = np.array([[0,-1.0],[1.0,0]])
        else:
            direction_matrix = np.array([[0,1.0],[-1.0,0]])

        node_points = np.dot(direction_matrix,node_points)
        node_points = dict(zip(dag.nodes(),np.transpose(node_points)))

        node_colors = dict([(node_id,dag.get_attribute(node=node_id, attribute='color_plotgraph') if 'color_plotgraph' in dag.get_attribute(node=node_id).keys() else 'k') for node_id in dag.nodes()])
        node_sizes = dict([(node_id,dag.get_attribute(node=node_id, attribute='nodescale_plotgraph') if 'nodescale_plotgraph' in dag.get_attribute(node=node_id).keys() else 1.) for node_id in dag.nodes()])

        graph_edges = np.concatenate([[(node_points[node_id],node_points[child_id]) for child_id in dag.children(node_id)] for node_id in dag.nodes() if len(dag.children(node_id))>0])

        figure.gca().scatter([node_points[node_id][0] for node_id in dag.nodes()],[node_points[node_id][1] for node_id in dag.nodes()],c=[node_colors[node_id] for node_id in dag.nodes()],edgecolor=edgecolor,s=[node_scale*node_sizes[node_id] for node_id in dag.nodes()], linewidth=linewidth, zorder=5)
        for l in graph_edges:
            points = [l[0], 0.75*l[0]+0.25*l[1], 0.5*l[0]+0.5*l[1], 0.25*l[0]+0.75*l[1], l[1]]
            path = [Path.MOVETO, Path.CURVE4, Path.CURVE4, Path.CURVE4, Path.LINETO]

            path_patch = patch.PathPatch(Path(points,path), facecolor='none', edgecolor=edgecolor, linewidth=linewidth, alpha=alpha)
            figure.gca().add_patch(path_patch)

        figure.gca().axis('off')

        return figure


    def view_tree_spline_2d(tree, figure=None, nodesize_property='nodescale_plotgraph', color_property='color_plotgraph', text_property=None, width_property=None, root_point=[0,0], child_distance=1., leaf_alignment=-1, color='k', node_scale=40, linewidth=1, line_alpha=0.2, textsize=12):
        """
        This function displays a Tree object in a Matplotlib figure

        Parameters
        ----------
        tree: Tree

        figure: Matplotlib figure object, optional
            The figure in which to display the tree

        nodesize_property: str

        color_property: str

        text_property: str, optional

        width_property: str, optional

        root_point: [int, int], optional

        child_distance: float

        leaf_alignment: int

        color: string or hexadecimal color code or ...

        node_scale: float

        linewidth: float

        line_alpha: float
        
        textsize: float

        Returns
        -------
        Matplotlib figure

        See Also
        --------
        treex.vizualisation.matplotlib_plot.view_tree

        """

        figure = __matplotlib_figure(figure)

        size = tree.get_property('number_of_leaves',compute_level=1)

        if width_property in tree.get_attribute():
            node_width = np.sqrt(float(tree.get_attribute(width_property)))
        else:
            node_width = 1.


        if leaf_alignment is None:
            leaf_alignment = child_distance*(tree.get_property('height',compute_level=1)+0.5)
            figure.gca().plot([root_point[0]-child_distance/2.,root_point[0]],[root_point[1],root_point[1]],color=color, linewidth=linewidth*node_width, alpha=line_alpha)
            

        import matplotlib.transforms as mtrans
        from matplotlib.transforms import offset_copy
        trans_offset = mtrans.offset_copy(figure.gca().transData, fig=figure,x=0, y=textsize/2, units='dots')

      
        if (len(tree.my_children)>0) or leaf_alignment<0:
            node_point = [root_point[0]+child_distance/2.,root_point[1]]
        else:
            node_point = [leaf_alignment,root_point[1]]

        if text_property in tree.get_attribute():
            node_text = str(tree.get_attribute(text_property))
        else:
            node_text = ""

        if nodesize_property in tree.get_attribute():
            node_size = tree.get_attribute(nodesize_property)
        else:
            node_size = 1.

        if color_property in tree.get_attribute():
            node_color = tree.get_attribute(color_property)
        else:
            node_color = 'k'

        figure.gca().scatter([root_point[0]],[root_point[1]],c=node_color,edgecolor=color,s=node_size*node_scale, linewidth=linewidth, zorder=5)
        if (len(tree.my_children)>0):
            figure.gca().text(root_point[0],root_point[1],node_text,size=textsize,horizontalalignment='right',verticalalignment='bottom',transform=trans_offset)
        else:
            if node_point[0]-child_distance>root_point[0]:
                figure.gca().plot([root_point[0],node_point[0]-child_distance/2.],[root_point[1],node_point[1]],color=color, linewidth=linewidth*node_width, alpha=line_alpha)

            bbox_props = dict(boxstyle="round,pad=0.5",facecolor='w',edgecolor=color,linewidth=linewidth*node_width,alpha=1)
            figure.gca().text(node_point[0]-child_distance/2.,root_point[1],node_text,size=textsize,style='italic',horizontalalignment='left',verticalalignment='center',bbox=bbox_props)
           

        children_size = size/2.
        children_y = []
        for child in tree.my_children:
            child_size = child.get_property('number_of_leaves',compute_level=1)

            if width_property in child.get_attribute():
                child_width = np.sqrt(float(child.get_attribute(width_property)))
            else:
                child_width = 1.

            child_point = [root_point[0]+child_distance,root_point[1]+children_size-child_size/2.]
            node_child_point = [child_point[0]-child_distance/2.,child_point[1]]

            if (len(child.my_children)>0) or leaf_alignment<0:
                next_child_point = [child_point[0]+child_distance/2.,child_point[1]]
            else:
                next_child_point = [leaf_alignment,child_point[1]]

            view_tree_spline_2d(child, figure, nodesize_property=nodesize_property, color_property=color_property, text_property=text_property, width_property=width_property, root_point=child_point, child_distance=child_distance, leaf_alignment=leaf_alignment, color=color, node_scale=node_scale, linewidth=linewidth, line_alpha=line_alpha)

            points = [root_point, node_point, node_child_point, child_point, child_point]
            path = [Path.MOVETO, Path.CURVE4, Path.CURVE4, Path.CURVE4, Path.LINETO]

            path_patch = patch.PathPatch(Path(points,path), facecolor='none', edgecolor=color, alpha=line_alpha, linewidth=linewidth*child_width)
            figure.gca().add_patch(path_patch)

            children_y += [child_point[1]]
            children_size -= child_size

        figure.gca().axis('off')

        return figure


# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_zhang_labeled_trees
#
#       File author(s):
#           salah eddine habibeche <salah-eddine.habibeche@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.tree import *
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.analysis.edit_distance.zhang_labeled_trees import zhang_edit_distance
from treex.analysis.edit_distance.add_del_leaf import constrained_unordered_edit_distance

import math

def test_distpos():
	t = gen_random_tree(10)
	s = gen_random_tree(10)
	give_label_dist(t,(1))
	give_label_dist(s,(1))

	dist = zhang_edit_distance(t,s,'label_for_distance',local_distance,verbose=False)
	assert dist>=0


def test_treetonone():
	t = gen_random_tree(10)

	dist = zhang_edit_distance(t,None,None,verbose=False)
	assert dist==t.get_property("size")

def test_treetonone_with_cost():
	t = gen_random_tree(10)
	give_label_dist(t,(1))
	dist = zhang_edit_distance(t,None,'label_for_distance',local_distance,verbose=False)
	s=sum([local_distance(x.get_attribute('label_for_distance'),None) for x in t.list_of_subtrees()])
	assert dist==s

def test_distnull():
	t = gen_random_tree(10)
	give_label_dist(t,(1))
	dist = zhang_edit_distance(t,t,'label_for_distance',local_distance,verbose=False)
	assert dist == 0

def test_infconst():
	t = gen_random_tree(15)
	s = gen_random_tree(15)
	give_label_dist(t,(1))
	give_label_dist(s,(1))

	distconst=constrained_unordered_edit_distance(t,s)
	dist = zhang_edit_distance(t,s,'label_for_distance',local_distance,verbose=False)
	assert distconst >= dist

def test_without_cost_function():
	t = gen_random_tree(20)
	s = gen_random_tree(20)

	dist1 = zhang_edit_distance(t,s,verbose=False)
	give_label_dist(t,(1))
	give_label_dist(s,(1))

	dist2 = zhang_edit_distance(t,s,'label_for_distance',local_distance,verbose=False)
	assert dist1==dist2

def test_cost_function_tuple():
	t = gen_random_tree(20)
	s = gen_random_tree(20)

	give_label_dist(t,(1,5))
	give_label_dist(s,(1,3))

	dist = zhang_edit_distance(t,s,'label_for_distance',local_distance,verbose=False)
	assert dist >= 0

def test_verbose_with_cost():
	t = gen_random_tree(20)
	s = gen_random_tree(20)
	give_label_dist(t,(1))
	give_label_dist(s,(1))

	d = zhang_edit_distance(t,s,'label_for_distance',local_distance,verbose=True)
	assert d['distance']==d["matrix_of_trees_distances"][(t.my_id,s.my_id)]

def test_verbose():
	t = gen_random_tree(20)
	s = gen_random_tree(20)

	d = zhang_edit_distance(t,s,None,verbose=True)
	assert d['distance']==d["matrix_of_trees_distances"][(t.my_id,s.my_id)]


def local_distance(v1=None,v2=None):
	if isinstance(v1,tuple)==True:
		if v2==None:
			cout=0
			v=list(v1)
			for i in range(len(v)):
				cout+=abs(v[i])
			return cout
		else:
			d=len(v1)
			return sum( [ abs(v1[i]-v2[i]) for i in range(0,d) ])  
	else: 
		if v2==None:
			return abs(v1)
		else:
			return abs(v1-v2)


def give_label_dist(tree,value):
	# associate attribute values with the tree
	# Parameters
	# ----------
	# tree: treex tree
	# value: number vector
	tree.add_attribute_to_id('label_for_distance',value)
	for child in tree.my_children:
		give_label_dist(child,value)

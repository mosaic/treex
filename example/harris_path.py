# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/harris_path
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to compute the harris path of a tree (and how to go back to the tree)
"""

from treex.tree import *
from treex.simulation import gen_random_tree
from treex.coding_process.harris import tree_to_harris_walk, harris_walk_to_tree

######################################################################
def display_processus (aDictionaryFunction,aTitle='Display function'):
	# This function needs matplotlib and displays a dictionary that
	# represents a function
	from matplotlib import pyplot as plt

	plt.plot(aDictionaryFunction['x'],aDictionaryFunction['y'],'ro-')
	plt.title(aTitle)
	plt.show()
######################################################################

t=gen_random_tree(15)

print('-------------------------------------')

print("initial tree")
t.get_attribute()
t.treeprint('one attribute','height')

print('-------------------------------------')

print("Harris")
print("Kill the display window to continue")
harris=tree_to_harris_walk(t)
display_processus(harris,"Harris walk")

print('-------------------------------------')

print("Tree from Harris walk")
atree=harris_walk_to_tree(harris)
atree.get_attribute()
atree.treeprint('one attribute','height')





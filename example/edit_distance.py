# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/edit_distance
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to compute the constrained edit distance between two trees
"""

from treex.tree import *
from treex.edit_distance.add_del_leaf import constrained_unordered_edit_distance, constrained_tree_matching
from treex.simulation import gen_random_tree

print('Example 1')

t1 = gen_random_tree(30)
t2 = gen_random_tree(20)

dist1 = constrained_unordered_edit_distance(t1,t2)

print('Distance between t1 and t2:')
print(dist1)


def cost_function_ex1(tree1,tree2):
	if tree2==None:
		return tree1.get_attribute('size')
	else:
		return 0

dist2 = constrained_unordered_edit_distance(t1,t2,cost_function_ex1)
print(dist2)
# Using cost_function_ex1 provides the same result as without cost function

match = constrained_tree_matching(t1,t2)
print('Matching between t1 and t2:')
print(match)

print('-------------------------------------')

print('Example 2')

def cost_function_ex2(tree1,tree2):
	label_distance='label for distance'
	if tree2==None: # cost for creation of tree1
		cout=0
		for i in range(len(tree1.get_attribute('label for distance'))):
			cout+=tree1.get_attribute('label for distance')[i]
		for child in tree1.my_children:
			cout+=cost_function_ex2(child,None)
	else: # tree1 and tree2 must have only one node
		cout= sum( [ abs(tree1.get_attribute('label for distance')[i]-tree2.get_attribute('label for distance')[i]) for i in range(len(tree1.get_attribute('label for distance'))) ] ) 
	return cout

def give_label_dist(tree,value):
	if isinstance(value,list):
		tree.add_attribute_to_id('label for distance',value)
	else:
		tree.add_attribute_to_id('label for distance',[value])
	for child in tree.my_children:
		give_label_dist(child,value)

give_label_dist(t1,[2,3])
give_label_dist(t2,[1,2])

dist3 = constrained_unordered_edit_distance(t1,t2,cost_function_ex2)
print('Distance that takes account of the label "label for distance":')
print(dist3)
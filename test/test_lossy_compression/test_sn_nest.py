# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_lossy_compression/test_sn_nest
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.lossy_compression.selfnestedness.nest import *

def test_nestmin():
	t = gen_random_tree(200)
	s = nest_min(t)
	diff = t.get_property('size')-s.get_property('size')
	assert diff>=0

def test_nestmax():
	t = gen_random_tree(200)
	s = nest_max(t)
	diff = t.get_property('size')-s.get_property('size')
	assert diff<=0

def test_nestmin_max():
	t = gen_random_tree(200)
	lst = nest_min_max(t)
	s = nest_min(t)
	assert s.is_isomorphic_to(lst[0],'unordered')
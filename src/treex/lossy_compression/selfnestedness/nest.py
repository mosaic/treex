# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.lossy_compression.selfnestedness.nest
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
    Called by this module: treex.selfnested.heightprofile

    This module implements functions to approximate a Tree class object by a
    selfnested tree (of Tree class too)

    - File author(s): Romain Azais <romain.azais@inria.fr>

    References
    ----------
    This module implements the algorithms presented in
    AZAIS "Nearest embedded and embedding selfnested trees". Algorithms
    (Special Issue Data Compression Algorithms and their Applications). 2019

    A first algorithm for computing the nest_max is presented in
    GODIN and FERRARO "Quantifying the degree of self-nestedness of trees:
    application to the structural analysis of plants".
    IEEE/ACM Trans Comput Biol Bioinform. 2010

"""

from treex.lossy_compression.selfnestedness.heightprofile import *

# --------------------------------------------------------------

def nest_min_max(tree):
    """
    This function computes in the same time the approximations nest_min
    (Nearest Embedded Selfnested Tree) and nest_max (Nearest Embedding Selfnested Tree)

    Parameters
    ----------
    tree: Tree

    Returns
    -------
    list
        list of two Tree class objects

    References
    ----------
    This function implements the approximation algorithms presented in
    Azais "Nearest embedded and embedding selfnested trees", Preprint arXiv 2019

    See also
    --------
    nest_min(...)
    nest_max(...)

    """
    h_p=heightprofile(tree)
    hauteur=h_p.height()
    h_p_nestmin = h_p.copy()
    h_p_nestmax = h_p.copy()

    for h1 in range(1,hauteur+1):
        for h2 in range(h1-1,-1,-1):

            # NeST
            delta=[ ( h_p_nestmin.profile[h1-1][h2][i]-min(h_p_nestmin.profile[h1-1][h2]) ) for i in range(len(h_p_nestmin.profile[h1-1][h2])) ]
            h_p_nestmin.profile[h1-1][h2]=[ min(h_p_nestmin.profile[h1-1][h2]) ]

            if h2>0 and h1>=2:
                test=[[0] for i in range(h1-2)]
                test.append([1])

                if h_p_nestmin.profile[h1-1-1]==test:
                    for i in range(len(h_p_nestmin.profile[h1-1][h2-1])):
                        h_p_nestmin.profile[h1-1][h2-1][i]=h_p_nestmin.profile[h1-1][h2-1][i]+delta[i]


            # NEST
            delta=[ ( max(h_p_nestmax.profile[h1-1][h2])-h_p_nestmax.profile[h1-1][h2][i] ) for i in range(len(h_p_nestmax.profile[h1-1][h2])) ]
            h_p_nestmax.profile[h1-1][h2]=[ max(h_p_nestmax.profile[h1-1][h2]) ]
            
            c=1
            test=[0 for i in range(len(delta))]
            while delta!=test and c<=h2:
                for i in range(len(delta)):
                    delta[i] , h_p_nestmax.profile[h1-1][h2-c][i] = max(delta[i]-h_p_nestmax.profile[h1-1][h2-c][i],0) , h_p_nestmax.profile[h1-1][h2-c][i]-delta[i]
                c+=1

    h_p_nestmin = SelfNestedHeightProfile.from_heightprofile(h_p_nestmin)
    h_p_nestmax = SelfNestedHeightProfile.from_heightprofile(h_p_nestmax)

    return [h_p_nestmin.to_tree() , h_p_nestmax.to_tree()]

# --------------------------------------------------------------

def nest_min(tree):
    """
    This function computes the approximations nest_min (Nearest Embedded Selfnested Tree)

    Parameters
    ----------
    tree: Tree

    Returns
    -------
    Tree

    References
    ----------
    This function implements the approximation algorithm presented in
    Azais "Nearest embedded and embedding selfnested trees", Preprint arXiv 2019

    See also
    --------
    nest_min_max(...)
    nest_max(...)
    
    """
    h_p=heightprofile(tree)
    hauteur=h_p.height()
    h_p_nestmin = h_p.copy()

    for h1 in range(1,hauteur+1):
        for h2 in range(h1-1,-1,-1):
            # NeST
            delta=[ ( h_p_nestmin.profile[h1-1][h2][i]-min(h_p_nestmin.profile[h1-1][h2]) ) for i in range(len(h_p_nestmin.profile[h1-1][h2])) ]
            h_p_nestmin.profile[h1-1][h2]=[ min(h_p_nestmin.profile[h1-1][h2]) ]

            if h2>0 and h1>=2:
                test=[[0] for i in range(h1-2)]
                test.append([1])

                if h_p_nestmin.profile[h1-1-1]==test:
                    for i in range(len(h_p_nestmin.profile[h1-1][h2-1])):
                        h_p_nestmin.profile[h1-1][h2-1][i]=h_p_nestmin.profile[h1-1][h2-1][i]+delta[i]

    h_p_nestmin = SelfNestedHeightProfile.from_heightprofile(h_p_nestmin)

    return h_p_nestmin.to_tree()

# --------------------------------------------------------------

def nest_max(tree):
    """
    This function computes the approximations nest_min (Nearest Embedded Selfnested Tree)

    Parameters
    ----------
    tree: Tree

    Returns
    -------
    Tree

    References
    ----------
    This function implements the approximation algorithm presented in
    Azais "Nearest embedded and embedding selfnested trees", Preprint arXiv 2019

    See also
    --------
    nest_min_max(...)
    nest_min(...)
    
    """
    h_p=heightprofile(tree)
    hauteur=h_p.height()
    h_p_nestmax = h_p.copy()

    for h1 in range(1,hauteur+1):
        for h2 in range(h1-1,-1,-1):
            # NEST
            delta=[ ( max(h_p_nestmax.profile[h1-1][h2])-h_p_nestmax.profile[h1-1][h2][i] ) for i in range(len(h_p_nestmax.profile[h1-1][h2])) ]
            h_p_nestmax.profile[h1-1][h2]=[ max(h_p_nestmax.profile[h1-1][h2]) ]
            
            c=1
            test=[0 for i in range(len(delta))]
            while delta!=test and c<=h2:
                for i in range(len(delta)):
                    delta[i] , h_p_nestmax.profile[h1-1][h2-c][i] = max(delta[i]-h_p_nestmax.profile[h1-1][h2-c][i],0) , h_p_nestmax.profile[h1-1][h2-c][i]-delta[i]
                c+=1

    h_p_nestmax = SelfNestedHeightProfile.from_heightprofile(h_p_nestmax)
    return h_p_nestmax.to_tree()

# --------------------------------------------------------------
# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_utils/test_save
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
import unittest

import treex
import os

from treex.simulation import gen_random_tree

from treex.utils.save import save_object, mat_to_txt, str_to_txt, save_tree_properties_and_attributes, read_object, reassign_ids_to_trees
from treex.utils.save import read_disk_size,read_txt


class TestSave(unittest.TestCase):
    '''
    Tests the treex.save module
    '''

    def setUp(self):    
        self.t = gen_random_tree(20)
        self.l = [gen_random_tree(10), 3 , 'a']
        self.d = {'a' : gen_random_tree(10)}
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)
        # if os.path.exists(self.data_dir+"test_save_object.p"):
        #     os.remove(self.data_dir+"test_save_object.p")
        # if os.path.exists(self.data_dir+"test_mat_to_txt.txt"):
        #     os.remove(self.data_dir+"test_mat_to_txt.txt")

    def test_read_object(self):
        save_object(self.t,self.data_dir+'test_read_object.p')
        s = read_object(self.data_dir+'test_read_object.p')
        assert isinstance(s,treex.Tree)
        assert s.is_isomorphic_to(self.t)
        assert s.my_id != self.t.my_id

    def test_read_object2(self):
        save_object(self.t,self.data_dir+'test_read_object.p')
        s = read_object(self.data_dir+'test_read_object.p' , return_mapping = True)
        assert isinstance(s,dict)
        assert isinstance(s['mapping'],dict)
        assert isinstance(s['object'],treex.Tree)

    def test_read_object3(self):
        save_object([self.t,self.t.my_children[0]],self.data_dir+'test_read_object.p')
        s = read_object(self.data_dir+'test_read_object.p' , return_mapping = True)
        assert isinstance(s,dict)
        assert isinstance(s['mapping'],list)

    def test_read_object4(self):
        save_object([1,2,3],self.data_dir+'test_read_object.p')
        s = read_object(self.data_dir+'test_read_object.p' , return_mapping = True)
        assert s == [1,2,3]

    def test_reassign_ids(self):
        tp = reassign_ids_to_trees(self.t)['object']
        lp = reassign_ids_to_trees(self.l)['object']
        dp = reassign_ids_to_trees(self.d)['object']

        assert tp.is_isomorphic_to(self.t)
        assert tp.my_id != self.t.my_id
        assert lp[0].is_isomorphic_to(self.l[0])
        assert lp[0].my_id != self.l[0].my_id
        assert dp['a'].is_isomorphic_to(self.d['a'])
        assert dp['a'].my_id != self.d['a'].my_id

    def test_save_read_object(self):
        save_object(self.t,self.data_dir+'test_save_object.p')
        s = read_object(self.data_dir+'test_save_object.p')
        assert self.t.is_isomorphic_to(s)

    def test_read_disk_size(self):
        save_object(self.t,self.data_dir+'test_save_object.p')
        s = read_disk_size(self.data_dir+'test_save_object.p')
        assert s>0

    def test_mat_to_txt(self):
        m = [[1,0],['a',1]]
        print(self.data_dir+'test_mat_to_txt.txt')
        mat_to_txt(m,self.data_dir+'test_mat_to_txt.txt')
        s = read_disk_size(self.data_dir+'test_mat_to_txt.txt')
        assert s>0

    def test_str_to_txt(self):
        a = 'hello world'
        str_to_txt(a , self.data_dir+'test_str_to_txt.txt')
        s2 = read_disk_size(self.data_dir+'test_str_to_txt.txt')
        assert s2>0

    def test_save_tree_properties_and_attributes(self):
        self.t._Tree__my_properties['new_prop'] = 'a'
        self.t._Tree__my_attributes['new_attr'] = 'b'

        save_tree_properties_and_attributes(self.t , self.data_dir+'test_save_tree_props_and_attrs.txt')
        s3 = read_disk_size(self.data_dir+'test_save_tree_props_and_attrs.txt')
        assert s3>0

    def test_read_txt(self):
        m = [[1,0,5]]
        mat_to_txt(m,self.data_dir+'test_mat_to_txt.txt')
        a = read_txt(self.data_dir+'test_mat_to_txt.txt')
        assert isinstance(a,str)

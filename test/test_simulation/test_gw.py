# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_simulation/test_gw
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.simulation.galton_watson import gen_galton_watson

def test_galton_watson_1():
	for i in range(20):
		t = gen_galton_watson('',[0.2,0.4,0.4],10)
		assert t.get_property('height')<=10

def test_galton_watson_2():
	for i in range(20):
		t = gen_galton_watson('geometric',0.9,5)
		assert t.get_property('height')<=5

def test_galton_watson_3():
	for i in range(20):
		t = gen_galton_watson('binomial',[3,0.5],5)
		assert t.get_property('height')<=5

def test_galton_watson_3_2():
	for i in range(20):
		t = gen_galton_watson('binomial',[2,0.95],5)
		assert t.get_property('height')<=5

def test_galton_watson_4():
	for i in range(20):
		t = gen_galton_watson('poisson',1,5)
		assert t.get_property('height')<=5

def test_galton_watson_5():
	for i in range(20):
		t = gen_galton_watson('geometric_with_shift',0.8,5)
		assert t.get_property('height')<=5

def test_galton_watson_6():
	for i in range(20):
		t = gen_galton_watson('geometric_with_dirac',[0.1,0.8],5)
		assert t.get_property('height')<=5

def test_galton_watson_6_2():
	for i in range(20):
		t = gen_galton_watson('geometric_with_dirac',[0.7,0.8],5)
		assert t.get_property('height')<=5

# ------------------------------------------------------------------------------

from treex.simulation.galton_watson import gen_conditioned_galton_watson

def test_cgw():
	t = gen_conditioned_galton_watson('',[0.2,0.4,0.4],5)
	assert t.get_property('height') >= 5
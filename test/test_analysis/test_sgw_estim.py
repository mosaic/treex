# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_analysis/test_sgw_estim
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.tree import *
from treex.simulation.spinal_galton_watson import gen_spinal_galton_watson, sgw_spine, sgw_special_distribution
from treex.analysis.spinal_galton_watson.uglyduckling import sgw_uglyduckling, sgw_uglyduckling_criterion

def test_sgw_uglyduckling():

	### Parameters ###
	mu = [0.3,0.4,0.3]
	f = [0.0 , 1.0, 3.0]
	nu = sgw_special_distribution(mu,f)

	max_gen = 31
	obs_gen = 30

	### Normalization of f ###
	f = [f[k]/sum(f) for k in range(len(f))]

	### Convergence criterion ###
	crit = sgw_uglyduckling_criterion(mu,nu)

	### Simulation ###
	t = gen_spinal_galton_watson('',mu,'',nu,max_gen)
	spine = sgw_spine(t)

	### Estimation ###
	estim = sgw_uglyduckling(t,obs_gen,max_degree=len(mu)-1)

	mu_hat = estim['estimated_normal_birth_distribution']
	mu_hat_c = estim['corrected_estimated_normal_birth_distribution']
	f_hat = estim['estimated_transform_function']
	nu_hat = estim['estimated_special_birth_distribution']
	spine_hat = estim['estimated_spine'][0]

	assert crit<0
	assert len(mu_hat) == 3
	assert len(mu_hat_c) == 3
	assert mu_hat[0]>=0
	assert len(f_hat) == 3
	assert len(nu_hat) == 3
	assert len(spine_hat) == obs_gen
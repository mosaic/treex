# -*- coding: utf-8 -*-
# -*- python -*-
#
#
#       OpenAlea.OALab: Multi-Paradigm GUI
#
#       Copyright 2014-2017 INRIA - CIRAD - INRA
#
#       File author(s):
#            Guillaume Cerutti <guillaume.cerutti@inria.fr>
#            Julien Diener <julien.diener@inria.fr>
#            Guillaume Baty <guillaume.baty@inria.fr>
#
#       File contributor(s):
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
#       OpenAlea WebSite : http://openalea.gforge.inria.fr
#
###############################################################################

from PyQt5 import QtGui, QtWidgets, QtCore

import matplotlib

matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from matplotlib.backends.backend_qt5agg import _BackendQT5Agg
from matplotlib.backends.backend_qt5 import _BackendQT5

from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.backend_bases import FigureManagerBase
from matplotlib._pylab_helpers import Gcf

# from matplotlib.backends.qt_compat import _getSaveFileName

import os

all_widgets = {}
figure_containers = []


class MplFigure(Figure):
    pass


class MplCanvas(FigureCanvasQTAgg):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None):
        fig = MplFigure()
        FigureCanvasQTAgg.__init__(self, fig)


class FigureManagerQT(FigureManagerBase):
    """
    Public attributes

    canvas      : The FigureCanvas instance
    num         : The Figure number
    window      : The qt.QMainWindow
    """

    def __getattribute__(self, *args, **kwargs):
        return FigureManagerBase.__getattribute__(self, *args, **kwargs)

    def __init__(self, canvas, num):
        FigureManagerBase.__init__(self, canvas, num)
        self.canvas = canvas
        self.canvas.setFocusPolicy(QtCore.Qt.StrongFocus)

    def show(self):
        # print 'pylab.plot'
        pass


class MplFigureTabWidget(QtWidgets.QFrame):
    tabCreated = QtCore.pyqtSignal(bool)

    def __init__(self, canvas=None, num=None):
        QtWidgets.QFrame.__init__(self)

        c = self.palette().color(self.backgroundRole())
        self._default_color = str((c.red(), c.green(), c.blue()))

        num = 0 if num is None else num

        self.canvas = MplCanvas()
        self.manager = FigureManagerQT(self.canvas, num)

        Gcf.figs[num] = self.manager
        all_widgets[num] = self

        self.setToolTip("Figure %d" % self.manager.num)

        self.setFrameShape(QtWidgets.QFrame.Box)
        self.setFrameShadow(QtWidgets.QFrame.Plain)
        self.setContentsMargins(1, 1, 1, 1)

        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.addWidget(self.canvas)
        self._layout.setContentsMargins(1, 1, 1, 1)

    def show_active(self):
        self.setFrameShape(QtWidgets.QFrame.Box)
        self.setStyleSheet("background-color: rgb(0, 150, 0);")

    def show_inactive(self):
        self.setStyleSheet("")
        self.setFrameShape(QtWidgets.QFrame.NoFrame)

    def hold(self, state=True):
        for axe in self.canvas.figure.axes:
            axe.hold(state)

    # def toolbar_actions(self):
    # return [['', '', action, 0] for action in self.mpl_toolbar.actions()]

@classmethod
def mpl_new_figure_manager_given_figure(cls, num, figure):
    """
    Create a new figure manager instance for the given figure.
    """
    # canvas = mplMplCanvasView(figure).widget()
    canvas = MplCanvas(figure)
    widget = MplFigureTabWidget(canvas, num)

    for f in figure_containers:
        f.refresh_tab_widgets()
    return widget.manager


@classmethod
def mpl_draw_if_interactive(cls):
    """
    Is called after every pylab drawing command
    """
    # if matplotlib.is_interactive():

    figManager = Gcf.get_active()
    # print("Draw figures :",figManager)
    if figManager is not None:
        figManager.canvas.draw_idle()


def activate():
    plt.switch_backend('qt5agg')
    plt.ion()

    _BackendQT5.draw_if_interactive = mpl_draw_if_interactive
    _BackendQT5Agg.draw_if_interactive = mpl_draw_if_interactive

    _BackendQT5.new_figure_manager_given_figure = mpl_new_figure_manager_given_figure
    _BackendQT5Agg.new_figure_manager_given_figure = mpl_new_figure_manager_given_figure

    _BackendQT5Agg.show = lambda: None


activate()

# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.lossless_compression.dag
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       File contributor(s):
#           Florian Ingels <florian.ingels@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
Called by this module: treex.tree, copy.deepcopy()

This module implements Directed Acyclic Graphs (DAG) structures and several generic functions to construct and process them.


- File author(s): Romain Azais <romain.azais@inria.fr>
- File contributor(s): Florian Ingels <florian.ingels@inria.fr>

"""

from treex.tree import *

from copy import deepcopy


# --------------------------------------------------------------

class Dag(object):
    """
    A dag is a Directed Acyclic Graph. This class can deal with multiples
    sources (here called roots) and multiples sinks (here called leaves). Even if
    non-connex DAG can be encoded with this structure, it is not meant to be. Due
    to a strong link with trees, as a tree can be compressed into a DAG (see the
    function tree_to_dag(...)), the DAG structure is oriented toward tree
    compression.

    A DAG can compress either a single tree (with one root) or a forest of
    tree (with multiples roots).

    Attributes
    ----------
    my_structure: dict

        Each node in the DAG is associated to a unique number that serves as a
        key in the dictionary. Associated to this key/node is another
        dictionary containing, for that node :

            * its list of children
            * all attributes and properties computed for that node

        Unlike Tree class, Dag class is not recursive.

    my_roots: list

        A root is a node with no parents. This list contains the numbers
        associated to all the roots. A DAG with a single root is representing
        a tree, whereas a DAG with multiple roots represents a forest of
        trees.

    my_leaves: list

        A list containing the numbers associated to each node which is a leaf
        - nodes with no children.

    my_compression: dict

        If a DAG is compressing a tree, this attribute will indicates all
        relevant informations as a dictionary of three attributes:

        * tree_type : either 'ordered' or 'unordered'
        * attribute : if the compression depends on an attribute or not, and which one
        * compression_rate : 'optimal', 'no compression' or 'unknown'

        For more details, refer to the functions tree_to_dag(...) and compress()

    my_origins: dict

        Keys of this dictionary are the name, or origins, of the tree(s)
        encoded by the DAG. To each key is associated the root of the
        corresponding tree in the DAG. Most of the time, they will be the same
        as the roots of the DAG itself, but there are two special cases that
        justify the presence of this attribute when a forest is encoded:

        * if one tree of the forest is a subtree of another, after
          compression, the root of the smallest will not be a root of the DAG
          anymore

        * if a tree is present twice in the forest, after compression, the two
          roots will be merged

        Each node in the DAG has an attribute called "origin" that is the dictionary of all origins corresponding to trees that have that node as a subtree. The value associated to each origin key is the number of times that particular subtree appears in the original tree. For further details on how it is recursively computed, see
        https://arxiv.org/abs/1904.05421 (preprint).


    See Also
    --------
    tree_to_dag(...), compress(...)

    """
    __bottomup_properties = ['height', 'size', 'outdegree', 'number_of_children', 'strahler_number']

    __slots__ = 'my_structure', 'my_roots', 'my_leaves', 'my_compression', 'my_origins'

    def __init__(self):
        self.my_structure = {}
        self.my_roots = []
        self.my_leaves = []
        self.my_compression = {'tree_type': None, 'attribute': None, 'compression_rate': None}
        self.my_origins = {}

    def __str__(self, k=0):
        """
        This method simply allows the use of print(...) function for Dag class
        objects
        """
        return self.__string_dag()

    # --------------------------------------------------------------

    def __string_dag(self):  # Private
        out = ''
        for node in self.nodes():
            out += str(node) + ': ' + str(self.children(node)) + '\n'
        return out

    # --------------------------------------------------------------

    def __my_attributes(self, node):  # Private
        try:
            if 'attributes' not in self.my_structure[node]:
                self.my_structure[node]['attributes'] = {}

            return self.my_structure[node]['attributes']
        except:
            print('Requested node does not belong to the Dag')

    def __my_properties(self, node):  # Private
        try:
            if 'properties' not in self.my_structure[node]:
                self.my_structure[node]['properties'] = {}

            return self.my_structure[node]['properties']
        except:
            print('Requested node does not belong to the Dag')

    # --------------------------------------------------------------

    def nodes(self):
        """
        Returns the list of nodes (as a list of numbers) of the dag
        """
        return list(self.my_structure.keys())

    # --------------------------------------------------------------

    def children(self, node):
        """
        Returns the list of children of a specific node

        Parameters
        ----------
        node: int
            It must be in dag.nodes()

        Returns
        -------
        list
            list of children of the node
        """
        return self.my_structure[node]['children']

    # --------------------------------------------------------------

    def __descendants_add_function(self, node, liste):  # Only to compute descendants(...) # Private
        for child in set(self.children(node)):
            liste.add(child)
            self.__descendants_add_function(child, liste)

    # --------------------------------------------------------------

    def descendants(self, node):
        """
        Returns the list of descendants (children and their children, and so
        on) of a specific node

        Parameters
        ----------
        node: int
            It must be in dag.nodes()

        Returns
        -------
        list
            list of integers giving the descendants of the node

        Warnings
        --------
        descendants returned are not counted with their multiplicity
        """

        l = set()
        self.__descendants_add_function(node, l)
        return list(l)

    # --------------------------------------------------------------

    def assign_roots(self):
        """
        Find which node in the dag are roots, and assign them to dag.my_roots
        """
        l = set()
        for node in self.nodes():
            l = l.union(set(self.children(node)))
        self.my_roots = list(set(self.nodes()) - l)

    # --------------------------------------------------------------

    def assign_leaves(self):
        """
        Find which nodes in the dag are leaves, and assign them to dag.my_leaves
        """
        self.my_leaves = []
        for node1 in self.nodes():
            if self.children(node1) == []:
                self.my_leaves.append(node1)

    # --------------------------------------------------------------

    def get_attribute(self, node=None, attribute=None):
        """
        This method allows to access to the attributes of nodes in the dag

        Parameters
        ----------
        node: int, optional
            It must be in dag.nodes(). The node which attributes will be
            returned. Default is None; two cases can then occur:

            * if there is a single root, then the attributes of the root are
              returned
            * if there are multiple roots, then a dictionary is
              returned, where the keys are the roots and the values their
              attributes

        property: str, optional
            Default is None. In this case, all previously existing
            attributes of the node are returned (sse below)

        Returns
        -------
            If attribute is set to None, a dictionary of all already existing attributes is returned.
            If a single attribute is provided, its value is returned.

        Warnings
        --------
        If an attribute is given, and if it does not exist at the node, then the
        method raise an error

        See Also
        --------
        add_attribute_to_node(...), erase_attribute(...), erase_attribute_at_node(...), get_property(...)

        """
        if node is None:
            attr = {}
            for rt in self.my_roots:
                attr[rt] = self.__get_attribute_of_node(rt, attribute)
            if len(self.my_roots) > 1:
                return attr
            else:
                return attr[self.my_roots[0]]
        else:
            return self.__get_attribute_of_node(node, attribute)

    def __get_attribute_of_node(self, node, attribute=None):
        if attribute == None:
            return self.__my_attributes(node)
        else:
            try:
                return self.__my_attributes(node)[attribute]
            except:
                print('Requested attribute does not belong to the dag attributes')

    def get_property(self, node=None, prop=None, compute_level=1):
        """
        This method allows to access to the properties of nodes in the dag

        Parameters
        ----------
        node: int, optional
            It should be in dag.nodes(). The node which properties will be
            returned. Default is None; two cases can then occur:

            * if there is a single root, then the properties of the root are
              returned
            * if there are multiple roots, then a dictionary is
              returned, where the keys are the roots and the values their
              properties

        property: str, optional
            name of the property
            Default is None. In this case, all previously existing
            properties of the node are returned (see below)

        compute_level: int
            - if compute_level is 2, the property is computed even if existing
            - if compute_level is 1, the property is computed only if not existing
            - if compute_level is 0, the property is only read
            Default is 1

        Returns
        -------
            - If prop is set to None, a dictionary of all properties is returned.
            - If a single property is provided, its value is returned.
            - If the property does not belong to the dag properties,
            or the node does not belong to the dag,
            None is returned.

        See Also
        --------
        get_attribute(...)
        """

        ### 1 ### Computation step
        if compute_level == 2:
            if prop in self.__bottomup_properties or prop==None:
                al_comp = []
                for rt in self.my_roots:
                    self.__compute_property(rt , self.__bottomup_properties, update_only=False, already_computed=al_comp)

        elif compute_level == 1:
            if prop in self.__bottomup_properties or prop == None:
                al_comp = []
                for rt in self.my_roots:
                    self.__compute_property(rt, self.__bottomup_properties, update_only=True, already_computed=al_comp)

        ### 2 ### Read step
        if node is None:
            attr = {}
            for rt in self.my_roots:
                attr[rt] = self.__get_property_of_node(rt, prop)
            if len(self.my_roots) > 1:
                return attr
            else:
                return attr[self.my_roots[0]]
        else:
            return self.__get_property_of_node(node, prop)

    def __get_property_of_node(self, node, prop):
        if node in self.nodes():
            if prop == None:
                return self.__my_properties(node)
            elif prop in self.__my_properties(node):
                return self.__my_properties(node)[prop]
            else:
                print('Requested property is not a property of this node in the dag')
                return None
        else:
            print('Requested node does not belong to the dag')
            return None

    # --------------------------------------------------------------

    def __compute_property(self, rt, prop, update_only=False, already_computed=[]):  # Private
        ### Warning: in DAGs, only bottom-up properties make sense ###

        if isinstance(prop, list):
            assert all([a in self.__bottomup_properties for a in prop])
        else:
            assert prop in self.__bottomup_properties

        if ('height' in prop) or ('size' in prop) or ('outdegree' in prop) or ('number_of_children' in prop) or ('strahler_number' in prop):

            lst_children = self.children(rt)

            s = 1
            lst_h = []
            lst_d = [len(lst_children)]
            lst_sn = []
            for (u, v) in [(u, lst_children.count(u)) for u in set(lst_children)]:

                att_u = self.get_property(node=u, prop=None, compute_level=0)
                if update_only and 'number_of_children' in att_u and 'height' in att_u and 'size' in att_u and 'outdegree' in att_u and 'strahler_number' in att_u:
                    compute = False
                else:
                    compute = True

                if compute and u not in already_computed:
                    self.__compute_property(u, prop, update_only, already_computed)

                att_u = self.get_property(node=u, prop=None, compute_level=0)

                h_u = att_u['height']
                s_u = att_u['size']
                d_u = att_u['outdegree']
                sn_u = att_u['strahler_number']

                s += v * s_u
                lst_h.append(h_u)
                lst_d.append(d_u)
                lst_sn.extend([sn_u] * v)

            h = max(lst_h) + 1 if len(lst_h) > 0 else 0
            d = max(lst_d)
            if len(lst_sn) == 0:
                sn = 1
            else:
                if len([x for x in lst_sn if x == max(lst_sn)]) < 2:
                    sn = max(lst_sn)
                else:
                    sn = max(lst_sn) + 1

            if 'properties' not in self.my_structure[rt].keys():
                self.my_structure[rt]['properties'] = {}
            self.my_structure[rt]['properties']['size'] = s
            self.my_structure[rt]['properties']['height'] = h
            self.my_structure[rt]['properties']['outdegree'] = d
            self.my_structure[rt]['properties']['number_of_children'] = len(lst_children)
            self.my_structure[rt]['properties']['strahler_number'] = sn

            already_computed.append(rt)

    # --------------------------------------------------------------

    def sub_dag(self, rt):
        """
        Creates a new dag, which is a substructure of the first one, and
        starting with the root given

        Parameters
        ----------
        rt: int
            The node where to root the new dag. It must be in dag.nodes()

        Returns
        -------
        Dag

        """
        out = Dag()

        out.my_roots = [rt]

        out.my_structure[rt] = self.my_structure[rt]
        for node in self.nodes():
            if node in self.descendants(rt):
                out.my_structure[node] = self.my_structure[node]

        out.my_leaves = [node for node in self.my_leaves if node in out.nodes()]

        ### Warning: Romain Dec18 2019 ###
        # if 'origin' in self.get_attribute(rt):
        #     for origin in self.get_attribute(rt, 'origin'):
        #         out.my_origins[origin] = rt
        #     out.__propagate_origins()

        ### Correction (Romain Oct 8 2019) ###
        out.my_compression = self.my_compression

        return out

    # --------------------------------------------------------------

    def to_tree(self):
        """
        From a single-rooted DAG, creates the corresponding tree.

        Returns
        -------
        Tree

        Warnings
        --------
        If a DAG with more than one root is passed, an Exception will be raised.

        See Also
        --------
        treex.tree.is_isomorphic_to(), tree_to_dag(...)

        """
        if len(self.my_roots) > 1:
            raise Exception("This function works only with single-rooted DAGs")
        else:

            # Tree type
            tree_type = self.my_compression['tree_type']
            if tree_type == None:
                tree_type = 'unordered'


            # Attribute
            attr = self.my_compression['attribute']

            # Construction of tree
            t = Tree()
            t._Tree__my_attributes[tree_type + '_equivalence_class'] = self.my_roots[0]

            if self.my_compression['compression_rate']=='no_compression':
                t._Tree__my_attributes = deepcopy(self.get_attribute())

            if attr != None:
                t._Tree__my_attributes[attr] = self.get_attribute(attribute=attr)

            for child in self.children(self.my_roots[0]):
                sub_dag_child = self.sub_dag(child)
                t.add_subtree(sub_dag_child.to_tree())
            return t

    # --------------------------------------------------------------

    def add_attribute_to_node(self, attribute_name, attribute_value, node):
        """
        Add an attribute to a node in the dag

        Parameters
        ----------
        attribute_name: str
            The name of the new attribute

        attribute_value:
            The value of the new attribute

        node: int
            The number of the node where to give the new attribute. The node must be
            in dag.nodes()

        Returns
        -------
        None

        See Also
        --------
        get_attribute(...), erase_attribute(...), erase_attribute_at_node(...)

        """
        try:
            if 'attributes' not in self.my_structure[node]:
                self.my_structure[node]['attributes'] = {}

            self.my_structure[node]['attributes'][attribute_name] = attribute_value
        except:
            print('Requested node does not belong to the Dag')

    # --------------------------------------------------------------

    def erase_attribute_at_node(self, attribute_name, node):
        """
        Erase an attribute from one node in the dag

        Parameters
        ----------
        attribute_name: str
            The name of the attribute to delete. If it does not exist, nothing
            is done

        node: int
            The number of the node where to delete the attribute. It must be
            in dag.nodes()

        Returns
        -------
        None

        See Also
        --------
        get_attribute(...), erase_attribute(...), add_attribute_to_node(...)

        """
        try:
            if 'attributes' not in self.my_structure[node]:
                self.my_structure[node]['attributes'] = {}

            else:
                if attribute_name in self.my_structure[node]['attributes'].keys():
                    del self.my_structure[node]['attributes'][attribute_name]
        except:
            print('Requested node does not belong to the Dag')

    # --------------------------------------------------------------

    def erase_attribute(self, attribute_name):
        """
        Erase an attribute from all the nodes of the dag

        Parameters
        ----------
        attribute_name: str
            The name of the attribute to delete

        Returns
        -------
        None

        See Also
        --------
        get_attribute(...), add_attribute_to_node(...), erase_attribute_at_node(...)

        """
        for node in self.nodes():
            self.erase_attribute_at_node(attribute_name, node)

    # --------------------------------------------------------------

    def assign_ids_from_tree(self, t):
        """
        This function create an attribute on all nodes of the dag, that list the ids of the subtrees of `t` that are compressed by this node.

        :param t: Tree. Must be the tree from which the DAG has been calculcated. Otherwise, this function might behave oddly and return an error.
        """
        tree_type = self.my_compression['tree_type']
        node = t.get_attribute(tree_type + '_equivalence_class')
        attr = self.get_attribute(node)
        if "tree_ids" in attr.keys():
            tree_id = attr['tree_ids']
        else:
            tree_id = []
        tree_id.append(t.my_id)
        self.add_attribute_to_node("tree_ids", tree_id, node)
        for child in t.my_children:
            self.assign_ids_from_tree(child)

    # --------------------------------------------------------------

    def copy(self):
        """
        Returns a deep copy of the dag

        """
        d = Dag()
        d.my_structure = deepcopy(self.my_structure)
        d.my_roots = deepcopy(self.my_roots)
        d.my_leaves = deepcopy(self.my_leaves)
        d.my_compression = deepcopy(self.my_compression)
        d.my_origins = deepcopy(self.my_origins)
        return d

    # --------------------------------------------------------------

    def renumber_nodes(self, option=None):
        """
        All nodes in the dag are referenced by a unique number. This method allows to change the numbers with different manners.

        Parameters
        ----------
        option: variable, optional

            * Default is None. In this case, the DAG is renumbered from the
              leaves to the roots, with the numbers :math:`0` to :math:`n-1`, where :math:`n` is
              the number of nodes of the DAG.

            * If 'canonical'. The DAG is also renumbered from :math:`0` to :math:`n-1` with the following conditions, for any two nodes :math:`u` and :math:`v`:

                * if height(:math:`u`) :math:`>` height(:math:`v`), then :math:`n(u)>n(v)`
                * if height(:math:`u`) :math:`=` height(:math:`v`), then :math:`n(u)>n(v)` if and only if the list of children of :math:`u`, sorted by decreasing order of their numbers, is greater with respect to the lexicographical order than the list of :math:`v`


                This notion of canonical will be further discussed in a upcoming article.

            * if a list [nodes, numbers] is passed; where `nodes` is a list of nodes to renumber, and `numbers` the list of the new numbers, then only thoses nodes will be renumbered with the associated numbers. If the two list have elements in common, an Exception is raised. The two lists shall have the same length.

        """

        if isinstance(option, list):
            nodes = option[0]
            numbers = option[1]

            if not isinstance(nodes, list):
                nodes = [nodes]
            if not isinstance(numbers, list):
                numbers = [numbers]
            if not set(nodes).isdisjoint(set(numbers)):
                raise Exception("Nodes numbers and replacement numbers overlap")

            for n in self.nodes():
                children = [i if i not in nodes else numbers[nodes.index(i)] for i in self.my_structure[n]['children']]
                self.my_structure[n]['children'] = children
                if n in nodes:
                    self.my_structure[numbers[nodes.index(n)]] = self.my_structure.pop(n)

            leaves_copy = self.my_leaves.copy()
            for leaf in leaves_copy:
                if leaf in nodes:
                    self.my_leaves.remove(leaf)
                    self.my_leaves.append(numbers[nodes.index(leaf)])

            roots_copy = self.my_roots.copy()
            for root in roots_copy:
                if root in nodes:
                    self.my_roots.remove(root)
                    self.my_roots.append(numbers[nodes.index(root)])

            for origin in self.my_origins:
                root = self.my_origins[origin]
                if root in nodes:
                    self.my_origins[origin] = numbers[nodes.index(root)]


        elif option == 'canonical':

            if self.my_compression['tree_type'] != 'unordered':
                raise Exception("This method is meant for unordered DAGs only")

            nodes = self.nodes()
            self.renumber_nodes([nodes, [i + (max(nodes)+1) * len(nodes) for i in nodes]])
            nodes = self.nodes_sorted_by('height')
            i = 0
            for h in range(0, max(nodes.keys()) + 1):
                dic = {}
                for node in nodes[h]:
                    child = self.children(node)
                    child.sort(reverse=True)
                    dic[tuple(child)] = node
                while len(dic) > 0:
                    self.renumber_nodes([dic.pop(min(dic)), i])
                    i += 1

        else:
            nodes = self.nodes()
            self.renumber_nodes([nodes, [i + (max(nodes)+1) * len(nodes) for i in nodes]])
            nodes = self.nodes_sorted_by('height')
            i = 0
            for h in range(0, max(nodes.keys()) + 1):
                for node in nodes[h]:
                    self.renumber_nodes([node, i])
                    i += 1

    # --------------------------------------------------------------

    def nodes_sorted_by(self, properties=None, attributes=None):
        """
        This method returns a nested dictionary of all nodes of the Dag, sorted
        by their value of the provided properties and attributes.

        :param properties: string or list of strings, optional
            Default is None. If one or several properties are provided, they must be chosen among the bottom-up properties.
        :param attributes: string or list of strings, optional
            Default is None. If one or several attributes are provided, they must have been added in the Dag before.
        :return: dict
        """

        if properties is not None:
            if not isinstance(properties, list):
                props = [properties]
            else:
                props=properties
        else:
            props = []

        if attributes is not None:
            if not isinstance(attributes, list):
                attrs= [attributes]
            else:
                attrs=attributes
        else:
            attrs = []

        def recursive_defaultdict():
            return defaultdict(recursive_defaultdict)

        dico = recursive_defaultdict()

        nodes = self.nodes()

        self.get_property(self.my_roots[0], None, compute_level=1)
        for node in nodes:
            prop = self.get_property(node, None, compute_level=0)
            attr = self.get_attribute(node)

            my_command = 'dico'
            for propert in props:
                my_command += '[' + str(prop[propert]) + ']'
            for attribute in attrs:
                if isinstance(attr[attribute],str):
                    a='"{}"'.format(attr[attribute])
                else:
                    a=attr[attribute]
                my_command += '[' + str(a) + ']'
            if eval(my_command + '=={}'):
                my_command += '=[' + str(node) + ']'
            else:
                my_command += '.append(' + str(node) + ')'

            exec(my_command)

        def default_to_regular(d):
            if isinstance(d, defaultdict):
                d = {k: default_to_regular(v) for k, v in d.items()}
            return d

        return default_to_regular(dico)

    # --------------------------------------------------------------

    def __list_of_nodes_to_be_merged(self, l_nodes, tree_type, attribute):  # Private

        # create the list of nodes at same height that has same children and therefore must be merged

        dic_child = {}  # to each node, we associate the corresponding children
        for node in l_nodes:
            child = self.children(node).copy()
            if tree_type == 'unordered':
                child.sort()
            dic_child[node] = tuple(child)

        dic_parents = {}  # to each children, we associate the list of nodes that have exactly that children
        for node, child in dic_child.items():
            if attribute is None:
                if child in dic_parents:
                    dic_parents[child].append(node)
                else:
                    dic_parents[child] = [node]
            else:
                val = self.get_attribute(node, attribute)
                if (child, val) in dic_parents:
                    dic_parents[(child, val)].append(node)
                else:
                    dic_parents[(child, val)] = [node]

        # if a children has two or more nodes associated to it, we need to merge those nodes
        l_nodes_to_be_merged = []

        if attribute is None:
            for child in dic_parents:
                if len(dic_parents[child]) > 1:
                    l_nodes_to_be_merged.append(dic_parents[child])
        else:
            for (child, val) in dic_parents.keys():
                if len(dic_parents[(child, val)]) > 1:
                    l_nodes_to_be_merged.append(dic_parents[(child, val)])

        return l_nodes_to_be_merged

    # --------------------------------------------------------------

    def __merge_nodes(self, nodes_sorted, list_of_nodes, height):  # Private

        # given a list of nodes to be merged, merge them and change every reference to suppressed nodes to the new merged node
        while len(list_of_nodes) > 0:
            nodes = list_of_nodes.pop()
            index = min(nodes)
            nodes.remove(index)

            h = height + 1
            H = len(nodes_sorted)

            while h < H:
                h_nodes = nodes_sorted[h]
                for i in h_nodes:
                    child = self.children(i)
                    for x in range(0, len(child)):
                        if child[x] in nodes:
                            child[x] = index
                h += 1

            h_nodes = nodes_sorted[height]

            for i in nodes:
                n = self.my_structure.pop(i, None)
                h_nodes.remove(i)
                for origin in self.my_origins:
                    if i == self.my_origins[origin]:
                        self.my_origins[origin] = index
                if i in self.my_roots:
                    self.my_roots.remove(i)
                if i in self.my_leaves:
                    self.my_leaves.remove(i)

    # --------------------------------------------------------------

    def compress(self, **kwargs):
        """
        This method compress the provided DAG according to various criteria.

        Parameters
        ----------

        **kwargs:

            tree_type: str, optional
                Either 'unordered' (default) or 'ordered'. Any other string is
                interpreted as 'ordered'. When compressing the nodes of the
                original dag, this parameter indicates if it is important to
                consider the order of appearance of the children of a node
                (ordered) or if it is not (unordered). It corresponds to the
                relation of equivalence defined in the function is_isomorphic_to()
                from the treex.tree module

            attribute: str, optional
                If an attribute is provided, nodes will be merged only if their
                attribute value is the same.

        Notes
        -----
        The algorithm of compression basically lays on the following idea: starting
        from the leaves, and visiting the nodes by increasing height; at each
        height, the nodes that have exactly the same children (ordered or not,
        see tree_type) are chosen to be merged, and their parents are
        actualized, replacing the ancient nodes in their children by the new
        node.

        If at some height, there are no more nodes found to merge, the
        algorithm stops UNLESS the 'compression_rate' attribute of
        'my_compression' is set to 'unknown'; in that case the loop continue.

        Warnings
        --------
        If the provided DAG has already been compressed, it is still possible to compress it again by changing the criteria:

        * from 'ordered' to 'unordered'
        * from 'attribute' to no attribute

        Any other change of criteria would lead to a DAG whose compression would mean nothing.

        References
        ----------
        https://arxiv.org/abs/1904.05421 (preprint)

        See Also
        --------
        tree_to_dag(...), concatenate(...)

        """
        if 'tree_type' in kwargs.keys():
            tree_type = kwargs['tree_type']
        else:
            tree_type = 'unordered'

        if 'attribute' in kwargs.keys():
            attribute = kwargs['attribute']
        else:
            attribute = None

        # the 'zipper' algorithm merge the nodes starting from height 0
        nodes_sorted = self.nodes_sorted_by('height')

        for h in range(0, max(nodes_sorted.keys()) + 1):
            list_of_nodes = self.__list_of_nodes_to_be_merged(nodes_sorted[h], tree_type, attribute)
            if len(list_of_nodes) == 0 and self.my_compression['compression_rate'] != 'unknown':  # no more nodes to merge
                break
            self.__merge_nodes(nodes_sorted, list_of_nodes, h)

        self.renumber_nodes()
        self.erase_attribute('origin')
        self.__propagate_origins()

        self.my_compression['compression_rate'] = 'optimal'
        self.my_compression['tree_type'] = tree_type
        self.my_compression['attribute'] = attribute

    # --------------------------------------------------------------

    def assign_origin(self, origin):
        """
        Define the attribute 'my_origin' to the name specified. Works only for single-rooted DAG.
        """
        if len(self.my_roots) > 1:
            raise Exception("This function is meant for single rooted DAG only")
        else:
            self.my_origins = {}
            self.my_origins[origin] = self.my_roots[0]
            self.__propagate_origins()

    def __propagate_origins(self):
        # initialisation
        for origin in self.my_origins:
            root = self.my_origins[origin]
            if 'origin' in self.get_attribute(root):
                c_origin = self.get_attribute(root, 'origin')
            else:
                c_origin = {}
            c_origin[origin] = 1
            self.add_attribute_to_node('origin', c_origin, root)

        # propagation
        nodes = self.nodes_sorted_by('height')

        for h in range(max(nodes.keys()), -1, -1):
            for node in nodes[h]:
                origin = self.get_attribute(node, 'origin')
                for child in set(self.children(node)):
                    if 'origin' in self.get_attribute(child):
                        c_origin = self.get_attribute(child, 'origin')
                    else:
                        c_origin = {}

                    for o in origin:
                        if o not in c_origin:
                            c_origin[o] = 0
                        c_origin[o] += origin[o] * self.children(node).count(child)
                    self.add_attribute_to_node('origin', c_origin, child)

    def concatenate(self, forest, **kwargs):
        """
        This method is meant to concatenate to the provided DAG a forest made of trees, DAG (single or multi-rooted) or a mix of them.

        Parameters
        ----------

        forest: list
            Either trees or DAG, or mix of them. Tree objects will be converted into DAG before processing.

        **kwargs:

            tree_type, attribute, compression:
                Same as the function tree_to_dag(...). They are used relatively to the compression of possible trees in the forest. Default is 'unordered' tree type, no attributes and 'optimal' compression.

        Notes
        -----
        This method does not recompress the concatenated DAG. It just combine all the structures in one.

        See Also
        --------
        tree_to_dag(...), compress(...)
        """

        if not isinstance(forest, list):
            forest = [forest]

        if 'tree_type' in kwargs.keys():
            tree_type = kwargs['tree_type']
        else:
            tree_type = 'unordered'

        if 'attribute' in kwargs.keys():
            attribute = kwargs['attribute']
        else:
            attribute = None

        if any([isinstance(t, Tree) for t in forest]):
            dag_forest = []
            for t in forest:
                if isinstance(t, Tree):
                    dag_forest.append(tree_to_dag(t, **kwargs))
                else:
                    dag_forest.append(t)
        else:
            dag_forest = forest

        if len(self.nodes()) > 0:
            start = max(self.nodes())
            end = max(self.nodes())
        else:
            start = 0
            end = 0

        for dag in dag_forest:
            tmp_dag = dag.copy()
            start += max(tmp_dag.nodes()) + 1
            end += len(tmp_dag.nodes()) + max(tmp_dag.nodes()) + 1
            tmp_dag.renumber_nodes([tmp_dag.nodes(), list(range(start, end))])
            start = end

            self.my_origins.update(tmp_dag.my_origins.copy())
            self.my_structure.update(tmp_dag.my_structure.copy())
            self.my_leaves += tmp_dag.my_leaves.copy()
            self.my_roots += tmp_dag.my_roots.copy()

        self.my_compression['tree_type'] = tree_type
        self.my_compression['attribute'] = attribute
        self.my_compression['compression_rate'] = 'no_compression'
        self.renumber_nodes()


# --------------------------------------------------------------------------------------------------------

def __tree_to_optimaldag(tree, tree_type, attribute):

    list_subtrees = tree.subtrees_sorted_by('height', attribute)

    compteur = 0

    # Initialization dag
    dag = Dag()

    if attribute == None:
        dag.my_structure[compteur] = {'children': [], 'properties': {'height': 0, 'size': 1}}
        # Height 0
        for leaf in list_subtrees[0]:
            leaf._Tree__my_attributes[tree_type + '_equivalence_class'] = compteur
        compteur += 1
        dag.my_leaves = [0]
    else:
        dag.my_leaves = []
        for att_value in list_subtrees[0]:
            for leaf in list_subtrees[0][att_value]:
                dag.my_structure[compteur] = {'children': [],
                                              'properties': {'height': 0, 'size': 1}, 'attributes': {attribute: att_value}}
                leaf._Tree__my_attributes[tree_type + '_equivalence_class_with_attribute'] = compteur

                if compteur not in dag.my_leaves:
                    dag.my_leaves.append(compteur)
            compteur += 1

    if attribute == None:
        # Height h
        for h in range(1, tree.get_property('height' , compute_level=1) + 1):
            subtrees_h = list_subtrees[h]
            seq = []
            for i in range(len(subtrees_h)):
                subtree_h_i = subtrees_h[i]

                list_labels_subtrees = []
                for child in subtree_h_i.my_children:
                    list_labels_subtrees.append(child._Tree__my_attributes[tree_type + '_equivalence_class'])

                if tree_type == 'unordered':
                    list_labels_subtrees.sort()

                if list_labels_subtrees in seq:
                    indice = seq.index(list_labels_subtrees)
                    subtree_h_i._Tree__my_attributes[tree_type + '_equivalence_class'] = \
                    subtrees_h[indice]._Tree__my_attributes[tree_type + '_equivalence_class']
                else:
                    subtree_h_i._Tree__my_attributes[tree_type + '_equivalence_class'] = compteur
                    dag.my_structure[compteur] = {
                        'properties': {'height': h, 'size': subtree_h_i.get_property('size',compute_level=1)},
                        'children': list_labels_subtrees}
                    compteur += 1
                seq.append(list_labels_subtrees)
    else:
        for h in range(1, tree.get_property('height' , compute_level=1) + 1):
            for att_value in list_subtrees[h]:
                subtrees_h_att = list_subtrees[h][att_value]
                seq = []
                for i in range(len(subtrees_h_att)):
                    subtree_h_i = subtrees_h_att[i]

                    list_labels_subtrees = []
                    for child in subtree_h_i.my_children:
                        list_labels_subtrees.append(
                            child._Tree__my_attributes[tree_type + '_equivalence_class_with_attribute'])

                    if tree_type == 'unordered':
                        list_labels_subtrees.sort()

                    if list_labels_subtrees in seq:
                        indice = seq.index(list_labels_subtrees)
                        subtree_h_i._Tree__my_attributes[tree_type + '_equivalence_class_with_attribute'] = \
                        subtrees_h_att[indice]._Tree__my_attributes[tree_type + '_equivalence_class_with_attribute']
                    else:
                        subtree_h_i._Tree__my_attributes[tree_type + '_equivalence_class_with_attribute'] = compteur
                        dag.my_structure[compteur] = {
                            'properties': {'height': h, 'size': subtree_h_i.get_property('size',compute_level=1)},
                            'attributes': {attribute: att_value}, 'children': list_labels_subtrees}
                        compteur += 1
                    seq.append(list_labels_subtrees)

    dag.my_roots = [compteur - 1]
    return dag


# --------------------------------------------------------------

def __tree_to_treedag(tree):
    d = Dag()

    list_subtrees = tree.subtrees_sorted_by('height')

    for h in range(0, tree.get_property('height',compute_level=1) + 1):
        subtrees_h = list_subtrees[h]
        for subtree in subtrees_h:
            d.my_structure[subtree.my_id] = {'children': []}
            for child in subtree.my_children:
                d.my_structure[subtree.my_id]['children'].append(child.my_id)
            prop = subtree.get_property(compute_level=1)
            d.my_structure[subtree.my_id]['properties'] = prop
            attr = subtree.get_attribute()
            d.my_structure[subtree.my_id]['attributes'] = attr

    d.assign_leaves()
    d.assign_roots()
    d.renumber_nodes()

    return d


def tree_to_dag(tree, **kwargs):
    """
    Convert a Tree object to a Dag class object

    Parameters
    ----------

    **kwargs:

        tree_type: str, optional

            Either 'unordered' (default) or 'ordered'. Any other string is
            interpreted as 'ordered'. It corresponds to the relation of
            equivalence defined in the function is_isomorphic_to() from the
            treex.tree module

        attribute: str, optional
            If given, the constructed dag will inherit of the attribute (it must
            exist in the tree) and the classes of equivalence will depend on the
            values. Default is None.

        compression: str, optional
            Default is 'optimal'. According to the tree type and attributes
            provided, the DAG will compress the tree in the more efficient
            way. Any other argument will lead to no_compression at all.

        origin: str, optional
            A specific origin name to give to the DAG. If not specified, the
            ID of the tree will be used.

    Returns
    -------
    Dag

    Notes
    -----
    The subtrees in the tree are assign with a number, corresponding to their
    class of equivalence number (ordered or not, see tree_type). All subtrees
    with same number are designed in the dag with a unique node, which number
    is the same

    References
    ----------
    DOWNEY, SETHI, AND TARJAN "Variations on the common subexpression problem"
    J. A.C.M. 27 (1980), 758-771
    """
    if 'tree_type' in kwargs.keys():
        tree_type = kwargs['tree_type']
    else:
        tree_type = 'unordered'

    if 'attribute' in kwargs.keys():
        attribute = kwargs['attribute']
    else:
        attribute = None

    if 'compression' in kwargs.keys():
        compression = kwargs['compression']
    else:
        compression = 'optimal'

    if 'origin' in kwargs.keys():
        origin = kwargs['origin']
    else:
        origin = str(tree.my_id)

    if compression == 'optimal':
        dag = __tree_to_optimaldag(tree, tree_type, attribute)
        dag.my_compression['compression_rate'] = 'optimal'
    else:  # for now, the opposite of optimal compression is no compression at all
        dag = __tree_to_treedag(tree)
        dag.my_compression['compression_rate'] = 'no_compression'

    dag.my_compression['tree_type'] = tree_type
    dag.my_compression['attribute'] = attribute
    dag.assign_origin(origin)

    return dag

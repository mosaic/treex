# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.lossy_compression.lossy_compression
#
#       File author(s):
#           Salah Habibeche <habibechesalaheddine1@gmail.com>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: shutil, utils.kmedoid_clustering

To do

- File author(s): Salah Habibeche <habibechesalaheddine1@gmail.com>
- File contributor(s): Romain Azais <romain.azais@inria.fr>
"""

from treex  import *
import shutil
from treex.utils.kmedoid_clustering import get_optimal_kmedoid_clustering, kmedoid_clustering, get_medoid_of_cluster
from treex.analysis.edit_distance.zhang_labeled_trees import *
import os
from treex.utils.save import save_object,read_disk_size
import random
import math 

def clean_dag(d):
	for n in d.my_structure.keys():
		d.my_structure[n]['properties'].pop('number_of_leaves',None)
		d.my_structure[n]['properties'].pop('depth',None)
		d.my_structure[n]['properties'].pop('index_among_siblings',None)

def __get_clustered_cut(clusters):
    #form a list of clusters return a cut
    l=[]
    m=0
    coupe=[]
    c=[]
    for cluster in clusters:
        c.append(cluster.copy())
    while(len(c)!=0):
        ci=random.choice(c)
        while(len(ci)!=0):
            ti=random.choice(ci)
            ad=True
            for  tr in coupe:
                if ti in tr.list_of_subtrees() or tr in ti.list_of_subtrees():
                    ad=False
                    break
            if ad:
                coupe.append(ti)
            ci.remove(ti)
        c.remove(ci)
        m+=1
    return(coupe)
def __clusterd_id_to_tree(tr,cl):
    clr=[]
    for cluster in cl:
        cli=[]
        for tree_id in cluster:
            cli.append(tr.subtree(tree_id))
        clr.append(cli)
    return clr
def get_cut(t_tree,N,dist):
    # generate N cut from the tree t_tree
    all_cut=[]
    all_cut_id=[]
    j=round(N/100)
    forest=[st.my_id for st in t_tree.list_of_subtrees() if st.get_attribute("flag_cut")]  
    ki=get_optimal_kmedoid_clustering(forest,dist)
    for i in range(ki[0]-j,ki[0]+j):
        
        clu=kmedoid_clustering(i,forest,dist)
        for j in range(0,50):
            ci=__get_clustered_cut(__clusterd_id_to_tree(t_tree,clu[0]))
            if(len(ci)!=1):
                ci_id=[int(x.my_id) for x in ci]
             
                rl=list(set(ci_id))
             
                if rl not in all_cut_id:
                    all_cut.append(ci)
                    all_cut_id.append(rl)
        i+=1
    return all_cut_id
def __calcul_represantant(itree,dis,attrib,ri):
    d_dist={}
    clso=[]
    rep=[]
  
    for r in ri:
      
      
      
        d_dist.update(r)
        sorted_clu = {k: v for k, v in sorted(r.items(), key=lambda item: item[1])}
        clso.append(sorted_clu)
        id_rep=list(sorted_clu.keys())[0]
        rep.append(itree.subtree(id_rep))
    #evaluation de r0

    super_tree=Tree()
    super_tree.add_attribute_to_id(attrib,0)
    si=0
    for tr in rep:
        super_tree.add_subtree(tr)
        si+=d_dist[tr.my_id]
    df=tree_to_dag(super_tree,attribute=attrib)
 
    
    gr=si+len(df.nodes()) / super_tree.get_property("size")
  
    changed=bool(True)
    u=-1
    a=-1
    while(changed):
       
        while(u==a):
            u=random.randint(0,len(rep)-1)
        a=u
        crep=rep.copy()
        crep.remove(crep[u])
    

        su=0    #somme of coeff de centralite
        ln=0    #len sueper tree of all  other rep
        for tr in crep:
            su+=d_dist[tr.my_id]
            ln=tr.get_property("size")
        
        bd=Dag()
        if(len(ri)!=1):
            bd.concatenate(crep,attribute=attrib)
            bd.compress(attribute=attrib)
    
        ej0=math.inf
        for j in range(0,len(clso[u])):
          
            rj=    itree.subtree(list(clso[u])[j])
            cbd=bd.copy()
            cbd.concatenate(rj,attribute=attrib)
            cbd.compress(attribute=attrib)
            ej=su+d_dist[rj.my_id]+(len(cbd.nodes()))/(ln+rj.get_property('size'))
        
            if(ej<ej0):
            
                ej0=ej
            else:
                j=j-1
                break
        
        if (rep[u]!= itree.subtree(list(clso[u])[j])):
            
            rep[u]= itree.subtree(list(clso[u])[j])
        else:
            changed=False
    return rep
def __approximation(ttree,mapping,edges_attr):
    #replace trees belong to a same cluster by a single representative of the cluster
    c_tree=ttree.rebuild()
    d_parent={}
    for tr in c_tree['tree'].list_of_subtrees():
        if tr==c_tree['tree']:
            continue
        d_parent[tr.my_id]=tr.my_parent.my_id 
   
    for key in mapping.keys():
        id_rep=[x for x in  c_tree['mapping'].keys() if c_tree['mapping'][x]==key][0]
        represantant=c_tree['tree'].subtree(id_rep)
        
        for ti in mapping[key]:
            id_tree=([x for x in  c_tree['mapping'].keys() if c_tree['mapping'][x]==ti])[0]
            to_delete=c_tree['tree'].subtree(id_tree)
            id_parent=d_parent[id_tree]
            new_tree=represantant.copy()
            for att in edges_attr:            
                new_tree.add_attribute_to_id(att,to_delete.get_attribute(att))
            c_tree['tree'].delete_subtree_at_id(id_tree)
            c_tree['tree'].add_subtree_to_id(new_tree,id_parent)
    return c_tree['tree']
def __approximation_from_one_cut(itree,cut,dist,edges_attr,attrib):
    #provide the approximation corespondante to an input cut
    r=get_optimal_kmedoid_clustering(cut,dist)
    
    rep0=[]
    for cluster in r[1][0]:
            rep0.append(get_medoid_of_cluster(cluster,dist)[1])
    rep=__calcul_represantant(itree,dist,attrib,rep0)
    irep=[x.my_id for x in rep]
   
    mapp={}
    for i in range(0,len(rep)):
        
        l=[]
        for ti in r[1][0][i]:
            l.append(ti)  
        mapp[rep[i].my_id]=l

    atree=__approximation(itree,mapp,edges_attr)
    atree.get_property(compute_level=2)
    return atree,mapp
def __evaluation_of_one_approximation(atree,matching,exacte_eval,attrib,dist,tmp_directory,itree=None,local_distance=None):
    #provide scores for an input approximation
    approximated_dag=tree_to_dag(atree,attribute=attrib)
    save_object( approximated_dag ,tmp_directory+ '/dag.p' )
    tree_size=read_disk_size(tmp_directory+ '/init_tree.p' )
    dag_size=read_disk_size(tmp_directory+ '/dag.p' )
    tau=1-dag_size/tree_size
    if exacte_eval:
        sigma=zhang_edit_distance(atree,itree,local_distance,False)
    else:
        sigma=0
        for key in matching.keys():
            for ti in matching[key]:
                if key==ti:
                    continue
                sigma+=dist[(key,ti)]
    return approximated_dag,tau,sigma

def lossy_compression(tree,dict_result,exact_compression,label_attribute=None,
    local_distance=None,edge_attribute=None,cut_generator=get_cut,cut_number=500):
    """
	provide a lossy compression of an input tree

	Parameters
	----------
	tree: Tree
	dict_result: dict
	exact_compression: boolean
	label_attribute: tuple
	local_distance: function, optional
    
	edge_attribute: list, optional
	cut_generator: function, optional
	cut_number: int, optional

	Returns
	-------
	dag
		the Lossy compression of tree
	"""
    #create a Temporary file
    n=0
    directory_list= [f.path for f in os.scandir() if f.is_dir()]
    while True:
        if './tmp'+str(n) not in directory_list:
            break
        n+=1
    tmp_directory='./tmp'+str(n)
    os.mkdir(tmp_directory)
    #evaluate the size of the tree in the disque
  
    ini_dag=tree_to_dag(tree,attribute=label_attribute)
    tree2 = tree.copy()
    tree2.erase_attribute('lstring_successor')
    tree2.erase_attribute('unordered_equivalence_class_with_attribute')
    tree2.erase_attribute('module')
    tree2.erase_attribute('time')
    tree2.erase_attribute('state')
    tree2.erase_attribute('divangle')
    tree2.erase_attribute('insangle')
    tree2.erase_attribute('length')

    d = tree_to_dag(tree2,compression=None)
    clean_dag(d)
    save_object( d , tmp_directory+'/init_tree.p' )
    save_object( ini_dag ,tmp_directory+ '/init_dag.p' )
    #calculate matrix of similariry between subtrees 
    dist=zhang_edit_distance(tree,tree,label_attribute,local_distance,True)['matrix_of_trees_distances']
    delta=zhang_edit_distance(tree,None,label_attribute,local_distance,verbose=False)
    #generation of cuts
    ls=cut_generator(tree,cut_number,dist)
    all_approximation=[]
    for cu in ls:
       approximated_tree,approximation_mapping=__approximation_from_one_cut(tree,cu,dist,edge_attribute,label_attribute) 
       approximated_dag,tau,sigma=__evaluation_of_one_approximation(approximated_tree,approximation_mapping,exact_compression,label_attribute,dist,tmp_directory,tree,local_distance)
       all_approximation.append([approximated_tree,approximated_dag,sigma/delta,tau])

    compression={}
    loss={}
    
    for ci in dict_result['compression']:
        rest_approximation=[x for x in all_approximation if x[3]>ci]
        if(len(rest_approximation)!=0):
            sigma_min=min([x[2] for x in rest_approximation])
            compression[ci]=[x for x in rest_approximation if x[2]==sigma_min ][0]

    for ci in dict_result['loss']:
        rest_approximation=[x for x in all_approximation if x[2]<ci]
        if(len(rest_approximation)!=0):
            tau_max=max([x[3] for x in rest_approximation])
            loss[ci]=[x for x in rest_approximation if x[3]==tau_max][0]


    shutil.rmtree(tmp_directory)
    return compression,loss

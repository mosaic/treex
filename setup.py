#!/usr/bin/env python
# -*- coding: utf-8 -*-

# {# pkglts, pysetup.kwds
# format setup arguments

from setuptools import setup, find_packages


short_descr = "Python library for handling tree data"
readme = open('README.rst').read()
history = open('HISTORY.rst').read()

# find packages
pkgs = find_packages('src')



setup_kwds = dict(
    name='treex',
    version="2.1.2",
    description=short_descr,
    long_description=readme + '\n\n' + history,
    author="Romain Azais",
    author_email="romain.azais@inria.fr",
    url='',
    license='LGPL',
    zip_safe=False,

    packages=pkgs,
    
    package_dir={'': 'src'},
    setup_requires=[
        ],
    install_requires=[
        ],
    tests_require=[
        "coverage",
        "mock",
        "nose",
        ],
    entry_points={},
    keywords='',
    
    test_suite='nose.collector',
    )
# #}
# change setup_kwds below before the next pkglts tag

setup_kwds['entry_points']['console_scripts'] = ["tree_editor = treex.app.tree_editor:main",
                                                 "probability_tree_editor = treex.app.probability_tree_editor:main"]

# do not change things below
# {# pkglts, pysetup.call
setup(**setup_kwds)
# #}

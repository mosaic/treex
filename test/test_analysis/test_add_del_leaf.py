# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_analysis/test_add_del_leaf
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.simulation.random_recursive_trees import gen_random_tree
from treex.analysis.edit_distance.add_del_leaf import constrained_unordered_edit_distance, constrained_tree_matching

def test_distpos():
	t = gen_random_tree(20)
	s = gen_random_tree(30)
	dist = constrained_unordered_edit_distance(t,s)
	assert dist>=0

def test_distnull():
	t = gen_random_tree(30)
	dist = constrained_unordered_edit_distance(t,t)
	assert dist == 0

def test_distlist1():
	t = gen_random_tree(30)
	s = gen_random_tree(10)
	dist = constrained_unordered_edit_distance([t,s],t)
	assert (t.my_id,t.my_id) in dist.keys()

def test_distlist2():
	t1 = gen_random_tree(5)
	t2 = gen_random_tree(5)
	s1 = gen_random_tree(5)
	s2 = gen_random_tree(5)
	dist = constrained_unordered_edit_distance([t1,t2],[s1,s2])
	assert (t1.my_id,s2.my_id) in dist.keys()

def test_root_to_root():
	t = gen_random_tree(30)
	s = gen_random_tree(30)
	match = constrained_tree_matching(t,s)
	assert match[t.my_id] == s.my_id or match[s.my_id] == t.my_id

def cost_function_ex1(tree1,tree2):
	if tree2==None:
		return tree1.get_property('size')
	else:
		return 0

def test_cost_function1():
	t = gen_random_tree(10)
	s = gen_random_tree(10)
	dist1 = constrained_unordered_edit_distance(t,s,cost_function_ex1)
	dist2 = constrained_unordered_edit_distance(t,s)
	assert dist1 == dist2

def cost_function_ex2(tree1,tree2):
	label_distance='label_for_distance'
	if tree2==None: # cost for creation of tree1
		cout=0
		for i in range(len(tree1.get_attribute('label_for_distance'))):
			cout+=tree1.get_attribute('label_for_distance')[i]
		for child in tree1.my_children:
			cout+=cost_function_ex2(child,None)
	else: # tree1 and tree2 must have only one node
		cout= sum( [ abs(tree1.get_attribute('label_for_distance')[i]-tree2.get_attribute('label_for_distance')[i]) for i in range(len(tree1.get_attribute('label_for_distance'))) ] ) 
	return cout

def give_label_dist(tree,value):
	if isinstance(value,list):
		tree.add_attribute_to_id('label_for_distance',value)
	else:
		tree.add_attribute_to_id('label_for_distance',[value])
	for child in tree.my_children:
		give_label_dist(child,value)

def test_cost_function2():
	t = gen_random_tree(10)
	s = gen_random_tree(10)
	give_label_dist(t,[1,2])
	give_label_dist(s,[3,2])
	dist = constrained_unordered_edit_distance(t,s,cost_function_ex2)
	assert dist>=0

def test_cost_function_list1():
	t = gen_random_tree(10)
	s = gen_random_tree(10)
	give_label_dist(t,[1,2])
	give_label_dist(s,[3,2])
	dist = constrained_unordered_edit_distance([t,s],[s,t],cost_function_ex2)
	assert (t.my_id,s.my_id) in dist.keys()

def test_cost_function_list2():
	t = gen_random_tree(10)
	s = gen_random_tree(10)
	give_label_dist(t,[1,2])
	give_label_dist(s,[3,2])
	dist = constrained_unordered_edit_distance(t,[s,t],cost_function_ex2)
	assert (t.my_id,s.my_id) in dist.keys()

def test_cost_function_list3():
	t = gen_random_tree(10)
	s = gen_random_tree(10)
	give_label_dist(t,[1,2])
	give_label_dist(s,[3,2])
	dist = constrained_unordered_edit_distance([t,s],s,cost_function_ex2)
	assert (t.my_id,s.my_id) in dist.keys()
import os
import argparse
import pickle
import logging

from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog, QFileDialog
from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QComboBox, QCheckBox
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QGridLayout, QFormLayout
from PyQt5.QtGui import QFont
from PyQt5.Qt import Qt, QSizePolicy, QDoubleValidator, QIntValidator, QLocale
from PyQt5.QtCore import pyqtSignal


class NodeAttributeEditor(QWidget):
    editingFinished = pyqtSignal()

    def __init__(self, attribute_value, attribute_type, parent=None):
        super().__init__(parent)

        self.global_attributes = {}
        self.global_name = ""

        self.layout = QHBoxLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.attribute_type = attribute_type

        if self.attribute_type == 'bool':
            self.value_edit = QCheckBox()
            self.value_edit.setChecked(attribute_value)
            self.value_edit.stateChanged.connect(self.editingFinished)
        else:
            self.value_edit = QLineEdit(str(attribute_value))
            if self.attribute_type == 'int':
                self.value_edit.setValidator(QIntValidator())
            elif self.attribute_type == 'float':
                self.value_edit.setValidator(QDoubleValidator())
            self.value_edit.editingFinished.connect(self.editingFinished)
        self.layout.addWidget(self.value_edit)

        self.global_value_edit = QComboBox()
        self.global_value_edit.currentTextChanged.connect(self.set_global_value)
        self.layout.addWidget(self.global_value_edit)

        self.set_global_attributes({})

    def value(self):
        if self.attribute_type == 'bool':
            return self.value_edit.isChecked()
        else:
            if self.attribute_type == 'int':
                return int(self.value_edit.text())
            elif self.attribute_type == 'float':
                return float(self.value_edit.text())
            elif self.attribute_type == 'str':
                return self.value_edit.text()
        return

    def has_global_value(self):
        return self.global_name == "" or self.global_name not in self.global_attributes

    def set_global_value(self, global_name):
        self.global_name = global_name
        if global_name in self.global_attributes:
            self.global_value_edit.setCurrentText(self.global_name)
        self.update_value_edit()

    def update_value_edit(self):
        if self.global_name == "" or self.global_name not in self.global_attributes:
            self.value_edit.setEnabled(True)
        else:
            self.value_edit.setText(str(self.global_attributes[self.global_name]))
            self.editingFinished.emit()
            self.value_edit.setEnabled(False)

    def set_global_attributes(self, global_attributes):
        self.global_attributes = global_attributes

        self.global_value_edit.clear()
        self.global_value_edit.addItems([""]+list(self.global_attributes.keys()))

        if self.global_name in [""]+list(self.global_attributes.keys()):
            self.global_value_edit.setCurrentText(self.global_name)
        else:
            self.global_value_edit.setCurrentText("")
        if len(self.global_attributes) == 0:
            self.global_value_edit.setVisible(False)
        else:
            self.global_value_edit.setVisible(True)



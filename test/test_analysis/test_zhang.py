# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_zhang_dag
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.simulation.random_recursive_trees import gen_random_tree
from treex.lossless_compression.dag import tree_to_dag
from treex.analysis.edit_distance.zhang_dag import zhang_edit_distance_dags
from treex.analysis.edit_distance.zhang_labeled_trees import zhang_edit_distance

def local_distance(v1=None,v2=None):
	if isinstance(v1,tuple)==True:
		if v2==None:
			cout=0
			v=list(v1)
			for i in range(len(v)):
				cout+=v[i]
			return cout
		else:
			d=len(v1)
			return sum( [ abs(v1[i]-v2[i]) for i in range(0,d) ])  
	else: 
		if v2==None:
			return v1
		else:
			return abs(v1-v2)


def test_zhang_dag_1():
	for i in range(10):
		t = gen_random_tree(20)
		s = gen_random_tree(20)
		
		give_label_dist(t,(1))
		give_label_dist(s,(1))

		ref = zhang_edit_distance(t,s,'label_for_distance',local_distance,verbose=False)
	
		d_t = tree_to_dag(t)
		d_s = tree_to_dag(s)

		dist_ns = zhang_edit_distance_dags(d_t,d_s,algo='NS')

		dist_ek = zhang_edit_distance_dags(d_t,d_s,algo='EK')

		assert ref == dist_ns and ref == dist_ek

def test_zhang_dag_2():
	t = gen_random_tree(10)
	s = gen_random_tree(10)
	
	d_t = tree_to_dag(t)
	d_s = tree_to_dag(s)

	dist_ns = zhang_edit_distance_dags(d_t,d_s,algo='NS',all_info=True)
	dist_ek = zhang_edit_distance_dags(d_t,d_s,algo='EK',all_info=True)

	assert 'Flow' in dist_ns.keys() and 'distance' in dist_ek.keys()



def cost_function_ex(tree1=None,tree2=None):
	# compute  distance between the vector associated to tree1 and the vector 
	# associated to tree2 
	# Parameters
	# ----------
	# tree1,tree2: treex tree
	# Returns
	# -------    
	# the distance  between the 2 vectors 
	label_distance='label_for_distance'
	if(tree1==None and tree2==None):
		return label_distance;
	if isinstance(tree1.get_attribute(label_distance),tuple)==True:
		if tree2==None: # cost for creation of tree1
			cout=0
			v=list(tree1.get_attribute('label_for_distance'))
			for i in range(len(v)):
				cout+=v[i]
		else: # tree1 and tree2 must have only one node
			v1=list(tree1.get_attribute('label_for_distance'))
			v2=list(tree2.get_attribute('label_for_distance'))
			d=len(v1)
			cout= sum( [ abs(v1[i]-v2[i]) for i in range(0,d) ])  
		return cout
	else:
		if tree2==None:
			return tree1.get_attribute('label_for_distance')
		else:
			return abs(tree1.get_attribute('label_for_distance')-tree2.get_attribute('label_for_distance'))


def give_label_dist(tree,value):
	# associate attribute values ​​with the tree	
	# Parameters
	# ----------
	# tree: treex tree
	# value: number vector
	tree.add_attribute_to_id('label_for_distance',value)
	for child in tree.my_children:
		give_label_dist(child,value)

# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_ciphering
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

import os
import unittest

from treex.ciphering import *
from treex.ciphering_dag import *
from treex.simulation import gen_random_tree
import math
from random import seed
import numpy as np
from scipy.optimize import curve_fit

seed(1234)

def assign_random_feature(t, nb_feat):
    if isinstance(t, list):
        for elt in t:
            assign_random_feature(elt, nb_feat)
    else:
        t.add_attribute_to_id('Feature', randrange(nb_feat))
        for c in t.my_children:
            assign_random_feature(c, nb_feat)

def shuffle_subtrees(t):
    random.shuffle(t.my_children)
    for c in t.my_children:
        shuffle_subtrees(c)

def perturbate_attribute(t):

    used_features={}

    for st in t.list_of_subtrees():
        f = st.get_attribute('Feature')
        if f not in used_features.keys():
            used_features[f]=[]
        used_features[f].append(st)

    features = list(used_features.keys())

    candidates = [f for f in features if len(used_features[f])>1]

    if len(candidates)>0:
        f = random.choice(candidates)
        u = random.choice(used_features[f])

        u.add_attribute_to_id('Feature', max(features)+1)
    else:
        f,g = random.sample(features,2)
        u = random.choice(used_features[f])
        u.add_attribute_to_id('Feature', g)

def gen_trees(nb,feat):
    t1 = gen_random_tree(nb)
    assign_random_feature(t1, feat)
    t2 = t1.copy()
    shuffle_subtrees(t2)

    assert t1.is_isomorphic_to(t2)

    return (t1,t2)

class TestBijection(unittest.TestCase):
    '''
    Tests the treex.ciphering Bijection class
    '''

    def setUp(self):
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)

    def test_bijection(self):
        f = Bijection({'a':2})
        assert f=={'a':2}
        assert f.inverse=={2:'a'}

        f.extend('b',3)
        assert f=={'a': 2, 'b': 3}
        assert f.inverse=={2: 'a', 3: 'b'}

        del f['b']
        assert f == {'a': 2}
        assert f.inverse == {2: 'a'}

        f = Bijection({'a':1,'b':2,'c':3,'d':4})
        g = Bijection({1:'a',2:'b',3:'c'})
        h = f.compose(g)
        assert h.keys()==h.inverse.keys()

class TestCipheringIsomorphism(unittest.TestCase):
    '''
    Tests the treex.ciphering ciphering isomorhism algorithm
    '''

    def setUp(self):
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)

    def test_preprocessing(self):
        t1,t2 = gen_trees(100,10)

        tu = PreProcessing(t1,t2,labels="Feature")

        assert tu[0]

        _,state=tu

        assert abs(math.log10(state.current_size())-state.current_size(True))<1e-3
        assert abs(math.log10(isom_count(t1)) - isom_count(t1,log=True)) < 1e-3

    def test_backtracking(self):
        t1,t2 = gen_trees(100,500)

        tu = PreProcessing(t1,t2,labels="Feature")

        assert tu[0]

        _,state=tu

        tu = BackTracking(state,t1,t2)

        assert tu[0]

        _,f,phi=tu

        used_features = list(
            set([t1.list_of_subtrees()[i].get_attribute('Feature') for i in range(len(t1.list_of_subtrees()))]))

        assert len(f)==len(used_features)
        assert len(phi)==100

    def test_ciphering(self):
        t1, t2 = gen_trees(100, 10)
        tu = ciphering_isomorphism(t1,t2,"Feature")
        assert tu[0]

        t1, t2 = gen_trees(100, 100)
        tu = ciphering_isomorphism(t1,t2,"Feature")
        assert tu[0]

        t1, t2 = gen_trees(100, 500)
        tu = ciphering_isomorphism(t1,t2,"Feature")
        assert tu[0]

        t1, t2 = gen_trees(100, 10)
        perturbate_attribute(t2)
        tu = ciphering_isomorphism(t1,t2,"Feature")
        assert tu[0]==False

def cubic(x,a,b,c,d):
    return a*np.power(x,3)+b*np.power(x,2)+c*x+d

def RMSE(f, objective, param):
    # Root Mean Square Error
    s=0
    for k in f.keys():
        s += (objective(k, *param) - f[k]) ** 2
    return np.sqrt(s / len(f.keys()))

def scipy_fit(f, objective):
    x = []
    y = []
    for k in f.keys():
        x.append(k)
        y.append(f[k])

    param, _ = curve_fit(objective, x, y)

    return param

def poly(x):
    return np.power(np.array(x)-1,1)*np.power(np.array(x)+1,2)/4

def gen_regtree(nb):
    t1 = gen_random_tree(nb)
    for u in t1.list_of_subtrees():
        u.add_attribute_to_id('Feature',np.random.normal(0,1))
    t2=t1.copy()
    shuffle_subtrees(t2)
    for v in t2.list_of_subtrees():
        v.add_attribute_to_id('Feature',poly(v.get_attribute('Feature')))

    for u in t1.list_of_subtrees()+t2.list_of_subtrees():
        lab = u.get_attribute('Feature')
        lab += np.random.normal(0,0.5)
        u.add_attribute_to_id('Feature',lab)

    return (t1,t2)


class TestRegressionCiphering(unittest.TestCase):
    """
    Test the regression ciphering procedure
    """
    def setUp(self):
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)

    def test_regression_ciphering(self):

        t1,t2 = gen_regtree(250)

        tu = regression_ciphering(t1,t2,"Feature",cubic,scipy_fit,RMSE)
        assert tu[0]

class TestCIpheringDag(unittest.TestCase):
    '''
    Test the ciphering dag procedure
    '''

    def setUp(self):
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)

    def test_ciphering_dag_exact(self):
        t = gen_random_tree(100)
        assign_random_feature(t, 50)

        d = tree_to_ciphering_dag(t,"Feature")

        tb = decompress_tree(d,"Feature")

        tu = ciphering_isomorphism(t,tb,"Feature")
        assert tu[0]

    # def test_ciphering_dag_approximate(self):
    #     t,_ = gen_regtree(100)
    #
    #     d = tree_to_ciphering_dag(t,"Feature",False,epsilon=1,objective=cubic,estimate=scipy_fit,error=RMSE)
    #
    #     #tb = decompress_tree(d,"Feature")
    #
    #     #tu = ciphering_isomorphism(t,tb,"Feature")
    #     #assert tu[0]

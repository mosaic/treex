# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.analysis.hidden_markov
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: treex.tree

- File author(s): Romain Azais <romain.azais@inria.fr>

References
----------

This code is an implementation of Viterbi algorithm
for hidden Markov tree models presented in

DURAND, GONCALVES, GUEDON
"Computational methods for hidden Markov tree models-an application to wavelet trees"
IEEE Transactions on Signal Processing (2004)
"""

### Bottom-up phase of Viterbi algorithm ###
def __assign_conditional_probabilities_to_tree(t , obs_name , transition_matrix , pdf_emission , emission_parameters):
	nb_labels = len(transition_matrix)

	obs = t.get_attribute(obs_name)

	### Compute type only based on observation
	cond_prob = []
	for lab in range(nb_labels):
		cond_prob.append( pdf_emission(obs , lab , emission_parameters) )
	est_typ_obs_only = cond_prob.index(max(cond_prob))
	t.add_attribute_to_id('estimated_type_obs_only' , est_typ_obs_only)

	if t.my_children == []:

		### Compute delta_node
		delta_node = []
		for lab in range(nb_labels):
			delta_node.append( pdf_emission(obs , lab , emission_parameters) )

		t.add_attribute_to_id('viterbi_delta_node' , delta_node)

		### Compute delta_parent_node
		delta_parent_node = []
		maximum_indices = []
		for lab in range(nb_labels):

			prod = []
			for lab_pr in range(nb_labels):
				prod.append( delta_node[lab_pr]*transition_matrix[lab][lab_pr] )
			delta_parent_node.append( max(prod) )
			maximum_indices.append( prod.index(max(prod)) )

		t.add_attribute_to_id('viterbi_delta_parent_node' , delta_parent_node)
		t.add_attribute_to_id('viterbi_maximum_indices' , maximum_indices)

	else:

		### Read delta_parent_node of the children
		delta_children = []
		for child in t.my_children:
			__assign_conditional_probabilities_to_tree(child , obs_name , transition_matrix , pdf_emission , emission_parameters)
			delta_child = child.get_attribute('viterbi_delta_parent_node')
			delta_children.append( delta_child )

		### Compute delta_node
		delta_node = []
		for lab in range(nb_labels):
			
			prod = 1.0
			for count_children in range( len(delta_children) ):
				prod *= delta_children[count_children][lab]

			prod *= pdf_emission(obs,lab,emission_parameters)
			delta_node.append( prod )

		t.add_attribute_to_id('viterbi_delta_node' , delta_node)

		### Compute delta_parent_node (only if the node is not the root)
		if t.get_property('depth')>0:
			delta_parent_node = []
			maximum_indices = []
			for lab in range(nb_labels):

				prod = []
				for lab_pr in range(nb_labels):
					prod.append( delta_node[lab_pr]*transition_matrix[lab][lab_pr] )
				delta_parent_node.append( max(prod) )
				maximum_indices.append( prod.index(max(prod)) )

			t.add_attribute_to_id('viterbi_delta_parent_node' , delta_parent_node)
			t.add_attribute_to_id('viterbi_maximum_indices' , maximum_indices)

### Top-down phase of Viterbi algorithm ###
def __assign_estimated_types_to_tree(t , initial_distribution):
	nb_labels = len(initial_distribution)

	if t.get_property('depth') == 0: # the node is the root
		delta_node = t.get_attribute('viterbi_delta_node')
		probs = []
		for lab in range(nb_labels):
			probs.append( delta_node[lab]*initial_distribution[lab] ) 
		est_typ = probs.index(max(probs))
		t.add_attribute_to_id('viterbi_probability',max(probs))

	else: # the node is not the root
		par_typ = t.my_parent.get_attribute('viterbi_type')
		est_typ = t.get_attribute('viterbi_maximum_indices')[par_typ]

	t.add_attribute_to_id('viterbi_type',est_typ)

	for child in t.my_children:
		__assign_estimated_types_to_tree(child , initial_distribution)

### Viterbi algorithm ###
def viterbi(t , obs_name , initial_distribution , transition_matrix , pdf_emission , emission_parameters):
	__assign_conditional_probabilities_to_tree(t , obs_name ,  transition_matrix , pdf_emission , emission_parameters)
	__assign_estimated_types_to_tree(t , initial_distribution)
	
# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.converters.nestedbrackets
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
Called by this module: treex.tree()

This module allows the conversion of strings with nested parentheses or brackets
to the treex format of trees

- File author(s): Romain Azais <romain.azais@inria.fr>

"""

from treex.tree import *

def __next_opening_par(chaine,par): # Private
    par_ouvr = par[0]
    par_ferm = par[1]

    out = 0
    for i in range(len(chaine)-1):
        if chaine[i+1]==par_ouvr and out == 0:
            out = i+1
    return out

def __closing_par(chaine,par): # Private
    par_ouvr = par[0]
    par_ferm = par[1]

    compteur=0
    out=0
    i=0
    while out==0:
        if chaine[i]==par_ouvr:
            compteur+=1
        elif chaine[i]==par_ferm and compteur!=1:
            compteur-=1
        elif chaine[i]==par_ferm and compteur==1:
            out=i
        i+=1
    return out

def __extract_subchain(chaine,par): # Private

    ipo = __next_opening_par(chaine,par)

    attribut = chaine[1:ipo]

    subchain=chaine[ipo:(len(chaine))]

    enfants=[]
    ouvr=0
    while len(subchain)>1:

        ferm=ouvr+__closing_par(subchain,par)

        subchain=subchain[ferm-ouvr+1:len(subchain)+1]

        enfants.append([ipo+ouvr,ipo+ferm])

        ouvr=ferm+1
    return attribut , enfants

def nestedbrackets_to_tree(parstr , flag_eval = True ,  par = '[]' , attribute_name = 'nestedbrackets_attribute'):
    """
    This method allows to convert a string of nested brackets into a Tree class object

    Parameters
    ----------
    parstr: str
        a string of nested brackets

    flag_eval: bool, optional
        if True, attributes are evaluated

    par: str
        string of len 2
        - first element is the opening bracket for new nodes
        - second element is the closing bracket for new nodes

    attribute_name: str
        name of the attribute to be added to the tree

    Returns
    -------
    Tree

    """

    if __next_opening_par(parstr,par)==0:
        t = Tree()

        attr = parstr[1:(len(parstr)-1)]
        if attr != '':
            if flag_eval:
                attr = eval(attr)
            t.add_attribute_to_id(attribute_name,attr)

        return t
    else:

        attr , subchains = __extract_subchain(parstr,par)
        t=Tree()
        
        if attr != '':
            if flag_eval:
                attr = eval(attr)
            t.add_attribute_to_id(attribute_name,attr)

        for i in subchains:
            ch = parstr[ (i[0]) : (i[1]+1) ]
            t.add_subtree(nestedbrackets_to_tree(ch , flag_eval , par , attribute_name ))
        return t

def tree_to_nestedbrackets(t , par = '[]' , attribute_name = 'nestedbrackets_attribute'):
    """
    This method allows to convert a tree into a string of nested brackets

    Parameters
    ----------
    t: Tree

    par: str
        string of len 2
        - first element is the opening bracket for new nodes
        - second element is the closing bracket for new nodes

    attribute_name: str
        name of the attribute in the tree to be added to the string of nested brackets

    Returns
    -------
    str

    See Also
    --------
    nestedbrackets_to_tree(...)

    """

    if attribute_name == None:
        attr = ''
    else:        
        if t.get_attribute(attribute_name)==None:
            attr=''
        else:
            attr = str(t.get_attribute(attribute_name))

    if t.get_property('size',compute_level=1)==1:
        strg = par[0] + attr + par[1]
    else:
        strg = par[0] + attr
        for child in t.my_children:
            strg += tree_to_nestedbrackets(child, par , attribute_name)
        strg += par[1]

    return strg
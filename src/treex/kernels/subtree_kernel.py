# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.kernels.subtree_kernel
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""

This module introduce a new class, SubtreeKernelData, in order to compute the subtree kernel from a dataset. The subtree kernel was first in Vishwanathan, S. V. N., & Smola, A. J. (2004). Fast kernels for string and tree matching. Kernel methods in computational biology, 15, 113-130.
The version computed here is based on our work : [AZA] Azaıs R, Ingels F. The Weight Function in the Subtree Kernel is Decisive. Journal of Machine Learning Research. 2020;21:1-36.

- File author(s): Florian Ingels <florian.ingels@inria.fr>
- File contributor(s): Romain Azais <romain.azais@inria.fr>
"""

import math
from itertools import combinations_with_replacement

from treex.kernels.support import *
import numpy as np
from treex.kernels.support import _points_of_interest, _dist_to_points


class SubtreeKernelData(object):
    """
    This class is used to compute the subtree kernel.

    Attributes
    ----------

    dag:
        a dag compressing a dataset

    classes:
        a dictionary, where keys are classes and to each class is associated the list of origins in the DAG that belong to that class. If all origins are not covered by a class, they will be referenced under the class `unknown`


    matching_subtrees: dict
        Creates a dictionary which is used to compute the subtree kernel.

        The :math:`(i,j)` entry is a list of tuple :math:`(n_i,n_j,t)`, where :

            :math:`t` is the number of the node in the dag, such as both :math:`i` and :math:`j` belongs
            to :math:`\\text{origins}(t)`

            :math:`n_i` and :math:`n_j` are respectively, the presence of :math:`t` in :math:`T_i` and :math:`T_j`

    """

    __slots__ = 'dag','classes', 'matching_subtrees'

    def __init__(self, dag, classes={}):
        self.dag = dag

        classes_origins = []
        for c in classes.keys():
            classes_origins += classes[c]

        unknown = list(set(dag.my_origins.keys()).difference(set(classes_origins)))
        self.classes = copy.deepcopy(classes)
        self.classes['unknown'] = unknown

        self.__matching_subtrees()

    def sample_origins(self, n):
        """
        This methods will split at random the `classes` attribute into :math:`n` classes repartition balanced dictionary on the same format of `classes`.
        :param n: int
            the number of class sample to obtain
        :return: a list of dictionaries
        """
        classes = copy.deepcopy(self.classes)
        del classes['unknown']
        samples = [{} for i in range(n)]
        i = 0
        for c in classes:
            while len(classes[c]) > 0:
                o = classes[c].pop(random.randrange(len(classes[c])))
                if c in samples[i].keys():
                    samples[i][c].append(o)
                else:
                    samples[i][c] = [o]
                i = (i + 1) % n

        if n==1:
            return samples[0]
        else:
            return tuple(samples)

    def __assign_vector_of_discriminance(self, classes):

        origins = {}
        for c in classes.keys():
            if c != 'unknown':
                for o in classes[c]:
                    origins[o] = c

        for node in self.dag.nodes():
            node_origin = self.dag.get_attribute(node, 'origin')

            vec = {}
            for c in classes.keys():
                if c != 'unknown':
                    vec[c] = 0

            for origin in node_origin:
                if origin in origins:
                    vec[origins[origin]] += 1 / len(classes[origins[origin]])
            self.dag.add_attribute_to_node('vector_of_discriminance', vec, node)

    def assign_discriminance(self, classes):
        """

        Add to each node in `dag` a new attribute, 'discriminance', equal to its
        distance of discrimination.

        Parameters
        ----------

        classes: dict
            the dictionary of classes - origins from which the discriminance will be computed

        Notes
        -----
        For each node, the discrimination of this node with regards to each
        different class, is the proportion of tree of this class with an origin in
        index_sample, that contain this node. Then, the distance of discrimination
        is the distance of this vector (of length number of different classes) to
        points of interest, which are given by (0,...,1,...,0) and (1,...,0,...,1).

        The 'discriminance' attribute is exactly equal to this distance of
        discrimination.

        For complete explanations, see [AZA]

        """
        self.__assign_vector_of_discriminance(classes)

        list_of_points = _points_of_interest(set(classes.keys()))

        for node in self.dag.nodes():
            vec = self.dag.get_attribute(node, 'vector_of_discriminance')
            val, pointmin = _dist_to_points(vec, list_of_points)

            if val > 1:
                val = 1

            val = 1 - val

            self.dag.add_attribute_to_node('discriminance', val, node)
            self.dag.add_attribute_to_node('discriminance_closest_point_of_interest', pointmin, node)

    def __matching_subtrees(self):
        origin_list = list(self.dag.my_origins.keys())

        m = {}

        for i, j in list(combinations_with_replacement(origin_list, 2)):
            i, j = tuple(sorted([i, j]))
            if i not in m.keys():
                m[i]={}
            m[i][j]=[]

        for node in self.dag.nodes():
            couples = list(combinations_with_replacement(self.dag.get_attribute(node, 'origin'), 2))
            for i, j in couples:
                index = sorted([i, j])
                i, j = tuple(index)
                m[i][j].append(node)

        self.matching_subtrees = m

    def __compute_single_kernel_value(self, i, j, **kwargs):
        if 'weight_param' in kwargs.keys():
            weight_param = kwargs['weight_param']
        else:
            weight_param = ()

        if 'kernel_param' in kwargs.keys():
            kernel_param = kwargs['kernel_param']
        else:
            kernel_param = ()

        if 'weight' in kwargs.keys():
            weight = kwargs['weight']
        else:
            if 'discriminance' in kwargs.keys():
                weight = discriminance_weight
            else:
                weight = exponential_weight

        if 'kernel' in kwargs.keys():
            kernel = kwargs['kernel']
        else:
            kernel = polynomial_kernel

        value = 0
        index = sorted([i, j])
        i, j = tuple(index)

        for node in self.matching_subtrees[i][j]:
            attributes = {**self.dag.get_attribute(node), **self.dag.get_property(node)} ###CAREFUL THE SECOND DICT ERASE THE KEYS IN COMMON WITH THE FIRST ONE
            ni = attributes['origin'][i]
            nj = attributes['origin'][j]
            value += weight(attributes, *weight_param) * kernel(ni, nj, *kernel_param)
        return value

    def compute_kernel(self, sample_1, sample_2, **kwargs):
        """
        This function returns a matrix :math:`K(T_i,T_j)` for all :math:`(i,j)` given.

        :param sample_1: dict
            dictionary of class-origins. Origins will play the role of :math:`i`
        :param sample_2: dict
            dictionary of class-origins. Origins will play the role of :math:`j`

        :param **kwargs:

            * discriminance : boolean
                ``True`` if the kernel should be computed using discriminance. Default is ``False``.

            * normalized : boolean
                ``True`` if the kernel that is computed should be normalized. Default is ``False``.

            * weight : func
                The weight function to use in the subtree kernel

            * weight_param :
                see weight functions in support module

            * kernel : func
                The inner kernel function to use in the subtree kernel.

            * kernel_param :
                see kernel functions in support module

        Notes
        -----
        Compute, for all :math:`(i,j)`: :math:`K(T_i,T_j)=\\sum_{\\nu\in \mathcal{M}_{i,j}} \omega_\\nu \kappa(N_\\nu(T_i)N_\\nu(T_j))`


        where :math:`\mathcal{M}_{i,j}` is the (i,j) entry of ``matching_subtrees``;
        :math:`\omega_\\nu` is the ``weight`` function evaluated in node :math:`\\nu`; :math:`\kappa` is the ``kernel`` function; :math:`N_\\nu(T)` is the number of times :math:`\\nu` appears as a subtree of :math:`T` (which is the ``presence`` attribute,  that appears in :math:`\mathcal{M}_{i,j}`)

        If the kernel is normalized, then :math:`K_\\text{norm.}(T_i,T_j)=\\frac{K(T_i,T_j)}{\sqrt{K(T_i,T_i)K(T_j,T_j)}}`

        See [AZA] for a complete explanation of this framework.
        """
        if not isinstance(sample_1, dict):
            sample_1 = {'fake': [sample_1]}
        if not isinstance(sample_2, dict):
            sample_2 = {'fake': [sample_2]}

        origins_1 = []
        for c in sample_1.keys():
            origins_1 += sample_1[c]

        origins_2 = []
        for c in sample_2.keys():
            origins_2 += sample_2[c]

        N = len(origins_1)
        M = len(origins_2)

        m = np.zeros((N, M))

        if 'normalized' in kwargs.keys():
            norm = kwargs['normalized']
        else:
            norm = False

        if norm:
            for i in range(N):
                for j in range(M):
                    a = self.__compute_single_kernel_value(origins_1[i], origins_2[j], **kwargs)
                    if a == 0:
                        m[i][j] = a
                    else:

                        b = self.__compute_single_kernel_value(origins_1[i], origins_1[i], **kwargs)
                        c = self.__compute_single_kernel_value(origins_2[j], origins_2[j], **kwargs)

                        if b * c == 0:
                            m[i][j] = float('Inf')
                        else:
                            m[i][j] = a / math.sqrt(b * c)

        else:
            for i in range(N):
                for j in range(M):
                    m[i][j] = self.__compute_single_kernel_value(origins_1[i], origins_2[j], **kwargs)

        return m


def assign_discriminance_nodescale(dag):
    """

    This function add an attribute in the dag, for visualization purposes. Each node is given a scale; when plotted, the nodes will be large if they discriminate well, and small otherwise.

    Parameters
    ----------
    dag : Dag
        Discriminance must have been computed before.


    See Also
    --------
    treex.visualization(...)


    """
    try:
        for node in dag.nodes():
            x = dag.get_attribute(node, 'discriminance')
            dag.add_attribute_to_node('nodescale_plotgraph', max(3 * x ** 2 - 2 * x ** 3, 0.0001), node)
        # dag.add_attribute_to_node('nodescale_plotgraph',max(2/3-x,0.01),node)
    except:
        print('Discriminance has not been calculated yet')


def assign_discriminance_color(dag):
    """
    This function gives to each node in the dag a color, according to the class it discriminates the best.

    Each class is assigned two colors, one for presence discrimination, one for absence discrimination.

    A node discriminates a class by its presence if it is more present in that
    class that in every other class. Conversely, a node discriminates a class
    by its absence if is more present in any other class but that class.

    This function returns a dictionary of the colors associated to each class.

    See Also
    ________
    treex.vizualisation(...)
    """

    dark_colors = ['blue', 'red', 'orange', 'olive', 'purple', 'darkgray']

    light_colors = ['cyan', 'magenta', 'yellow', 'green', 'violet', 'lightgray']

    try:

        color_dic = {}

        for node in dag.nodes():
            pointmin = dag.get_attribute(node, 'discriminance_closest_point_of_interest')

            if list(pointmin.values()).count(1) == len(pointmin) - 1:
                # absence point
                key = list(pointmin.keys())[list(pointmin.values()).index(0)]
                i = list(pointmin.keys()).index(key)
                color_dic[str(key) + ' : absence'] = dark_colors[i % len(dark_colors)]
                dag.add_attribute_to_node('color_plotgraph', dark_colors[i % len(dark_colors)], node)
            else:
                # presence point
                key = list(pointmin.keys())[list(pointmin.values()).index(1)]
                i = list(pointmin.keys()).index(key)
                color_dic[str(key) + ' : presence'] = light_colors[i % len(light_colors)]
                dag.add_attribute_to_node('color_plotgraph', light_colors[i % len(light_colors)], node)
        return color_dic
    except:
        print('Discriminance has not been calculated yet')


# --------------------------------------------------------------------------------
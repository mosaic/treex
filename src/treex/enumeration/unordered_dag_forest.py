# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.enumeration.unordered_dag_forest
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#           
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

""" 
Called by this module : treex.dag, random.choice

This module implements the algorithm proposed in [CITE] for exhaustive enumeration of DAGs compressing unordered tree
forests. If the core of the procedure comes from [CITE], the functions proposed here have been enriched with various
parameters and functionalities.

.. rubric:: References

.. [CITE] The article is in writing, references will be updated as soon as possible.

- File author(s): Florian Ingels <florian.ingels@inria.fr>

"""

from treex.lossless_compression.dag import *

from random import choice


# ------------------------------------------------------------------------------

def __widen(dag, index, child):
    dag.my_structure[index + 1] = {}
    dag.my_structure[index + 1]['children'] = child
    dag.my_structure[index + 1]['properties'] = {}

    dag.my_structure[index + 1]['properties']['size'] = 1 + sum(
        [dag.my_structure[a]['properties']['size'] for a in child])
    dag.my_structure[index + 1]['properties']['number_of_children'] = len(child)
    dag.my_structure[index + 1]['properties']['outdegree'] = max(len(child), max(
        [dag.my_structure[a]['properties']['outdegree'] for a in child]))
    dag.my_structure[index + 1]['properties']['height'] = dag.my_structure[index]['properties']['height']


def __branch(dag, index, a):
    dag.my_structure[index]['children'] += [a]
    dag.my_structure[index]['properties']['size'] += dag.my_structure[a]['properties']['size']
    dag.my_structure[index]['properties']['number_of_children'] += 1
    dag.my_structure[index]['properties']['outdegree'] = max(dag.my_structure[index]['properties']['outdegree'],
                                                             dag.my_structure[a]['properties']['outdegree'],
                                                             dag.my_structure[index]['properties'][
                                                                 'number_of_children'])


def __elongate(dag, index, a):
    dag.my_structure[index + 1] = {}
    dag.my_structure[index + 1]['children'] = [a]
    dag.my_structure[index + 1]['properties'] = {}

    dag.my_structure[index + 1]['properties']['size'] = 1 + dag.my_structure[a]['properties']['size']
    dag.my_structure[index + 1]['properties']['number_of_children'] = 1
    dag.my_structure[index + 1]['properties']['outdegree'] = max(1, dag.my_structure[a]['properties']['outdegree'])
    dag.my_structure[index + 1]['properties']['height'] = 1 + dag.my_structure[a]['properties']['height']


# ------------------------------------------------------------------------------

def __random_widening(word, pivot, **constraints):
    options = list(range(0, len(word) + 1))

    if 'outdegree' in constraints.keys():
        if len(word) == constraints['outdegree']:
            options = list(range(0, len(word)))

    while len(options) > 0:

        u = choice(options)
        options.remove(u)

        if u == 0:
            if word[0] != pivot:
                return [choice(range(word[0] + 1, pivot + 1))]
        elif u == len(word):
            return word + [choice(range(0, word[u - 1] + 1))]
        else:
            if word[u] != word[u - 1]:
                return word[0:u] + [choice(range(word[u] + 1, word[u - 1] + 1))]
    raise ValueError("Not able to find any new word")


def __random_constraints(dag, pivot, index, **constraints):
    operations = {"elongation", "widening", "branching"}

    height = dag.get_property(index, 'height')
    outdegree = dag.get_property(index, 'number_of_children')

    if 'nodes' in constraints.keys():
        if index == constraints['nodes'] - 1:
            operations.discard("elongation")
            operations.discard("widening")

    if 'height' in constraints.keys():
        if height == constraints['height']:
            operations.discard("elongation")

    if 'outdegree' in constraints.keys():
        if outdegree == constraints['outdegree']:
            operations.discard("branching")
            child = dag.children(index)
            if set(child) == {pivot}:
                operations.discard("widening")
    return list(operations)


def __random_successor(dag, pivot, index, **constraints):
    options = __random_constraints(dag, pivot, index, **constraints)

    if len(options) == 0:
        raise ValueError("Not able to find any successor due to too restrictive constraints")

    u = choice(options)

    if u == "branching":

        a = choice(range(0, dag.children(index)[-1] + 1))

        __branch(dag, index, a)

    elif u == "elongation":

        a = pivot + 1 + choice(range(0, index - pivot))

        __elongate(dag, index, a)

        pivot = index
        index += 1

    else:

        child = __random_widening(dag.children(index), pivot, **constraints)

        __widen(dag, index, child)

        index += 1

    return pivot, index


def __assign_fake_origins(dag):
    dag.my_origins = {}

    i = 0
    for root in dag.my_roots:
        dag.my_origins['root_' + str(i)] = root
        i += 1

    dag._Dag__propagate_origins()


def dag_random_walk(nb=1, dag=None, **constraints):
    """
    Perform a random trajectory on the enumeration tree defined in [CITE]. Starting from a given Dag,
    we choose uniformly at random a successor, and repeat this operation a fixed number of time. The starting Dag is
    modified at each step, and returned at the end.


    Parameters
    ----------
    nb: int,optional
        The number of steps to perform. Default is 1.

    dag : Dag, optional
        If a Dag is provided, it will be the starting point of the walk. Note that this Dag will be
        modified during the walk. If no Dag is given, default starting point will be the two-vertices Dag with a single
        edge.

    **constraints : optional
        Same as in dag_successors(...)

    :rtype: Dag


    Warnings
    --------
    If the constraints are too restrictive, it is very likely that the random walk won't reach its
    end. In this case, an error is raised. Such a case can occur if the constraints contain both `outdegree` and
    `nodes`, or both `outdegree` and `height`. Note that `nodes` and `height` combine well.

    """

    if dag is None:
        t = Tree()
        t.add_subtree(Tree())
        dag = tree_to_dag(t)
        dag.erase_attribute('origin')
        dag.get_attribute()
        index = 1
        pivot = 0
    else:
        dag.renumber_nodes('canonical')
        index = max(dag.nodes())
        nodes_sorted = dag.nodes_sorted_by('height')
        pivot = max([node for node in nodes_sorted[max(nodes_sorted.keys()) - 1]])

    for i in range(nb):
        pivot, index = __random_successor(dag, pivot, index, **constraints)

    dag.assign_roots()
    __assign_fake_origins(dag)

    return dag


# ------------------------------------------------------------------------------

def __branching(dag, index, pivot):
    l = []

    for a in range(0, dag.children(index)[-1] + 1):
        dp = dag.copy()

        __branch(dp, index, a)

        l.append((dp, index, pivot))

    return l


def __elongation(dag, index, pivot):
    l = []

    for i in range(0, index - pivot):
        a = pivot + 1 + i

        dp = dag.copy()

        __elongate(dp, index, a)

        l.append((dp, index + 1, index))

    return l


def __widening_words(word, pivot):
    l = []

    u = 0
    if word[0] != pivot:
        l += [[i] for i in range(word[0] + 1, pivot + 1)]
    u += 1
    while u < len(word):
        if word[u] != word[u - 1]:
            l += [word[0:u] + [i] for i in range(word[u] + 1, word[u - 1] + 1)]
        u += 1
    l += [word + [i] for i in range(0, word[u - 1] + 1)]

    return l


def __widening(dag, index, pivot):
    l = []

    for child in __widening_words(dag.children(index), pivot):
        dp = dag.copy()

        __widen(dp, index, child)

        l.append((dp, index + 1, pivot))

    return l


def __check_constraints(dag, index, **constraints):
    height = dag.get_property(index, 'height')
    outdegree = dag.get_property(index, 'outdegree')
    # if 'size' in constraints.keys():
    #	if dic['size']>constraints['size']:
    # 		return False
    if 'nodes' in constraints.keys():
        if index >= constraints['nodes']:
            return False
    if 'height' in constraints.keys():
        if height > constraints['height']:
            return False
    if 'outdegree' in constraints.keys():
        if outdegree > constraints['outdegree']:
            return False
    return True


def dag_successors(dag=None, nb=float('Inf'), **constraints):
    """
    Returns the list of all DAGs compressing an unordered tree forest that:

        * are a successor in at most `nb` steps of the starting Dag
        * satisfy the constraints demanded (if given).

    Parameters
    ----------

    dag: Dag, optional
        If a Dag is given, it will be the starting point of the succession (it
        won't be modified). If no Dag is given, default starting point will be the two-vertices Dag with a single edge.

    nb : int, optional
        If given, successors will be investigated up to `nb` steps from the starting Dag. Default is ``inf``.

    **constraints: optional
        Dags whose attributes exceeding at least one constraint given will be rejected, and their successors won't be investigated.

        * nodes: int, the number of vertices

        * height: int

        * outdegree: int


    Returns
    -------
    A list of Dag satisfying the constraints within at most `nb` steps from the starting tree. The starting Dag is the first item of the list.


    Warnings
    --------
    As the enumeration tree is infinite, this function will behave like an endless loop if none of those conditions is fulfilled:

    * a finite parameter `nb` is passed

    * the constraints `nodes` and `outdegree` are present

    * the constraints  `height` and `outdegree` are present

    """
    if dag is None:
        t = Tree()
        t.add_subtree(Tree())
        dag = tree_to_dag(t)
        dag.erase_attribute('origin')
        dag.get_property()
        index = 1
        pivot = 0
    else:
        dag.renumber_nodes('canonical')
        index = max(dag.nodes())
        nodes_sorted = dag.nodes_sorted_by('height')
        pivot = max([node for node in nodes_sorted[max(nodes_sorted.keys()) - 1]])

    to_explore_now = [(dag, index, pivot)]
    to_explore_next = []
    to_return = []

    i = 0
    while i < nb + 1:
        i += 1
        while len(to_explore_now) > 0:
            dag, index, pivot = to_explore_now.pop()
            if __check_constraints(dag, index, **constraints):
                dag.assign_roots()
                __assign_fake_origins(dag)
                to_return.append(dag)
                to_explore_next += __branching(dag, index, pivot)
                to_explore_next += __elongation(dag, index, pivot)
                to_explore_next += __widening(dag, index, pivot)

        if len(to_explore_next) == 0:
            break
        to_explore_now = to_explore_next
        to_explore_next = []

    return to_return

# ------------------------------------------------------------------------------

def dag_predecessor(dag):
    """
    This function implements the reduction rule of [CITE].
    :param dag: Dag
    :return: The parent of `dag` in the enumeration tree defined in [CITE].
    """
    if len(dag.nodes())==1:
        return None
    else:
        d = dag.copy()
        n=dag.nodes()[-1]
        w = dag.children(n)
        wp = dag.children(n-1)
        nodes_sorted=dag.nodes_sorted_by('height')
        if len(nodes_sorted[dag.get_property(n,'height')])==1:
            if len(w)==1:
                del d.my_structure[n]
            else:
                d.my_structure[n]['children']=w[:-1]
                d.my_structure[n]['attributes']={}
                d.get_property(node=n)
        else:
            if w>wp and w[:-1]<=wp:
                del d.my_structure[n]
            else:
                d.my_structure[n]['children']=w[:-1]
                d.my_structure[n]['attributes']={}
                d.get_property(node=n)
        return d
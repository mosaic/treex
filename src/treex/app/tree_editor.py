import os
import argparse
import json
import logging
import time

from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog, QFileDialog
from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QComboBox, QCheckBox
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QGridLayout, QFormLayout
from PyQt5.QtGui import QFont
from PyQt5.Qt import Qt, QSizePolicy, QDoubleValidator, QIntValidator, QLocale

import numpy as np
import matplotlib.pyplot as plt

from treex.tree import Tree
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.utils.save import save_object, read_object, save_tree_properties_and_attributes
from treex.visualization.matplotlib_plot import view_tree
from treex.visualization.texplot import __coord_treegraph as coord_treegraph
from treex.visualization.texplot import tree_to_tex
from treex.visualization.options import assign_random_color_to_tree, assign_random_nodescale_to_tree, assign_random_linewidth_to_tree

from treex.app.options import assign_category_variable_color_to_tree, assign_scalar_variable_nodescale_to_tree, assign_scalar_variable_linewidth_to_tree
from treex.app.options import all_node_variable_names, _scalar_variable_names, _category_variable_names
from treex.app.node_attribute_editor import NodeAttributeEditor
from treex.app.matplotlib_tools import all_widgets


class TreeEditor(QWidget):

    def __init__(self, tree, parent=None):
        super().__init__(parent)

        self.tree = tree
        self.node_id = None

        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.direction = 'down'
        self.nodescale = 100.
        self.linewidth = 1.
        self.highlight_color = 'k'

        self.variable_type = 'attribute'
        self.nodescale_attribute = ""
        self.color_attribute = ""
        self.linewidth_attribute = ""

        self.direction_edit = QComboBox()
        self.nodescale_edit = QLineEdit(str(self.nodescale))
        self.linewidth_edit = QLineEdit(str(self.linewidth))

        self.nodescale_attribute_edit = QComboBox()
        self.color_attribute_edit = QComboBox()
        self.linewidth_attribute_edit = QComboBox()

        self.visualization_layout = QVBoxLayout()
        self.visualization_pane = QWidget(self)
        
        self.visualization_parameters_label = QLabel("Visualization Parameters")
        self.visualization_parameters_layout = QFormLayout()
        self.visualization_parameters_pane = QWidget()

        self.global_attribute_values = {}
        self.global_widgets = {}
        self.global_types = {}
        self.node_global_attributes = {}

        self.global_label = QLabel("Global Attribute Values")
        self.global_layout = QFormLayout()
        self.global_pane = QWidget()
        self.new_global_button = QPushButton('New Global Attribute')
        
        self.layout.addWidget(self.visualization_pane)

        self.initialize_visualization_pane()

        self.figure = plt.figure(0)
        self.figure_widget = all_widgets[0]
        self.figure_widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.layout.addWidget(self.figure_widget)

        self.hidden_attributes = []
        self.hidden_attributes_for_plots = []
        self.attribute_widgets = {}
        self.attribute_types = {}
        self.pick_connect = None
        self.unselect_connect = None
        self.select = False

        self.load_button = QPushButton("Load Tree")
        self.save_button = QPushButton("Save Tree")
        self.export_button = QPushButton("Export Tree Data")
        self.latex_button = QPushButton("Export to LaTeX")
        self.screenshot_button = QPushButton("Save Screenshot")
        self.new_attribute_button = QPushButton('New Attribute')
        self.add_button = QPushButton("Add Child")
        self.add_select_parent = QCheckBox("Select Parent Node After Adding Child")
        self.remove_button = QPushButton("Remove Subtree")

        self.node_property_label = QLabel("Current Node Properties")
        self.property_layout = QFormLayout()
        self.property_pane = QWidget()
        
        self.node_attribute_label = QLabel("Current Node Attributes")
        self.attribute_layout = QFormLayout()
        self.attribute_pane = QWidget()

        self.edition_layout = QVBoxLayout()
        self.edition_pane = QWidget(self)
        self.layout.addWidget(self.edition_pane)

        self.initialize_edition_pane()

        self.refresh_variables()
        self.refresh_globals()
        self.refresh_plot()

    def initialize_visualization_pane(self):
        self.visualization_layout.setContentsMargins(0, 10, 0, 0)
        self.visualization_layout.setAlignment(Qt.AlignCenter)

        self.visualization_pane.setFixedWidth(300)
        self.visualization_pane.setLayout(self.visualization_layout)

        font = QFont()
        self.visualization_parameters_label.setFont(QFont(font.defaultFamily(), 16, QFont.Bold))
        self.visualization_parameters_label.setAlignment(Qt.AlignCenter)
        self.visualization_layout.addWidget(self.visualization_parameters_label)

        self.visualization_parameters_layout.setContentsMargins(0, 0, 0, 0)

        self.visualization_parameters_pane.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.visualization_parameters_pane.setLayout(self.visualization_parameters_layout)
        self.visualization_layout.addWidget(self.visualization_parameters_pane)

        self.direction_edit.clear()
        self.direction_edit.addItems(['down', 'up', 'left', 'right'])
        self.direction_edit.setCurrentText(self.direction)
        self.direction_edit.currentIndexChanged.connect(self.refresh_plot)
        self.visualization_parameters_layout.addRow("Direction", self.direction_edit)

        self.nodescale_edit.setValidator(QDoubleValidator())
        self.nodescale_edit.editingFinished.connect(self.refresh_plot)
        self.visualization_parameters_layout.addRow("Scale factor", self.nodescale_edit)

        self.linewidth_edit.setValidator(QDoubleValidator())
        self.linewidth_edit.editingFinished.connect(self.refresh_plot)
        self.visualization_parameters_layout.addRow("Width factor", self.linewidth_edit)

        self.nodescale_attribute_edit.setFixedWidth(150)
        self.nodescale_attribute_edit.clear()
        self.nodescale_attribute_edit.addItems([""])
        self.nodescale_attribute_edit.setCurrentText(self.nodescale_attribute)
        self.nodescale_attribute_edit.currentIndexChanged.connect(self.refresh_plot)
        self.visualization_parameters_layout.addRow("Node Scale Attribute", self.nodescale_attribute_edit)

        self.color_attribute_edit.setFixedWidth(150)
        self.color_attribute_edit.clear()
        self.color_attribute_edit.addItems([""])
        self.color_attribute_edit.setCurrentText(self.color_attribute)
        self.color_attribute_edit.currentIndexChanged.connect(self.refresh_plot)
        self.visualization_parameters_layout.addRow("Node Color Attribute", self.color_attribute_edit)

        self.linewidth_attribute_edit.setFixedWidth(150)
        self.linewidth_attribute_edit.clear()
        self.linewidth_attribute_edit.addItems([""])
        self.linewidth_attribute_edit.setCurrentText(self.linewidth_attribute)
        self.linewidth_attribute_edit.currentIndexChanged.connect(self.refresh_plot)
        self.visualization_parameters_layout.addRow("Line Width Attribute", self.linewidth_attribute_edit)

        font = QFont()
        self.global_label.setFont(QFont(font.defaultFamily(), 16, QFont.Bold))
        self.global_label.setAlignment(Qt.AlignCenter)
        self.global_label.setVisible(False)
        self.visualization_layout.addWidget(self.global_label)

        self.global_layout.setContentsMargins(0, 0, 0, 0)

        self.global_pane.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.global_pane.setLayout(self.global_layout)
        self.visualization_layout.addWidget(self.global_pane)

        self.new_global_button.clicked.connect(self.add_new_global)

    def initialize_edition_pane(self):
        self.edition_layout.setContentsMargins(0, 0, 0, 0)

        self.edition_pane.setFixedWidth(300)
        self.edition_pane.setLayout(self.edition_layout)

        self.load_button.setEnabled(True)
        self.load_button.clicked.connect(self.load_tree)
        self.edition_layout.addWidget(self.load_button)

        self.save_button.setEnabled(True)
        self.save_button.clicked.connect(self.save_tree)
        self.edition_layout.addWidget(self.save_button)

        self.export_button.setEnabled(True)
        self.export_button.clicked.connect(self.export_tree_data)
        self.edition_layout.addWidget(self.export_button)

        self.latex_button.setEnabled(True)
        self.latex_button.clicked.connect(self.export_to_latex)
        self.edition_layout.addWidget(self.latex_button)

        self.screenshot_button.setEnabled(True)
        self.screenshot_button.clicked.connect(self.save_screenshot)
        self.edition_layout.addWidget(self.screenshot_button)

        font = QFont()
        self.node_property_label.setFont(QFont(font.defaultFamily(), 16, QFont.Bold))
        self.node_property_label.setAlignment(Qt.AlignCenter)
        self.node_property_label.setVisible(False)
        self.edition_layout.addWidget(self.node_property_label)

        self.property_layout.setContentsMargins(0, 0, 0, 0)

        self.property_pane.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.property_pane.setLayout(self.property_layout)
        self.edition_layout.addWidget(self.property_pane)

        font = QFont()
        self.node_attribute_label.setFont(QFont(font.defaultFamily(), 16, QFont.Bold))
        self.node_attribute_label.setAlignment(Qt.AlignCenter)
        self.node_attribute_label.setVisible(False)
        self.edition_layout.addWidget(self.node_attribute_label)

        self.attribute_layout.setContentsMargins(0, 0, 0, 0)

        self.attribute_pane.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.attribute_pane.setLayout(self.attribute_layout)
        self.edition_layout.addWidget(self.attribute_pane)

        self.new_attribute_button.clicked.connect(self.add_new_attribute)

        self.add_button.setEnabled(False)
        self.add_button.clicked.connect(self.add_child)
        self.edition_layout.addWidget(self.add_button)

        self.add_select_parent.setChecked(False)
        self.edition_layout.addWidget(self.add_select_parent)

        self.remove_button.setEnabled(False)
        self.remove_button.clicked.connect(self.remove_subtree)
        self.edition_layout.addWidget(self.remove_button)

    def refresh_variables(self):
        tree_category_attributes = all_node_variable_names(self.tree, variable_type=self.variable_type, type='category')
        tree_category_attributes = [v for v in tree_category_attributes if v not in self.hidden_attributes_for_plots]

        tree_scalar_attributes = all_node_variable_names(self.tree, variable_type=self.variable_type, type='scalar')
        tree_scalar_attributes = [v for v in tree_scalar_attributes if v not in self.hidden_attributes_for_plots]

        self.nodescale_attribute_edit.blockSignals(True)
        self.nodescale_attribute_edit.clear()
        self.nodescale_attribute_edit.addItems([""]+tree_scalar_attributes)
        self.nodescale_attribute_edit.blockSignals(False)
        if self.nodescale_attribute in [""]+tree_scalar_attributes:
            self.nodescale_attribute_edit.setCurrentText(self.nodescale_attribute)
        else:
            self.nodescale_attribute_edit.setCurrentText("")
        self.nodescale_attribute_edit.setEnabled(len(tree_scalar_attributes) > 0)

        self.color_attribute_edit.blockSignals(True)
        self.color_attribute_edit.clear()
        self.color_attribute_edit.addItems([""]+tree_category_attributes)
        self.color_attribute_edit.blockSignals(False)
        if self.color_attribute in [""]+tree_category_attributes:
            self.color_attribute_edit.setCurrentText(self.color_attribute)
        else:
            self.color_attribute_edit.setCurrentText("")
        self.color_attribute_edit.setEnabled(len(tree_category_attributes) > 0)

        self.linewidth_attribute_edit.blockSignals(True)
        self.linewidth_attribute_edit.clear()
        self.linewidth_attribute_edit.addItems([""]+tree_scalar_attributes)
        self.linewidth_attribute_edit.blockSignals(False)
        if self.linewidth_attribute in [""]+tree_scalar_attributes:
            self.linewidth_attribute_edit.setCurrentText(self.linewidth_attribute)
        else:
            self.linewidth_attribute_edit.setCurrentText("")
        self.linewidth_attribute_edit.setEnabled(len(tree_scalar_attributes) > 0)

    def clear_globals(self):
        for r in reversed(range(self.global_layout.count())):
            widget = self.global_layout.itemAt(r).widget()
            if widget != self.new_global_button:
                widget.deleteLater()
        self.global_widgets = {}
        self.global_types = {}

    def refresh_globals(self):
        self.clear_globals()

        for name, value in self.global_attribute_values.items():
            if isinstance(value, int):
                self.global_types[name] = 'int'
            elif isinstance(value, float):
                self.global_types[name] = 'float'
            else:
                self.global_types[name] = 'str'

        for name, value in self.global_attribute_values.items():
            self.global_widgets[name] = QLineEdit(str(value))
            if self.global_types[name] == 'int':
                self.global_widgets[name].setValidator(QIntValidator())
            elif self.global_types[name] == 'float':
                self.global_widgets[name].setValidator(QDoubleValidator())
            self.global_widgets[name].editingFinished.connect(self.edit_global_value)

        for name, value in self.global_attribute_values.items():
            if name in self.global_widgets:
                self.global_layout.addRow(name, self.global_widgets[name])
        self.global_layout.addRow("",self.new_global_button)
        self.global_layout.setFieldGrowthPolicy(QFormLayout.AllNonFixedFieldsGrow)
        self.global_layout.setFormAlignment(Qt.AlignHCenter | Qt.AlignTop)

        self.global_label.setVisible(True)

    def edit_global_value(self):
        names = [n for n in self.global_widgets.keys() if self.global_widgets[n]==self.sender()]
        if len(names)>0:
            name = names[0]
            text = self.sender().text()
            if self.global_types[name] == 'int':
                global_value = int(text)
            elif self.global_types[name] == 'float':
                global_value = float(text)
            elif self.global_types[name] == 'str':
                global_value = text
            self.global_attribute_values[name] = global_value

            for node_id in self.node_global_attributes.keys():
                for attribute_name in self.node_global_attributes[node_id].keys():
                    if self.node_global_attributes[node_id][attribute_name] == name:
                        self.tree.add_attribute_to_id(attribute_name, self.global_attribute_values[name], node_id=node_id)

            self.refresh_plot()
            self.display_attributes()

    def refresh_plot(self):
        if self.pick_connect is not None:
            self.figure.canvas.mpl_disconnect(self.pick_connect)
        if self.unselect_connect is not None:
            self.figure.canvas.mpl_disconnect(self.unselect_connect)
        self.figure.clf()

        self.unselect_connect = self.figure.canvas.mpl_connect('button_release_event', self.unselect)

        self.direction = self.direction_edit.currentText()
        self.nodescale = float(self.nodescale_edit.text())
        self.linewidth = float(self.linewidth_edit.text())

        self.nodescale_attribute = self.nodescale_attribute_edit.currentText()
        if self.nodescale_attribute == "":
            assign_random_nodescale_to_tree(self.tree,[1])
        else:
            assign_scalar_variable_nodescale_to_tree(self.tree, self.nodescale_attribute, variable_type=self.variable_type)

        self.color_attribute = self.color_attribute_edit.currentText()
        if self.color_attribute == "":
            assign_random_color_to_tree(self.tree, ['grey'])
        else:
            assign_category_variable_color_to_tree(self.tree, self.color_attribute, variable_type=self.variable_type)
            node_color_attributes = [t.get_attribute(self.color_attribute) for t in self.tree.list_of_subtrees()]
            node_colors = [t.get_attribute('color_plotgraph') for t in self.tree.list_of_subtrees()]
            color_dict = {v:c for v,c in zip(node_color_attributes,node_colors)}
            color_attribute_values = np.sort(list(color_dict.keys()))
            color_points = []
            for c in color_attribute_values:
                color_points += [self.figure.gca().scatter([0],[0],color=color_dict[c],ec='k',s=self.nodescale)]
            self.figure.clf()
            self.figure.gca().legend(color_points,color_attribute_values,loc=2,title=self.color_attribute)

        self.linewidth_attribute = self.linewidth_attribute_edit.currentText()
        if self.linewidth_attribute == "":
            assign_random_linewidth_to_tree(self.tree,[1])
        else:
            assign_scalar_variable_linewidth_to_tree(self.tree, self.linewidth_attribute, variable_type=self.variable_type)

        view_tree(self.tree, self.figure, node_scale=self.nodescale, direction=self.direction, linewidth=self.linewidth)
        self.pick_connect = self.figure.canvas.mpl_connect('pick_event', self.pick_tree_node)

        graph_coords = coord_treegraph(self.tree, self.direction)

        if self.node_id is not None:
            focus_point = graph_coords[self.node_id][:2]
            self.figure.gca().scatter(focus_point[0],focus_point[1],s=2.*self.nodescale*graph_coords[self.node_id][4],ec='k',fc=self.highlight_color,linewidth=0,alpha=0.33,zorder=8)

        self.figure.tight_layout()

    def load_tree(self):
        file_dialog = QFileDialog(self,"Load tree",".","Pickled treex.Tree objects (*.*)")
        file_dialog.setAcceptMode(QFileDialog.AcceptOpen)
        # file_dialog.setWindowFlags(Qt.Sheet)
        # file_dialog.setWindowModality(Qt.WindowModal)
        accept = file_dialog.exec_()
        if accept:
            filename = file_dialog.selectedFiles()[0]
            try:
                data = read_object(filename, return_mapping=True)
                t = data['object']
                assert isinstance(t,Tree)
                mapping = dict(zip(data['mapping'].values(),data['mapping'].keys()))
            except:
                logging.error("Could not load tree from file "+filename+"!")
            else:
                self.tree = t
                self.node_id = None
                self.clear_attributes()

                json_filename = os.path.splitext(filename)[0] + ".json"
                if os.path.exists(json_filename):
                    global_data = json.load(open(json_filename,'r'))
                    self.global_attribute_values = global_data['global_attribute_values']
                    self.node_global_attributes = {mapping[int(i)]:v for i,v in global_data['node_global_attributes'].items()}
                    self.refresh_globals()
                self.refresh_variables()
                self.refresh_plot()

    def save_tree(self):
        file_dialog = QFileDialog(self,"Save tree",".","Pickled treex.Tree objects (*.*)")
        file_dialog.setAcceptMode(QFileDialog.AcceptSave)
        # file_dialog.setWindowFlags(Qt.Sheet)
        # file_dialog.setWindowModality(Qt.WindowModal)
        accept = file_dialog.exec_()
        if accept:
            filename = file_dialog.selectedFiles()[0]
            save_object(self.tree, filename)

            if len(self.global_attribute_values)>0:
                json_filename = os.path.splitext(filename)[0] + ".json"
                global_data = {}
                global_data['global_attribute_values'] = self.global_attribute_values
                global_data['node_global_attributes'] = self.node_global_attributes
                json.dump(global_data, open(json_filename,'w'), sort_keys=True, indent=4)

    def export_tree_data(self):
        file_dialog = QFileDialog(self,"Export Tree Data",".","Text files (*.txt)")
        file_dialog.setAcceptMode(QFileDialog.AcceptSave)
        # file_dialog.setWindowFlags(Qt.Sheet)
        # file_dialog.setWindowModality(Qt.WindowModal)
        accept = file_dialog.exec_()
        if accept:
            filename = file_dialog.selectedFiles()[0]
            save_tree_properties_and_attributes(self.tree,filename,excluded_attributes=self.hidden_attributes)

    def export_to_latex(self):
        file_dialog = QFileDialog(self,"Export to LaTeX",".","TeX source files (*.tex)")
        file_dialog.setAcceptMode(QFileDialog.AcceptSave)
        # file_dialog.setWindowFlags(Qt.Sheet)
        # file_dialog.setWindowModality(Qt.WindowModal)
        accept = file_dialog.exec_()
        if accept:
            filename = file_dialog.selectedFiles()[0]
            tree_to_tex(self.tree,filename,nodescale=self.nodescale,direction=self.direction)

    def save_screenshot(self):
        file_dialog = QFileDialog(self,"Save screenshot",".","Figure files (*.png *.eps *.pdf *.svg)")
        file_dialog.setAcceptMode(QFileDialog.AcceptSave)
        # file_dialog.setWindowFlags(Qt.Sheet)
        # file_dialog.setWindowModality(Qt.WindowModal)
        accept = file_dialog.exec_()
        if accept:
            filename = file_dialog.selectedFiles()[0]
            size = self.figure.get_size_inches()
            self.figure.set_size_inches(10,10)
            self.figure.savefig(filename)
            self.figure.set_size_inches(size)

    def clear_attributes(self):
        for r in reversed(range(self.property_layout.count())):
            self.property_layout.itemAt(r).widget().deleteLater()
        self.property_widgets = {}
        self.property_types = {}
        self.node_property_label.setVisible(False)
        for r in reversed(range(self.attribute_layout.count())):
            widget = self.attribute_layout.itemAt(r).widget()
            if widget != self.new_attribute_button:
                widget.deleteLater()
            else:
                widget.setVisible(False)
        self.attribute_widgets = {}
        self.attribute_types = {}
        self.node_attribute_label.setVisible(False)
        self.add_button.setEnabled(False)
        self.remove_button.setEnabled(False)

    def unselect(self, event):
        if not self.select:
            self.node_id = None
            self.clear_attributes()
            self.refresh_plot()
        self.select = False

    def pick_tree_node(self, event):
        self.node_id = self.tree.list_of_ids()[event.ind[0]]
        self.display_attributes()
        self.refresh_plot()
        self.select = True

    def display_attributes(self):
        t = self.tree.subtree(self.node_id)

        self.clear_attributes()

        for name, value in t.get_property().items():
            valid_properties = _scalar_variable_names(t,variable_type='property',all_nodes=False)
            valid_properties += _category_variable_names(t,variable_type='property',all_nodes=False)
            if name in valid_properties:
                self.property_layout.addRow(name, QLabel(str(value)))
        self.property_layout.setFieldGrowthPolicy(QFormLayout.AllNonFixedFieldsGrow)
        self.property_layout.setFormAlignment(Qt.AlignHCenter | Qt.AlignTop)

        self.node_property_label.setVisible(True)

        for name, value in t.get_attribute().items():
            if isinstance(value, bool):
                self.attribute_types[name] = 'bool'
            elif isinstance(value, int):
                self.attribute_types[name] = 'int'
            elif isinstance(value, float):
                self.attribute_types[name] = 'float'
            else:
                self.attribute_types[name] = 'str'

        for name, value in t.get_attribute().items():
            valid_attributes = _scalar_variable_names(t,variable_type='attribute',all_nodes=False)
            valid_attributes += _category_variable_names(t,variable_type='attribute',all_nodes=False)
            if not name in self.hidden_attributes and name in valid_attributes:
                self.attribute_widgets[name] = NodeAttributeEditor(value,self.attribute_types[name])
                same_type_globals = dict([(n,v) for n,v in self.global_attribute_values.items() if self.global_types[n] == self.attribute_types[name]])
                self.attribute_widgets[name].set_global_attributes(same_type_globals)
                if self.node_id in self.node_global_attributes.keys():
                    if name in self.node_global_attributes[self.node_id]:
                        self.attribute_widgets[name].set_global_value(self.node_global_attributes[self.node_id][name])
                # self.attribute_widgets[name] = QLineEdit(str(value))
                # if self.attribute_types[name] == 'int':
                #     self.attribute_widgets[name].setValidator(QIntValidator())
                # elif self.attribute_types[name] == 'float':
                #     self.attribute_widgets[name].setValidator(QDoubleValidator())
                self.attribute_widgets[name].editingFinished.connect(self.edit_attribute)


        for name, value in t.get_attribute().items():
            if name in self.attribute_widgets:
                self.attribute_layout.addRow(name, self.attribute_widgets[name])
        self.attribute_layout.addRow("",self.new_attribute_button)
        self.new_attribute_button.setVisible(True)
        self.attribute_layout.setFieldGrowthPolicy(QFormLayout.AllNonFixedFieldsGrow)
        self.attribute_layout.setFormAlignment(Qt.AlignHCenter | Qt.AlignTop)

        self.node_attribute_label.setVisible(True)

        self.add_button.setEnabled(True)
        if t.my_parent is None:
            self.remove_button.setEnabled(False)
        else:
            self.remove_button.setEnabled(True)

    def edit_attribute(self):
        names = [n for n in self.attribute_widgets.keys() if self.attribute_widgets[n]==self.sender()]
        if len(names)>0:
            name = names[0]
            attribute_value = self.sender().value()
            self.tree.add_attribute_to_id(name, attribute_value, node_id=self.node_id)

            if self.sender().has_global_value:
                if self.node_id not in self.node_global_attributes.keys():
                    self.node_global_attributes[self.node_id] = {}
                self.node_global_attributes[self.node_id][name] = self.sender().global_name
            else:
                if self.node_id in self.node_global_attributes.keys():
                    if name in self.node_global_attributes[self.node_id].keys():
                        del self.node_global_attributes[self.node_id][name]

            self.refresh_plot()
            self.display_attributes()

    def add_new_attribute(self):
        if self.node_id is not None:
            t = self.tree.subtree(self.node_id)

            dialog = QDialog(self)
            dialog.setWindowModality(Qt.WindowModal)

            new_attribute_layout = QGridLayout()
            new_attribute_layout.setContentsMargins(10, 10, 10, 10)

            name_label = QLabel("New Attribute Name")
            new_attribute_layout.addWidget(name_label, 0, 0, 1, 1)

            name_edit = QLineEdit()
            new_attribute_layout.addWidget(name_edit, 0, 1, 1, 1)

            value_label = QLabel("New Attribute Value")
            new_attribute_layout.addWidget(value_label, 1, 0, 1, 1)

            value_edit = QLineEdit()
            new_attribute_layout.addWidget(value_edit, 1, 1, 1, 1)

            ok_button = QPushButton("Ok")
            ok_button.clicked.connect(dialog.accept)
            new_attribute_layout.addWidget(ok_button, 2, 0, 1, 2)

            dialog.setLayout(new_attribute_layout)

            accept = dialog.exec_()
            if accept:
                attribute_name = name_edit.text()
                if attribute_name not in t.get_attribute().keys():
                    attribute_value = value_edit.text()

                    if len(attribute_name) > 0 and len(attribute_value) > 0:
                        try:
                            attribute_as_int = int(attribute_value)
                        except ValueError:
                            try:
                                attribute_as_float = float(attribute_value)
                            except ValueError:
                                pass
                            else:
                                attribute_value = attribute_as_float
                        else:
                            attribute_value = attribute_as_int

                        t.add_attribute_to_id(attribute_name, attribute_value)
                        
                        self.refresh_variables()
                        self.refresh_plot()
                        self.display_attributes()

    def add_new_global(self):
        dialog = QDialog(self)
        dialog.setWindowModality(Qt.WindowModal)

        new_global_layout = QGridLayout()
        new_global_layout.setContentsMargins(10, 10, 10, 10)

        name_label = QLabel("New Global Name")
        new_global_layout.addWidget(name_label, 0, 0, 1, 1)

        name_edit = QLineEdit()
        new_global_layout.addWidget(name_edit, 0, 1, 1, 1)

        value_label = QLabel("New Global Value")
        new_global_layout.addWidget(value_label, 1, 0, 1, 1)

        value_edit = QLineEdit()
        new_global_layout.addWidget(value_edit, 1, 1, 1, 1)

        ok_button = QPushButton("Ok")
        ok_button.clicked.connect(dialog.accept)
        new_global_layout.addWidget(ok_button, 2, 0, 1, 2)

        dialog.setLayout(new_global_layout)

        accept = dialog.exec_()
        if accept:
            global_name = name_edit.text()
            global_value = value_edit.text()
            if len(global_name) > 0 and len(global_value) > 0:
                try:
                    global_as_int = int(global_value)
                except ValueError:
                    try:
                        global_as_float = float(global_value)
                    except ValueError:
                        pass
                    else:
                        global_value = global_as_float
                else:
                    global_value = global_as_int
                self.global_attribute_values[global_name] = global_value
                self.refresh_globals()
                if self.node_id is not None:
                    self.display_attributes()

    def set_hidden_attributes(self, hidden_attributes):
        self.hidden_attributes = [a for a in hidden_attributes]
        self.hidden_attributes_for_plots = [a for a in hidden_attributes]
        self.refresh_variables()

    def set_variable_type(self, variable_type):
        self.variable_type = variable_type
        self.refresh_variables()

    def add_child(self):
        t = self.tree.subtree(self.node_id)
        c = Tree()
        for name, value in t.get_attribute().items():
            if not name in self.hidden_attributes_for_plots:
                c.add_attribute_to_id(name,value)

        # self.tree.add_subtree_to_id(c,node_id=self.node_id)
        t.add_subtree(c) # correction # Romain May 27 2020 #
        child_id = t.my_children[-1].my_id

        if self.node_id in self.node_global_attributes.keys():
            self.node_global_attributes[child_id] = {a:g for a,g in self.node_global_attributes[self.node_id].items()}

        if not self.add_select_parent.isChecked():
            self.node_id = child_id
        self.tree.get_property(compute_level = 2)
        self.refresh_plot()
        self.display_attributes()

    def remove_subtree(self):
        parent_id = self.tree.subtree(self.node_id).my_parent.my_id

        t = self.tree.subtree(self.node_id)
        for subtree_id in t.list_of_ids():
            if subtree_id in self.node_global_attributes.keys():
                del self.node_global_attributes[subtree_id]

        self.tree.delete_subtree_at_id(node_id=self.node_id)

        self.node_id = parent_id
        self.tree.get_property(compute_level = 2)
        self.refresh_plot()
        self.display_attributes()


def main():
    """
    Returns
    -------
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--filename', help='Path to file containing a pickled treex Tree object', default="")
    parser.add_argument('-H', '--hidden-attributes', default=['color_plotgraph','nodescale_plotgraph','linewidth_plotgraph'], nargs='+', help='List of attribute names to hide from the edition panel')
    parser.add_argument('-V', '--variable-type', help='Whether to use node attributes or properties as plot parameters', default='attribute')
    parser.add_argument('-c', '--highlight-color', help='Color used to highlight the current node', default='chartreuse')

    args = parser.parse_args()

    if os.path.exists(args.filename):
        data = read_object(args.filename, return_mapping=True)
        tree = data['object']
        tree.get_property(compute_level = 2)
    else:
        tree = Tree()
        tree.get_property(compute_level = 2)
        assign_random_color_to_tree(tree,['grey'])
        assign_random_nodescale_to_tree(tree,[1])
        assign_random_linewidth_to_tree(tree,[1])

    QLocale.setDefault(QLocale.c())

    app = QApplication([])

    window = QMainWindow()
    window.setWindowTitle("Tree Editor")

    editor = TreeEditor(tree)
    editor.set_variable_type(args.variable_type)
    editor.set_hidden_attributes(args.hidden_attributes)
    editor.highlight_color = args.highlight_color
    window.setCentralWidget(editor)

    window.show()

    app.exec_()


if __name__ == '__main__':
    main()


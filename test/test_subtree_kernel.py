# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_subtree_kernel
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
import unittest

import matplotlib as mpl

from treex.tree import *
from treex.lossless_compression.dag import *
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.kernels import *
import matplotlib.pyplot as plt

from random import seed
import os

seed(1234)

class TestSubtreeKernel(unittest.TestCase):
    '''
    Tests the treex.kernels.subtree_kernel module
    '''

    def setUp(self):    
        self.list = [gen_random_tree(100) for i in range(15)]
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)

    def test_subtree_kernel(self):

        classes={'a': [], 'b':[], 'c':[]}
        dag_list=[]

        for i in range(5):
            t=self.list.pop()
            classes['a'].append('a_'+str(i))
            dag_list.append(tree_to_dag(t,origin='a_'+str(i)))

            t = self.list.pop()
            classes['b'].append('b_' + str(i))
            dag_list.append(tree_to_dag(t, origin='b_' + str(i)))

            t = self.list.pop()
            classes['c'].append('c_' + str(i))
            dag_list.append(tree_to_dag(t, origin='c_' + str(i)))

        dag=Dag()
        dag.concatenate(dag_list)
        dag.compress()

        kd = SubtreeKernelData(dag, classes)

        assert len(kd.matching_subtrees.keys())==15
        assert type(kd.matching_subtrees['a_0']['a_0'][0])==int

        s = kd.sample_origins(1)

        assert s.keys()==classes.keys()

        for k in s:
            assert sorted(s[k])==sorted(classes[k])


        s_discr, s_train, s_pred = kd.sample_origins(3)

        s = merge_samples([s_discr,s_train,s_pred])

        assert s.keys()==classes.keys()

        for k in s:
            assert sorted(s[k])==sorted(classes[k])

        gram_train = kd.compute_kernel(s_train,s_train)
        gram_pred = kd.compute_kernel(s_pred,s_train)

        s_predicted = svm_predict(gram_train,gram_pred,s_train,s_pred)

        assert len([o for c in s_pred.keys() for o in s_pred[c]] )==len([o for c in s_predicted.keys() for o in s_predicted[c]])

        res = performance_measure(s_pred,s_predicted,'macro')

        assert len(res)==4

        kd.assign_discriminance(s_discr)

        assert 'discriminance' in kd.dag.get_attribute(0)

        gram_train = kd.compute_kernel(s_train,s_train,discriminance=True)

        fig = view_heatmap(gram_train)
        fig = view_heatmap(gram_train,s_train)

        #gram_train = kd.compute_kernel(s_train, s_train, discriminance=True,normalized=True,kernel_param=(1,0),weight_param=('not a function'))

        assign_discriminance_nodescale(dag)
        dag.add_attribute_to_node('vector_of_discriminance',[0,1,1],0)
        assign_discriminance_color(dag)

        assert gaussian_kernel(0,0)==1
        assert gaussian_kernel(0,0,1)==1
        assert sigmoid_kernel(0,0)==0
        assert sigmoid_kernel(0,0,1)==0
        assert type(sigmoid_kernel(0,0,1,1))==float
        assert exponential_weight({}) is None
        assert polynomial_kernel(0,0,1)==0
        assert threshold(1.1)==1

        classes = {'a' : 1, 'b':2}
        p=support._points_of_interest(classes)
        assert len(p)==2

        assert discriminance_weight({'discriminance': 0},smoothstep)==0
        assert discriminance_weight({'discriminance': 1.3}, 'foo') == 1.3

        assert discriminance_weight({}) is None

        #assert type(compute_kernel_value(dag,st_mat,'a_0','b_0',exponential_weight,gaussian_kernel))==float
        #assert len(compute_kernel_matrix(dag,st_mat,[index_list,index_list],exponential_weight,gaussian_kernel))==15

        gram_train = kd.compute_kernel(s_train, s_train,weight_param = ('height',0.5),kernel_param = (1,0), kernel_function = gaussian_kernel)
        gram_train = kd.compute_kernel(s_train, s_train,discriminance=True,kernel_function = sigmoid_kernel,kernel_param=(1,0),weight_function=discriminance_weight)
        gram_train = kd.compute_kernel(s_train, s_train,kernel_param = (1,),weight_function = exponential_weight, kernel_function = gaussian_kernel,weight_param=('height',))
        gram_train = kd.compute_kernel(s_train, s_train,kernel_function = gaussian_kernel)
        gram_train = kd.compute_kernel(s_train, s_train,discriminance=True,normalized=True,kernel_param=(1,0),weight_param=(threshold,0.5))
        dag.erase_attribute('discriminance')
        a = assign_discriminance_nodescale(dag)
        assert a==None
        dag.erase_attribute('discriminance_closest_point_of_interest')
        a=assign_discriminance_color(dag)
        assert a==None



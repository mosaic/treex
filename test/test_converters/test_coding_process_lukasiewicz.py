# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_converters/test_coding_process_lukasiewicz
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.tree import *
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.converters.coding_process.lukasiewicz import *

def test_coding_process_lukasiewicz_isomorphic():
	t1=gen_random_tree(100)
	h=tree_to_lukasiewicz(t1)
	t2=lukasiewicz_to_tree(h)
	assert t1.is_isomorphic_to(t2)


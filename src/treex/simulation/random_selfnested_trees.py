# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.simulation.random_selfnested_trees
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
    Called by this module: treex.heightprofile, random, itertools

    This module implements functions to create random selfnested trees of class Tree

    - File author(s): Romain Azais <romain.azais@inria.fr>

"""

from treex.lossy_compression.selfnestedness.heightprofile import *

import random
import itertools

# --------------------------------------------------------------

def __iter_simplexe(dim,length): # Private
    max_ = length+ dim - 1
    for c in itertools.combinations(range(max_), dim-1):
        c = list(c)
        yield [(y - x - 1)
               for x, y in itertools.zip_longest([-1] + c, c + [max_])]

def __iter_interior_simplex(dim,length): # Private
    a=[]
    for l in range(length+1):
        for b in __iter_simplexe(dim,l):
            a.append(b)
    return a

def gen_random_selfnested_tree(h,d):
    """
    This function creates a random selfnested tree as a Tree class object

    Parameters
    ----------
    h: int
        height

    d: int
        outdegree
        can be a list (of size h) of outdegrees by height

    Returns
    -------
    Tree

    References
    ----------
    This simulation algorithm is presented in
    AZAIS, DURAND, and GODIN "Approximation of trees by self-nested trees"
    ALENEX 2019, San Diego (Ca)
    
    """
    hp=[]
    for i in range(1,h+1):
        if isinstance(d,list)==False:
            simplex=__iter_interior_simplex(i,d-1)
        else:
            simplex=__iter_interior_simplex(i,d[i-1]-1)
        u=random.randint(0,len(simplex)-1)
        hp_i=simplex[u]
        hp_i[-1]+=1
        hp.append( [ hp_i[k] for k in range(len(hp_i)) ] )

    snhp = SelfNestedHeightProfile(h)
    snhp.profile = hp

    return snhp.to_tree()
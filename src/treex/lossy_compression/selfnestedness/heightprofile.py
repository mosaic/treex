# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.selfnestedness.heightprofile
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
    Called by this module: treex.tree, copy.deepcopy()

    This module defines the heightprofile structure

    - File author(s): Romain Azais <romain.azais@inria.fr>

    References
    ----------
    The heightprofile of a tree is defined in
    AZAIS "Nearest embedded and embedding selfnested trees". Algorithms
    (Special Issue Data Compression Algorithms and their Applications). 2019
    All the algorithms implemented in this module are presented in this paper
    
"""

from treex.tree import *
from copy import deepcopy

# --------------------------------------------------------------

class HeightProfile(object):
    
    def __init__(self,h):
        self.profile = [[ [0] for i in range(j) ]+[[1]] for j in range(h)]

    def __len__(self):
        """
        This method allows the use of len(...) function for HeightProfile class objects

        """
        return len(self.profile)

    def __str__(self):
        """
        This method allows the use of print(...) function for HeightProfile class objects

        """
        strg=''
        for h1 in range(len(self.profile)):
            for h2 in range(len(self.profile[h1])):
                strg += 'Number of trees of height '+str(h2)+' in trees of height '+str(h1+1)+':'+'\t'+str(self.profile[h1][h2])+'\n'
        return strg

    def is_selfnested(self):
        """
        This method asserts if a heightprofile is selfnested or not   

        Returns
        -------
        bool

        """
        sn = True
        for h1 in range(len(self.profile)):
            for h2 in range(h1+1):
                if len(list(set(self.profile[h1][h2])))>1:
                    sn=False
        return sn

    def height(self):
        """
        This method returns the height of the heightprofile

        """
        return len(self.profile)

    def copy(self):
        """
        Creates a deep copy of the heightprofile

        """
        return deepcopy(self)

# --------------------------------------------------------------

class SelfNestedHeightProfile(HeightProfile):

    def __init__(self,h):
        HeightProfile.__init__(self,h)
        self.profile = [[ 0 for i in range(j) ]+[1] for j in range(h)]

    @classmethod
    def from_heightprofile(cls,height_profile):
        """
        This class method converts a HeightProfile class object to a
        SelfNestedHeightProfile class object

        Returns
        -------
        SelfNestedHeightProfile

        """
        if height_profile.is_selfnested():
            snhp = cls(len(height_profile.profile))
            
            for h1 in range(len(height_profile.profile)):
                for h2 in range(h1+1):
                    snhp.profile[h1][h2] = height_profile.profile[h1][h2][0]
            return snhp
        else:
            print('Height profile not selfnested')

    def sub_heightprofile(self,h):
        """
        Creates a new selfnested heightprofile, which is a substructure of the first one
        
        Parameters
        ----------
        h: int
            The height of the sub selfnested heightprofile

        Returns
        -------
        SelfNestedHeightProfile

        """
        snhp = SelfNestedHeightProfile(h)
        snhp.profile = deepcopy( self.profile[0:h] )
        return snhp

    def to_tree(self):
        """
        From a selfnested heightprofile, creates the corresponding tree

        Returns
        -------
        Tree

        References
        ----------
        This code is an implementation of the algorithm presented in
        AZAIS "Nearest embedded and embedding selfnested trees". Algorithms
        (Special Issue Data Compression Algorithms and their Applications). 2019

        """
        t=Tree()
        t.add_attribute_to_id('unordered_equivalence_class' ,len(self))
        for i in range(len(self.profile)):
            for j in range(self.profile[-1][i]):
                s=self.sub_heightprofile(i).to_tree()
                t.add_subtree(s)
        return t

# --------------------------------------------------------------

def __heightprofile_node(tree): # Private
# Intermediate function to compute height_profile
    h=tree.get_property('height',compute_level=1)
    l=[0 for i in range(h)]
    for child in tree.my_children:
        l[int(child.get_property('height',compute_level=1))]+=1
    return l

def heightprofile(tree):
    """
    Compute the heightprofile of a Tree class object

    Parameters
    ----------
    tree: Tree

    Returns
    -------
    HeightProfile

    References
    ----------
    This code is an implementation of the algorithm presented in
    AZAIS "Nearest embedded and embedding selfnested trees". Algorithms
    (Special Issue Data Compression Algorithms and their Applications). 2019

    """
    hauteur = tree.get_property('height',compute_level=1)
    list_subtrees = tree.subtrees_sorted_by('height')
    h_p=[]
    for h in range(1,hauteur+1):
        arbres_h=list_subtrees[h]

        out=[[] for i in range(h)]
        for t in arbres_h:
            profil_t=__heightprofile_node(t)

            for h2 in range(h):
                out[h2].append(profil_t[h2])

        h_p.append(out)
    hp = HeightProfile(hauteur)
    hp.profile = h_p
    return hp

# --------------------------------------------------------------
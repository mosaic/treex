# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_converters/test_coding_process_heightprocess
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.tree import *
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.converters.coding_process.heightprocess import *

def test_coding_process_height_process_isomorphic():
	t1=gen_random_tree(100)
	h=tree_to_height_process(t1)
	t2=height_process_to_tree(h)
	assert t1.is_isomorphic_to(t2)

def test_coding_process_height_process_height():
	t=gen_random_tree(100)
	h=tree_to_height_process(t)
	maxi = max(h['y'])
	assert maxi == t.get_property('height')

def test_coding_process_height_process_none():
	d = {'x':[],'y':[]}
	t=height_process_to_tree(d)
	assert t == None
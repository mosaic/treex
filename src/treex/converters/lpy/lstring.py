# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.converters.lpy.lstring
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       File contributor(s):
#           Bruno Leggio <bruno.leggio@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
Called by this module: treex.tree()

This module allows the conversion of l-strings to the treex format of trees
and vice versa

- File author(s): Romain Azais <romain.azais@inria.fr>
- File contributor(s): Bruno Leggio <bruno.leggio@inria.fr>

"""

from treex.tree import *

import logging

# ------------------------------------------------------------------------------
# lstring to tree

def __format_lstring(lstring_to_be_formated):

    if lstring_to_be_formated.split('\n')[0]=='begin modlist':

        stri = lstring_to_be_formated

        stri = stri.replace('[]','')
        temp = stri.split('\n')

        lst_mods = []

        dico_mod_var = {}
        c = 0
        line = temp[c]
        while line !='end modlist':

            if line != 'begin modlist':

                a = line.split(':')

                lst_mods.append(a[0])

                b = a[1].split(',')

                dico_mod_var[a[0]] = []
                if b!=['']:
                    for var in b:
                        [var1,typ1] = var.split('(')
                        typ1 = typ1[0:(len(typ1)-1)]

                        dico_mod_var[a[0]].append([var1,typ1])

                c+=1
                line = temp[c]

            else:
                c+=1
                line = temp[c]

        lstring = temp[-1]
        if lstring[0] == '[' and lstring[-1]==']':
            lstring = lstring[1:(len(lstring)-1)]

        lst_mod_var = []
        for i in range(-1 , len(lstring)-1):

            if (i==-1 or lstring[i]==']' or lstring[i] == '[' or lstring[i]==')') and lstring[i+1] != '[' and lstring[i+1] != ']':

                end = 0
                c=0
                while end == 0:
                    c+=1
                    if lstring[i+1+c] == ']' or lstring[i+1+c] == '(':
                        end = 1


                mod = lstring[(i+1):(i+c+1)] # module only

                if mod not in lst_mods :
                    logging.warning('found module named '+mod+' that does not appear in list of modules')

                if lstring[i+len(mod)+1] == '(':

                    end = ''
                    c = 1
                    while end!=')':
                        c+=1
                        end = lstring[i+c]

                    index_beg = i+1
                    index_end = i+c+1

                else:

                    index_beg = i+1
                    index_end = i+1+len(mod)


                mod_var = lstring[index_beg:index_end] # module with variables

                if mod_var not in lst_mod_var:
                    lst_mod_var.append(mod_var)

        lst_mod_var.sort(reverse=True)
        for mod_var in lst_mod_var:
            if '(' in mod_var:
                lst_var = mod_var.split('(')
                typ = lst_var[0]
                variables = lst_var[1].split(',')
                variables[-1] = variables[-1][0:(len(variables[-1])-1)]

                new_mod_var = "{'module':'"+typ+"',"
                i=1

                if typ in dico_mod_var.keys():
                    if len(variables) == len(dico_mod_var[typ]):

                        for vari in variables:
                            new_mod_var += "'"+dico_mod_var[typ][i-1][0] + "': '"+vari+"'"
                            if i <len(variables):
                                new_mod_var += ','
                            i += 1

                    else:

                        logging.warning('module named '+typ+' found with '+str(len(variables))+' variables instead of '+str(len(dico_mod_var[typ])))

                        for vari in variables:
                            new_mod_var += "'var"+str(i)+"': '"+vari+"'"
                            if i <len(variables):
                                new_mod_var += ','
                            i += 1
                else:

                    logging.warning('module named '+typ+' found with '+str(len(variables))+' variables')

                    for vari in variables:
                        new_mod_var += "'var"+str(i)+"': '"+vari+"'"
                        if i <len(variables):
                            new_mod_var += ','
                        i += 1


                new_mod_var +='}'

                lstring = lstring.replace(mod_var,new_mod_var)
            else:
                new_mod_var = "{'module':'"+mod_var+"'}"
                lstring = lstring.replace(mod_var,new_mod_var)

        return lstring
    else:
        return lstring_to_be_formated


def __closing_par_node(strg , node_par='{}'): # Private
    index = -1
    c=0
    while index == -1:
        if strg[c]==node_par[1]:
            index = c
        c+=1
    return index


def __closing_par(chaine,par): # Private
    par_ouvr = par[0]
    par_ferm = par[1]

    compteur=0
    out=0
    i=0
    while out==0:
        if chaine[i]==par_ouvr:
            compteur+=1
        elif chaine[i]==par_ferm and compteur!=1:
            compteur-=1
        elif chaine[i]==par_ferm and compteur==1:
            out=i
        i+=1
    return out


def __lstring_to_tree_increment_function(strg ,  flag_eval, par, node_par, attribute_name, axis_name, include_node_par_in_attribute, axis):

    lstrg = __format_lstring(strg)
    t = Tree()
    t.add_attribute_to_id(axis_name,axis)

    # lstrg[0] is node_par[0] # opening node
    index_ferm = __closing_par_node( lstrg , node_par ) + 1

    if index_ferm == len(lstrg):

        if include_node_par_in_attribute:
            attr = lstrg[0:len(lstrg)]
        else:
            attr = lstrg[1:len(lstrg)-1]

        if attr != '':
            if flag_eval:
                attr = eval(attr)
            t.add_attribute_to_id(attribute_name,attr)

    else:

        if include_node_par_in_attribute:
            attr = lstrg[0:index_ferm]
        else:
            attr = lstrg[1:index_ferm-1]

        lst_subtrees = []
        next_index = index_ferm

        while next_index < len(lstrg) and lstrg[next_index] != node_par[0]:
            # then strg[next_index] = par[0]

            st_end = __closing_par(lstrg[next_index:] , par) + next_index

            subt = lstrg[next_index+1 : st_end]

            lst_subtrees.append(subt)

            next_index = st_end+1

        subt = lstrg[next_index:]

        flag_interrupted_branch = False
        if subt == '' and lst_subtrees != []:
            flag_interrupted_branch = True

        if subt != '':
            lst_subtrees.append(subt)

        if attr != '':
            if flag_eval:
                attr = eval(attr)
            t.add_attribute_to_id(attribute_name , attr)

        compteur = 1
        for substr in lst_subtrees:

            if compteur == len(lst_subtrees) and flag_interrupted_branch == False:
                t.add_subtree(__lstring_to_tree_increment_function(substr , flag_eval , par , node_par , attribute_name , axis_name , include_node_par_in_attribute , True))

            else:
                t.add_subtree(__lstring_to_tree_increment_function(substr , flag_eval , par , node_par , attribute_name , axis_name , include_node_par_in_attribute , False))

            compteur+=1

    return t

def lstring_to_tree(lstrg, header = None, flag_eval = True , par = '[]' , node_par = '{}' , attribute_name = 'lstring_attribute' , axis_name = 'lstring_successor' , include_node_par_in_attribute = True , axis = True ):
    """
    This method allows to convert a l-string into a Tree class object

    Parameters
    ----------
    lstrg: str
        a l-string

    header: str
        contains the list of modules with variables
        and comments

    flag_eval: bool
        if True, attributes are evaluated

    par: str
        string of len 2
        - first element is the opening bracket for new axis
        - second element is the closing bracket for new axis

    node_par: str
        string of len 2
        - first element is the opening bracket for nodes
        - second element is the closing bracket for nodes

    attribute_name: str
        name of the attribute to be added to the tree

    axis_name: str
        name of the attribute corresponding to the axis level

    include_node_par_in_attribute: bool
        if True, opening and closing node brackets are included in attribute value

    axis: bool
        True iff the current node is the successor of its parent

    Returns
    -------
    Tree

    See Also
    --------
    tree_to_lstring(...)

    """

    ### Read header and split list of modules and comments ###
    if header != None:
        init_modlist = -1
        end_modlist = -1
        init_comment = -1
        end_comment = -1
        for i in range(len(header.split("\n"))):
            
            line = header.split("\n")[i]
            if line == "begin modlist":
                init_modlist = i
            elif line == "end modlist":
                end_modlist = i
            elif line == "begin comment":
                init_comment = i
            elif line == "end comment":
                end_comment = i

        # List of modules
        header_modules = ""
        for i in range( init_modlist , end_modlist+1):
            header_modules += header.split("\n")[i] + "\n"

        # Comments
        if init_comment != -1 and end_comment != -1:
            header_comm = ""
            for i in range( init_comment+1 , end_comment):
                header_comm += header.split("\n")[i]
        else:
            header_comm = None

        strg = header_modules + "\n" + lstrg

    else:
        header_comm = None
        strg = lstrg

    ### Construction of the tree ###
    t = __lstring_to_tree_increment_function(strg, flag_eval, par, node_par, attribute_name, axis_name, include_node_par_in_attribute, axis)

    ### Add comment attribute to the root of the tree ###
    t.add_attribute_to_id('lstring_comment',header_comm)

    return t


# ------------------------------------------------------------------------------
# tree to lstring

def __format_lstring_attribute(t , header , lstring_attribute_name = 'lstring_attribute'):

    ### Read the header
    dico_mod = {}

    lines = header.split('\n')
    go = False
    for lin in lines:
        if 'end modlist' in lin:
            go = False

        if go:
            lst_elts = lin.split(':')
            mod_name = lst_elts[0]
            lst_elts = lst_elts[1].split(',')

            lst_var_names = []
            for elt in lst_elts:
                ind_par = elt.index('(')
                var_name = elt[0:ind_par]
                lst_var_names.append(var_name)
            dico_mod[mod_name] = lst_var_names
        
        if 'begin modlist' in lin:
            go = True

    ### Edit the tree
    for i in t.list_of_ids():
        lstring_attribute_i = t.subtree(node = i).get_attribute(lstring_attribute_name)
        out = ''
        out += lstring_attribute_i['module']
        if lstring_attribute_i['module'] in dico_mod.keys():

            out += '('

            lst_var_names = dico_mod[lstring_attribute_i['module']]
            for var_name in lst_var_names:
                out += lstring_attribute_i[var_name]
                out += ','

            out = out[0:(len(out)-1)] + ')'

        t.add_attribute_to_id(attribute_name = lstring_attribute_name + '_str',attribute_value = out,node_id=i)


def __tree_to_lstring_build(t , par = '[]' , node_par = None , attribute_name = 'lstring_attribute' , axis_name = 'lstring_successor' ):

    if attribute_name == None:
        attr = ''
    else:
        if t.get_attribute(attribute_name)==None:
            attr=''
        else:
            attr = str(t.get_attribute(attribute_name))

    if t.get_property('size',compute_level=1) == 1:
        if t.my_parent == None:
            if node_par == None:
                strg = attr
            else:
                strg = node_par[0] + attr + node_par[1]

        else:
            if t.get_attribute(axis_name):

                if node_par == None:
                    strg = attr
                else:
                    strg = node_par[0] + attr + node_par[1]

            else:

                if node_par == None:
                    strg = par[0] + attr + par[1]
                else:
                    strg = par[0] + node_par[0] + attr + node_par[1] + par[1]

    else:

        if node_par == None:
            strg = attr
        else:
            strg = node_par[0] + attr + node_par[1]

        children_strg = ''

        for child in t.my_children:

            if child.get_attribute(axis_name):
                children_strg += __tree_to_lstring_build(child , par , node_par , attribute_name , axis_name)

            else:
                children_strg = __tree_to_lstring_build(child , par , node_par , attribute_name , axis_name) + children_strg

        strg+=children_strg

        if t.my_parent != None:
            if t.get_attribute(axis_name) == False:
                strg = par[0]+strg+par[1]

    return strg


def tree_to_lstring(t , par = '[]' , node_par = None , attribute_name = 'lstring_attribute' , axis_name = 'lstring_successor' , header = None):
    """
    This method allows to convert a tree with axis into a l-string

    Parameters
    ----------
    t: Tree
    
    header: str
        contains the list of modules with variables
        and comments

    par: str
        string of len 2
        - first element is the opening bracket for new axis
        - second element is the closing bracket for new axis

    node_par: str
        string of len 2
        - first element is the opening bracket for nodes
        - second element is the closing bracket for nodes
        if None: no brackets are added

    attribute_name: str
        name of the attribute in the tree to be added to the l-string

    axis_name: str
        name of the attribute corresponding to the axis level in the tree

    Returns
    -------
    str

    See Also
    --------
    lstring_to_tree(...)
    assign_axis_to_tree(...)

    """
    
    if header != None:
        __format_lstring_attribute(t , header , attribute_name)
        attr_name  = attribute_name + '_str'
    else:
        attr_name  = attribute_name

    return __tree_to_lstring_build(t , par = par , node_par = node_par , attribute_name = attr_name , axis_name = axis_name)



def assign_axis_to_tree(t , axis_name = 'lstring_successor' , axis = True):
    """
    This method allows to add axis to a tree

    Parameters
    ----------
    t: Tree

    axis_name: str
        name of the attribute corresponding to the axis level in the tree

    axis: bool
        if True, t is the priviledged child of its parent called successor

    Returns
    -------
    None

    See Also
    --------
    tree_to_lstring(...)

    """
    t.add_attribute_to_id(axis_name , axis)
    compteur = 0
    for child in t.my_children:
        if compteur == 0:
            assign_axis_to_tree(child,axis_name , True)
        else:
            assign_axis_to_tree(child,axis_name , False)
        compteur += 1

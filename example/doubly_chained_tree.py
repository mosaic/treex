# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/doubly_chained_tree
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to compute the doubly chained tree of a tree (and how to go back to the tree)
"""

from treex.tree import *
from treex.doublychainedtree import *
from treex.simulation import gen_random_tree

t=gen_random_tree(15)

print('-------------------------------------')

print("initial tree")
t.get_attribute()
t.treeprint('one attribute','height')

print('-------------------------------------')

print("doubly chained tree")
doublychainedtree=DoublyChainedTree.from_tree(t)
doublychainedtree.get_attribute()
doublychainedtree.treeprint('one attribute','height')

print('-------------------------------------')

print("back to initial tree")
atree=doublychainedtree.to_tree()
atree.get_attribute()
atree.treeprint('one attribute','height')




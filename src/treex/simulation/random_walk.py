# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.simulation.random_walk
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: treex.tree, random

This module implements a conditioned random walk on trees

- File author(s): Romain Azais <romain.azais@inria.fr>
"""

from treex.tree import *
from treex.analysis.edit_distance.zhang_dag import *
from treex.analysis.edit_distance.incremental_zhang import *

from treex.utils.save import *

import random, math
import sys

#################################################################################

def __simul_height(parameter):
	u=random.random()
	cumfreq=[0]
	for i in range(len(parameter)):
		cumfreq.append(cumfreq[i]+parameter[i])
		if u>cumfreq[i] and u<=cumfreq[i+1]:
			out=i
	return out

def __random_edit_based_on_height(tree_dag , distrib , p=0.5):

	# NB: the tree must be of height 1 or more
	# If the tree has two nodes, an insertion operation will be selected

	# ==========================================
	# Random selection of height
	h_max = tree_dag.get_property(prop = 'height',compute_level=1)

	# Dictionary height-nodes
	dst = tree_dag.nodes_sorted_by(properties = 'height')

	distrib_2 = [ distrib[k] for k in range(min(h_max,len(distrib))) ]
	distrib_2 = [ distrib_2[k]/sum(distrib_2) for k in range(len(distrib_2))]
	hs = __simul_height(distrib_2) # NB: height of the tree can not be selected
	# ==========================================

	# ==========================================
	# Random selection of node
	nod = random.choice(dst[hs]) # NB: root of the tree can not be selected
	# ==========================================

	# ==========================================
	# Random selection of operation
	if hs == 0: # the node is a leaf
		if tree_dag.get_property(prop='size', compute_level=1)==2:
			op = 'add_leaf'
		else:
			u1 = random.random()
			if u1<p: # add leaf
				op = 'add_leaf'
			else:
				op = 'del_leaf'

	else: # the node is internal
		# ----------------------------------------------
		nodes_without_brothers = []
		leaves = []
		for v in tree_dag.my_structure.keys():

			if tree_dag.my_structure[v]['children'] == []:
				leaves.append(v)

			elif len(tree_dag.my_structure[v]['children']) == 1:
				w = tree_dag.my_structure[v]['children'][0]
				if len(tree_dag.my_structure[w]['children'])>0:
					nodes_without_brothers.append(w)
		# ----------------------------------------------

		if nod in nodes_without_brothers: # the node has no brother
			u1 = random.random()
			if u1<p: # add node
				u2 = random.random()
				if u2<0.5:
					op = 'add_internal_node'
				else:
					op = 'add_leaf'
			else:
				op = 'del_internal_node'
		else:
			op = 'add_internal_node'
	# ==========================================

	return {'operation_name':op,'node':nod}

#################################################################################

def __uniform_random_edit(tree_dag):

	nodes_without_brothers = []
	leaves = []
	others = []
	for v in tree_dag.my_structure.keys():

		if tree_dag.my_structure[v]['children'] == [] and v not in tree_dag.my_roots:
			leaves.append(v)

		elif len(tree_dag.my_structure[v]['children']) == 1:
			w = tree_dag.my_structure[v]['children'][0]
			if len(tree_dag.my_structure[w]['children'])>0:
				nodes_without_brothers.append(w)
			### NB: the root is not in this set

	l_l = len(leaves)
	l_nwb = len(nodes_without_brothers)
	### NB: leaves and nodes_without_brothers can be deleted ###

	u1 = random.random()
	if u1<0.5 and l_l+l_nwb>0: # delete a node with proba=0.5

		u2 = random.random()

		if u2<1.0*l_l/(l_l+l_nwb):
			nod = random.choice(leaves)
			op = 'del_leaf'
		else:
			nod = random.choice(nodes_without_brothers)
			op = 'del_internal_node'
	else: # add a node with proba=0.5
		nod = random.choice( tree_dag.nodes() )
		u2 = random.random()
		if u2<0.5 or tree_dag.my_structure[nod]['children']==[]: ### an internal node can not be added to a leaf
			op = 'add_leaf'
		else:
			op = 'add_internal_node'

	return {'operation_name':op , 'node':nod}

#################################################################################

def random_edit_operation(tree_dag , parameter):
	if parameter == 'uniform':
		return __uniform_random_edit(tree_dag)
	elif isinstance(parameter,list):
		p = parameter[0]
		distrib = parameter[1]
		return __random_edit_based_on_height(tree_dag , distrib , p)

#################################################################################

def back_edit_operation(info):
	if info['operation_name'] == 'add_leaf':
		return {'operation_name':'del_leaf' , 'node' : info['node']}

	elif info['operation_name'] == 'del_leaf':
		return {'operation_name':'add_leaf' , 'node': info['parent']}

	elif info['operation_name'] == 'add_internal_node':
		return {'operation_name': 'del_internal_node', 'node': info['node']}

	elif info['operation_name'] == 'del_internal_node':
		return {'operation_name': 'add_internal_node' , 'node':info['parent']}

#################################################################################

def random_walk(starting_point , reference , horizon , parameter , radius = math.inf , check=False):
	t = reference
	s = starting_point
	d_t = tree_to_dag(t , compression = 'no_compression')
	d_s = tree_to_dag(s , compression = 'no_compression')

	# ini
	zed_t_s = zhang_edit_distance_dags(d_t,d_s,algo='NS',all_info=True)
	ini_dist = zed_t_s['distance']

	lst_trees = [d_s]
	lst_dist = [zed_t_s['distance']]
	lst_info = []

	errs = 0
	for i in range(horizon):
		sys.stdout.write('\r'+"step: %i" % i)

		# -----------------------------------------------
		# Random operation
		if zed_t_s['distance'] > radius:
			rop = back_edit_operation(zed_t_s['info'])
		else:
			rop = random_edit_operation(d_s,parameter)

		# Operation and new edit distance
		zed_t_s = incremental_zhang_edit_distance(d_t,d_s,rop,zed_t_s,copy=False)

		# Edited tree
		d_s = zed_t_s['edited_tree']
		if check:
			zed_check = zhang_edit_distance_dags(d_t,d_s,algo='NS',all_info=False)
			if zed_check != zed_t_s['distance']:
				errs += 1

		lst_trees.append(d_s)
		lst_dist.append(zed_t_s['distance'])
		lst_info.append(zed_t_s['info'])
		# -----------------------------------------------

	print('\n')

	if check:
		return {'walk':lst_trees , 'distances':lst_dist , 'editions':lst_info , 'errors':errs}
	else:
		return {'walk':lst_trees , 'distances':lst_dist , 'editions':lst_info}

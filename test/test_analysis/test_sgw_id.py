# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_analysis/test_sgw_id
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.tree import *
from treex.simulation.spinal_galton_watson import gen_spinal_galton_watson
from treex.analysis.spinal_galton_watson.identification import sgw_identify_spinal_structure

def test_sgw_id_spine1():
	for i in range(10):
		h = 0
		while h<10:
			t = gen_spinal_galton_watson('',[0.2,0.5,0.3],'',[0.0,0.3,0.7],10)
			h = t.get_property('height')
		d1 = sgw_identify_spinal_structure(t)
		d2 = t.dict_of_ids()
		for i in d2.keys():
			if d2[i]['attributes']['identified_vertex_type'] in ['normal','special']:
				assert d2[i]['attributes']['identified_vertex_type'] == d2[i]['attributes']['vertex_type']
		assert isinstance(d1['identified_spine'],list) == True
		assert isinstance(d1['possible_spines'],list) == True

def test_sgw_id_spine2():
	for i in range(10):
		h = 0
		while h<5:
			t = gen_spinal_galton_watson('',[0.2,0.5,0.3],'',[0.0,0.3,0.7],5)
			h = t.get_property('height')
		d1 = sgw_identify_spinal_structure(t)
		d2 = t.dict_of_ids()
		s = 0
		for i in d2.keys():
			if d2[i]['attributes']['generation'] < h :
				s += 1
		assert len(d1['numbers_of_children']['observed_nodes']) == s

def test_sgw_id_spine3():
	for i in range(10):
		h = 0
		while h<5:
			t = gen_spinal_galton_watson('',[0.2,0.5,0.3],'',[0.0,0.3,0.7],5)
			h = t.get_property('height')
		d1 = sgw_identify_spinal_structure(t,4)
		d2 = t.dict_of_ids()
		for i in d2.keys():
			if d2[i]['properties']['depth']<4:
				assert d2[i]['attributes']['identified_vertex_type'] != 'unobserved'
			else:
				assert d2[i]['attributes']['identified_vertex_type'] == 'unobserved'
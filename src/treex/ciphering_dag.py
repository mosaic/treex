# -*- python -*-
# -*- coding: utf-8 -*-
#
#       src/ciphering_dag
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.lossless_compression.dag import *
from treex.ciphering import *
from copy import deepcopy

def __remove_ids(d, dic):
    for node in dic:
        tree_id = d.get_attribute(node, 'tree_ids')
        tree_id = [id for id in tree_id if (id not in dic[node])]
        d.add_attribute_to_node("tree_ids", tree_id, node)

def identity(t,labels):
    f = Bijection()
    for st in t.list_of_subtrees():
        a = st.get_attribute(labels)
        f.extend(a,a)
    return f

def approximate_partition(t,labels,tree_ids,epsilon,objective,estimate,error):
    partition = {}
    while len(tree_ids) > 0:
        test = False
        id = tree_ids.pop()
        for rep in partition.keys():
            st_rep = t.subtree(node=rep)
            if st_rep.my_parent is not None:
                p_rep = st_rep.my_parent.my_id
                st_rep.my_parent = None
            else:
                p_rep = None
            st_id = t.subtree(node=id)
            if st_id.my_parent is not None:
                p_id = st_id.my_parent.my_id
                st_id.my_parent = None
            else:
                p_id = None

            tu = regression_ciphering(st_rep, st_id, labels,objective,estimate,error)

            if p_rep is not None:
                st_rep.my_parent = t.subtree(node=p_rep)
            if p_id is not None:
                st_id.my_parent = t.subtree(node=p_id)

            if tu[0]:
                _,param,f,_ = tu

                if error(f,objective,param) < epsilon:
                    test = True
                    partition[rep].append((id, param))
                    break
        if not test:
            f = identity(t.subtree(node=id),labels)
            param = estimate(f,objective)
            partition[id] = [(id,param)]
    return partition

def exact_partition(t,labels,tree_ids):
    partition = {}
    while len(tree_ids) > 0:
        test = False
        id = tree_ids.pop()
        for rep in partition.keys():
            st_rep = t.subtree(node=rep)
            if st_rep.my_parent is not None:
                p_rep = st_rep.my_parent.my_id
                st_rep.my_parent = None
            else:
                p_rep = None
            st_id = t.subtree(node=id)
            if st_id.my_parent is not None:
                p_id = st_id.my_parent.my_id
                st_id.my_parent = None
            else:
                p_id = None

            tu = ciphering_isomorphism(st_rep, st_id, labels)

            if p_rep is not None:
                st_rep.my_parent = t.subtree(node=p_rep)
            if p_id is not None:
                st_id.my_parent = t.subtree(node=p_id)

            if tu[0] == True:
                test = True
                _, f, _ = tu
                partition[rep].append((id, f))
                break
        if not test:
            partition[id] = [(id, identity(t.subtree(node=id), labels))]
    return partition

def tree_to_ciphering_dag(t,labels,exact=True,**kwargs):
    ## unordered trees only

    d = tree_to_dag(t)

    parents = {}

    for node in d.nodes():
        for children in d.children(node):
            if children not in parents:
                parents[children]=set()
            parents[children].add(node)

    nodes_sorted = d.nodes_sorted_by('height')
    i = max(d.nodes()) + len(d.nodes())

    for h in sorted(nodes_sorted.keys(),reverse=True):
        for node in nodes_sorted[h]:

            if h == max(nodes_sorted.keys()):
                tree_ids = [t.my_id]
            else:
                tree_ids = []
                for parent in parents[node]:
                    for children in t.subtree(node=d.my_structure[parent]['attributes']['represents']).my_children:
                        if children.get_attribute('unordered_equivalence_class')==node:
                            tree_ids.append(children.my_id)

            if exact:
                partition = exact_partition(t,labels,tree_ids)
            else:
                partition = approximate_partition(t,labels,tree_ids,**kwargs)

            struct = d.my_structure.pop(node, None)

            for rep in partition.keys():
                new_node = deepcopy(struct)
                i+=1

                for children in new_node['children']:
                    if node in parents[children]:
                        parents[children].remove(node)
                    parents[children].add(i)

                new_node['attributes']['represents']=rep
                new_node['attributes'][labels]=t.subtree(node=rep).get_attribute(labels)
                new_node['attributes']['children_cipher'] = []

                for id,f in partition[rep]:
                    t.add_attribute_to_id('unordered_ciphering_equivalence_class', i, id)
                    st = t.subtree(node=id)
                    if st.my_parent is not None:
                        p = t.subtree(id).my_parent.get_attribute('unordered_ciphering_equivalence_class')
                        children = d.children(p)
                        children.remove(node)
                        children.append(i)
                        d.my_structure[p]['children'] = children
                        d.my_structure[p]['attributes']['children_cipher'].append((i, f))
                d.my_structure[i] = new_node

    d.my_compression['attribute']=labels
    d.assign_leaves()
    d.assign_roots()
    d.assign_origin(list(d.my_origins.keys())[0])
    d.get_property(compute_level=2)
    # d.renumber_nodes() ## Warning: don't do it since it won't renumber the attribute 'children_cipher'

    return d

def __recursive_reconstruct(d,node,f,labels):
    t=Tree()
    t.add_attribute_to_id(labels,f[d.get_attribute(node,labels)])
    for cnode, cf in d.get_attribute(node,'children_cipher'):
        # print(f,cf)
        t.add_subtree(__recursive_reconstruct(d,cnode,f.compose(cf),labels))
    return t

def decompress_tree(d,labels):
    # cas exact uniquement !
    t = Tree()
    t.add_attribute_to_id(labels,d.get_attribute(d.my_roots[0],labels))
    for node,f in d.get_attribute(d.my_roots[0],'children_cipher'):
        t.add_subtree(__recursive_reconstruct(d,node,f,labels))
    return t
# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/dag_compression

#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to compute the DAG compression of a tree
"""

from treex.tree import *
from treex.dag import *
from treex.simulation import gen_random_tree
from treex.visualization.options import assign_random_nodescale_to_tree

# Example 1: DAG compression of an unordered tree
print('Example 1')

t = gen_random_tree(100)
d = tree_to_optimaldag(t,tree_type = 'unordered')

print(d)
print(d.nodes())
print(d.get_attribute())

print('-------------------------------------')

# Example 2: DAG compression of an ordered tree and back to tree
print('Example 2')

t = gen_random_tree(100)
d = tree_to_optimaldag(t,tree_type = 'ordered')
t2 = d.to_tree()

print(t.is_isomorphic_to(t2,tree_type='ordered'))

print('-------------------------------------')

# Example 3: DAG compression with attributes
print('Example 3')

t = gen_random_tree(50)
assign_random_nodescale_to_tree(t,[1,2])
print(t.get_attribute())

d_sn = tree_to_optimaldag(t,tree_type = 'unordered',attribute='nodescale_plotgraph')
d = tree_to_optimaldag(t,tree_type='unordered')

print(d_sn)
print(d)

print('-------------------------------------')

# Example 4: DAG recompression 1/2
print('Example 4')
t = gen_random_tree(50)
d = tree_to_treedag(t)
print(d)

d_opt = OptimalDag.from_dag(d)
print(d_opt)

print('-------------------------------------')

# Example 5: DAG recompression 2/2
print('Example 5')
t = gen_random_tree(50)
assign_random_nodescale_to_tree(t,[1,2])

d_sn = tree_to_optimaldag(t,tree_type = 'unordered',attribute='nodescale_plotgraph')
d_opt = OptimalDag.from_dag(d_sn)

print(d_sn)
print(d_opt)

# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.converters.xml
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#           with an anonymous help
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
Called by this module: lxml

- File author(s): Romain Azais <romain.azais@inria.fr>
"""

from treex.tree import *

try:
	from lxml import etree
except ImportError:
	print("lxml should be installed to use these functionalities (conda install lxml)")
else:
	def __grow_xml_tree(lxml_vertex , current_tree , attribute_name): # Private

		for lxml_child in lxml_vertex.getchildren():

			# Read attributes
			cles = lxml_child.keys()
			vals = lxml_child.values()
			dict_attr = {}
			for i in range(len(cles)):
				dict_attr[cles[i]] = vals[i]

			# Creation of the corresponding vertex (treex)
			t = Tree()
			t.add_attribute_to_id(attribute_name , dict_attr)

			# Add the new subtree to the current tree
			current_tree.add_subtree(t)

			__grow_xml_tree( lxml_child , current_tree.subtree(t.my_id) , attribute_name)


	def __xml_tree_to_tree(xmltree , attribute_name): # Private

		r = xmltree.getroot()
		# Initialisation
		cles = r.keys()
		vals = r.values()
		dict_attr = {}
		for i in range(len(cles)):
			dict_attr[cles[i]] = vals[i]
		t = Tree()
		t.add_attribute_to_id(attribute_name, dict_attr)

		__grow_xml_tree(r,t,attribute_name)

		return t

	def xmlfile_to_tree(path , attribute_name = 'XML_tag'):

		xmltree = etree.parse(path)
		return __xml_tree_to_tree(xmltree , attribute_name)
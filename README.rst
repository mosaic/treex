========================
treex
========================

.. {# pkglts, doc

.. #}

treex is a Python library for manipulating rooted trees.
The trees can be ordered or not, with or without labels on their vertices.

:Coordination: Romain Azais and Florian Ingels (since 2024)

:Contributors: Farah Ben Naoum, Guillaume Cerutti, Didier Gemmerle, Salah Habibeche, Benoit Henry

:Team: Inria team MOSAIC

:Language: Python 3

:Supported OS: Linux, Mac OS, Windows

:Licence: LGPL

:Documentation: https://mosaic.gitlabpages.inria.fr/treex/

:Wiki: https://gitlab.inria.fr/mosaic/treex/wikis/home





The package provides a data structure for rooted trees as well as the following main functionalities:

* **Random generation algorithms**
* **DAG compression** for ordered or not, labeled or not, trees
* **Approximation algorithms** for unordered trees
* **Edit distance** for unordered labeled trees
* **Computation of coding processes** (Harris path, Lukasiewicz walk and height process)
* **Visualization algorithms** in Matplotlib or in LaTeX

************************
Requirements
************************

* Python >=3.6
* NetworkX: https://networkx.github.io/
* NumPy: http://www.numpy.org/
* matplotlib: https://matplotlib.org/

************************
Install
************************

Pre-requisite install miniconda
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Choose the Python 3.7 version suitable for your system on
   https://conda.io/miniconda.html

-  Open a terminal window, go to your Download folder and run
   ``bash Miniconda...`` where ``Miniconda...`` is the name of the file
   that you just downloaded

      During the installation you will be asked a number of choices

      -  You can set up the directory of your choice when asked, e.g.
         ``~/.miniconda``

      -  Make sure to answer YES when asked to add conda to your PATH

User procedure
~~~~~~~~~~~~~~

Create a new environment for treex
----------------------------------

::

   conda create -n treex -y python>=3.6

Activate the environment
------------------------

::

   source activate treex

You will have to run this command any time you want to use treex. To
exit the environment, enter:

::

   source deactivate

Install treex
-------------

::

   conda install -c mosaic treex

Developer procedure
~~~~~~~~~~~~~~~~~~~

.. _create-a-new-environment-for-treex-1:

Create a new environment for treex
----------------------------------

-  Create a file called ``treex.yml`` containing the list of
   dependencies

::

   name: treex
   channels:
     - defaults
   dependencies:
     - python=3.7
     - networkx
     - matplotlib
     - scikit-learn
     - lxml
     - jupyter
     - qtconsole
     - nose
     - coverage

-  Define a new **conda** environment

::

   conda env create -f treex.yml

-  Activate the environment

::

   source activate treex

Download treex package and install it
-------------------------------------

-  Download the sources of treex to the directory of your choice and run
   the setup

::

   git clone https://gitlab.inria.fr/mosaic/treex.git
   cd treex
   git checkout develop
   python setup.py develop --prefix=$CONDA_PREFIX
   cd ..

-  Check that treex is correctly installed by running the tests

::

   cd treex
   nosetests -v

-  Update treex

::

   cd treex
   git pull
   python setup.py develop --prefix=$CONDA_PREFIX

# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.kernels.fost_kernel
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
This module introduce a new class, FostKernelData, in order to compute the FoST kernel from a dataset. The FoST kernel will be introduced in a upcoming paper.

- File author(s): Florian Ingels <florian.ingels@inria.fr>

"""

import math
from itertools import combinations_with_replacement

from treex.kernels.support import *
import numpy as np
from treex.kernels.support import _points_of_interest, _dist_to_points


class FostKernelData(object):
    """
    This class is used to compute the STUFF kernel.

    Attributes
    ----------

    fost:
        a list of FoST object, enumerated from the dataset to process, via the ``extract_fost`` function of the ``fost_extraction`` module.

    origins:
        a list of all origins found in the FoST list

    classes:
        a dictionary, where keys are classes and to each class is associated the list of origins in the dataset that belong to that class. If all origins are not covered by a class, they will be referenced under the class `unknown`

    matching_fost: dict
        Creates a dictionary which is used to compute the fost kernel.

        The :math:`(i,j)` entry is a list of tuple :math:`(n_i,n_j,t)`, where :

            :math:`t` is the index of the FoST, such as both :math:`i` and :math:`j` belongs
            to :math:`\\text{origins}(t)`

            :math:`n_i` and :math:`n_j` are respectively, the presence of :math:`t` in :math:`T_i` and :math:`T_j`

    """

    __slots__ = 'fost', 'classes', 'matching_fost', 'origins'

    def __init__(self, fost_list, classes={}):

        self.fost = fost_list

        self.origins=fost_list[0].origins

        for fost in fost_list:
            self.origins= self.origins | fost.origins

        classes_origins = []
        for c in classes.keys():
            classes_origins += classes[c]

        unknown = list(self.origins.difference(set(classes_origins)))
        self.classes = copy.deepcopy(classes)
        self.classes['unknown'] = unknown

        self.__matching_fost()

    def sample_origins(self, n):
        """
        This methods will split at random the `classes` attribute into :math:`n` classes repartition balanced dictionary on the same format of `classes`.
        :param n: int
            the number of class sample to obtain
        :return: a list of dictionaries
        """
        classes = copy.deepcopy(self.classes)
        del classes['unknown']
        samples = [{} for i in range(n)]
        i = 0
        for c in classes:
            while len(classes[c]) > 0:
                o = classes[c].pop(random.randrange(len(classes[c])))
                if c in samples[i].keys():
                    samples[i][c].append(o)
                else:
                    samples[i][c] = [o]
                i = (i + 1) % n

        if n==1:
            return samples[0]
        else:
            return tuple(samples)

    def __matching_fost(self):
            m = {}

            for i, j in list(combinations_with_replacement(list(self.origins), 2)):
                i, j = tuple(sorted([i, j]))
                m[(i, j)] = []

            for fost in self.fost:
                couples = list(combinations_with_replacement(list(fost.origins), 2))
                for i, j in couples:
                    index = sorted([i, j])
                    i, j = tuple(index)
                    m[(i, j)].append(self.fost.index(fost))

            self.matching_fost=m

    def __compute_single_kernel_value(self, i, j, **kwargs):
        if 'weight_param' in kwargs.keys():
            weight_param = kwargs['weight_param']
        else:
            weight_param = ()

        if 'kernel_param' in kwargs.keys():
            kernel_param = kwargs['kernel_param']
        else:
            kernel_param = ()

        if 'weight' in kwargs.keys():
            weight = kwargs['weight']
        else:
            if 'discriminance' in kwargs.keys():
                weight = discriminance_weight
            else:
                weight = exponential_weight

        if 'kernel' in kwargs.keys():
            kernel = kwargs['kernel']
        else:
            kernel = polynomial_kernel

        value = 0
        index = sorted([i, j])
        i, j = tuple(index)

        for index in self.matching_fost[(i, j)]:
            ni = self.fost[index].presence[i]
            nj = self.fost[index].presence[j]
            attributes = self.fost[index].attributes
            value += weight(attributes, *weight_param) * kernel(ni, nj, *kernel_param)
        return value

    def compute_kernel(self, sample_1, sample_2, **kwargs):
        """
        This function returns a matrix :math:`K(T_i,T_j)` for all :math:`(i,j)` given.

        :param sample_1: dict
            dictionary of class-origins. Origins will play the role of :math:`i`
        :param sample_2: dict
            dictionary of class-origins. Origins will play the role of :math:`j`

        :param **kwargs:

            * discriminance : boolean
                ``True`` if the kernel should be computed using discriminance. Default is ``False``.

            * normalized : boolean
                ``True`` if the kernel that is computed should be normalized. Default is ``False``.

            * weight : func
                The weight function to use in the FoST kernel

            * weight_param :
                see weight functions in support module

            * kernel : func
                The inner kernel function to use in the FoST kernel.

            * kernel_param :
                see kernel functions in support module

        Notes
        -----
        Compute, for all :math:`(i,j)`: :math:`K(T_i,T_j)=\\sum_{\\nu\in \mathcal{M}_{i,j}} \omega_\\nu \kappa(N_\\nu(T_i)N_\\nu(T_j))`


        where :math:`\mathcal{M}_{i,j}` is the (i,j) entry of ``matching_fost``;
        :math:`\omega_\\nu` is the ``weight`` function evaluated in node :math:`\\nu`; :math:`\kappa` is the ``kernel`` function; :math:`N_\\nu(T)` is the number of times :math:`\\nu` appears as a FoST object of :math:`T` (which is the ``presence`` attribute,  that appears in :math:`\mathcal{M}_{i,j}`)

        If the kernel is normalized, then :math:`K_\\text{norm.}(T_i,T_j)=\\frac{K(T_i,T_j)}{\sqrt{K(T_i,T_i)K(T_j,T_j)}}`

        Complete references will soon be available in an upcoming paper.
        """
        if not isinstance(sample_1, dict):
            sample_1 = {'fake': [sample_1]}
        if not isinstance(sample_2, dict):
            sample_2 = {'fake': [sample_2]}

        origins_1 = []
        for c in sample_1.keys():
            origins_1 += sample_1[c]

        origins_2 = []
        for c in sample_2.keys():
            origins_2 += sample_2[c]

        N = len(origins_1)
        M = len(origins_2)

        m = np.zeros((N, M))

        if 'normalized' in kwargs.keys():
            norm = kwargs['normalized']
        else:
            norm = False

        if norm:
            for i in range(N):
                for j in range(M):
                    a = self.__compute_single_kernel_value(origins_1[i], origins_2[j], **kwargs)
                    if a == 0:
                        m[i][j] = a
                    else:

                        b = self.__compute_single_kernel_value(origins_1[i], origins_1[i], **kwargs)
                        c = self.__compute_single_kernel_value(origins_2[j], origins_2[j], **kwargs)

                        if b * c == 0:
                            m[i][j] = float('Inf')
                        else:
                            m[i][j] = a / math.sqrt(b * c)

        else:
            for i in range(N):
                for j in range(M):
                    m[i][j] = self.__compute_single_kernel_value(origins_1[i], origins_2[j], **kwargs)

        return m

    def __assign_vector_of_discriminance(self, classes):

        origins = {}
        for c in classes.keys():
            if c != 'unknown':
                for o in classes[c]:
                    origins[o] = c

        for fost in self.fost:
            fost_origin = fost.origins

            vec = {}
            for c in classes.keys():
                if c != 'unknown':
                    vec[c] = 0

            for origin in fost_origin:
                if origin in origins:
                    vec[origins[origin]] += 1 / len(classes[origins[origin]])
            fost.add_attribute('vector of discriminance', vec)

    def assign_discriminance(self, classes):
        """

        Add to each FoST in the FoST list a new attribute, 'discriminance', equal to its
        distance of discrimination.

        Parameters
        ----------

        classes: dict
            the dictionary of classes - origins from which the discriminance will be computed

        Notes
        -----
        For each FoST, the discrimination of this FoST with regards to each
        different class, is the proportion of tree of this class with an origin in
        index_sample, that contain this FoST. Then, the distance of discrimination
        is the distance of this vector (of length number of different classes) to
        points of interest, which are given by (0,...,1,...,0) and (1,...,0,...,1).

        The 'discriminance' attribute is exactly equal to this distance of
        discrimination.

        Complete references will soon be available in an upcoming paper.

        """
        self.__assign_vector_of_discriminance(classes)

        list_of_points = _points_of_interest(set(classes.keys()))

        for fost in self.fost:
            vec = fost.get_attribute('vector of discriminance')
            val, pointmin = _dist_to_points(vec, list_of_points)

            if val > 1:
                val = 1

            val = 1 - val

            fost.add_attribute('discriminance', val)
            fost.add_attribute('discriminance - closest point of interest', pointmin)

# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.simulation.random_attributes
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: treex.tree, random

This module implements functions to create or modify random attributes

- File author(s): Romain Azais <romain.azais@inria.fr>
"""

from treex.tree import *

import random, math

def assign_gaussian_attribute_to_tree(t , attribute_name = 'gaussian_attribute' , mean = 0 , sd = 1 , dim = 1):
    """
    Give to each node in the tree a new attribute simulated according
    to the Gaussian distribution.

    Parameters
    ----------
    t: Tree
        a tree

    attribute_name: str
        name of the attribute. Default is set to 'gaussian_attribute'

    mean: float
        mean of the Gaussian attribute

    sd: float
        standard deviation of the Gaussian attribute. Must be positive

    dim: int
        dimension of the Gaussian attribute
    """

    if dim>1:
        value = [random.gauss(mean,sd) for i in range(dim)]
    else:
        value = random.gauss(mean,sd)
    t.add_attribute_to_id(attribute_name , value)

    for child in t.my_children:
        assign_gaussian_attribute_to_tree(child , attribute_name , mean , sd , dim)

def add_gaussian_noise_to_attribute(t , attribute_name , pos = None , sd = 1):
    """
    Add a Gaussian noise to an attribute existing in the tree.

    Parameters
    ----------
    tree: Tree
        a tree

    attribute_name: str
        name of the attribute. Must be in the list of attributes of the tree

    pos: int, optional
        if the attribute is a list, index of the component to which the node
        is to be added. If None, the noise is added to all the components of
        the attribute

    sd: float
        standard deviation of the noise

    """

    if attribute_name in t.get_attribute():
        
        value = t.get_attribute(attribute_name)
        
        if pos == None:
            if isinstance(value,list):
                for k in range(len(value)):
                    value[k] += random.gauss(0,sd)

            elif isinstance(value,int) or isinstance(value,float):
                value += random.gauss(0,sd)

        elif isinstance(value,list) and isinstance(pos,int):
            value[pos] += random.gauss(0,sd)

        t.add_attribute_to_id(attribute_name , value)

    for child in t.my_children:
        add_gaussian_noise_to_attribute(child , attribute_name , pos , sd)
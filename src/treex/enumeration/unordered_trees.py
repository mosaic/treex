# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.enumeration.unordered_trees
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#           
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

""" 
Called by this module : treex.tree, treex.coding_process.heightprocess.tree_to_height_process, random.choice

This module implements the algorithm proposed in [UN03] for exhaustive enumeration of unordered trees. If the core of the procedure comes from [UN03], the functions proposed here have been enriched with various parameters and functionalities. 

.. rubric:: References

.. [UN03] Uno, T. and Nakano, S. “Efficient Generation of Rooted Trees.” (2003).

- File author(s): Florian Ingels <florian.ingels@inria.fr>

"""

from treex.tree import *
from treex.converters.coding_process.heightprocess import tree_to_height_process

from random import choice


def __depth_sequence(t):
    return tree_to_height_process(t)['y']


def __copy_depth(t):
    rp = __rightmost_path(t)
    active = []
    for tree in rp:
        d = tree.get_property('depth',compute_level=1)
        if len(tree.my_children) > 1:
            r = __depth_sequence(tree.my_children[-1])
            s = __depth_sequence(tree.my_children[-2])
            if len(s) >= len(r):
                if all([r[i] == s[i] for i in range(len(r))]):
                    active.append(d)
    if len(active) == 0:
        return 0
    else:
        return min(active)


def __rightmost_path(t):
    l = [t]
    if len(t.my_children)==0:
        return l
    else:
        return l + __rightmost_path(t.my_children[-1])


def __preceding_child(t):
    if t.my_parent == None:
        return 'do not exist'
    else:
        p = t.my_parent
        if len(p.my_children) > 1:
            return p.my_children[-2]
        else:
            return 'do not exist'


def left_heavy_embedding(t):
    """
    Given a Tree, rearrange its inner structure so that its depth sequence is the heavier possible in the sense of [
    UN03]. As unordered trees, both trees are identical.
    """
    subtrees = t.subtrees_sorted_by('depth')
    for d in reversed(list(subtrees.keys())):
        for st in subtrees[d]:
            for stst in st.list_of_subtrees():
                stst.add_attribute_to_id('depth_sequence', __depth_sequence(stst))
            st.sort_subtrees_by_attribute('depth_sequence', reverse=True)


# ------------------------------------------------------------------------------

def __random_successor(t, cd):
    t.get_attribute()
    rp = __rightmost_path(t)
    r = rp[cd + 1]
    s = __preceding_child(r)
    if s == 'do not exist':
        # print('Case 3b\n')
        d = len(__rightmost_path(t))  # not trivial to retrieve from the paper

        i = choice(range(0, d))

        if i == d - 1:
            t.add_subtree_to_id(Tree(), rp[d - 1].my_id)
            return cd
        else:
            t.add_subtree_to_id(Tree(), rp[i].my_id)
            return i

    elif __depth_sequence(s) == __depth_sequence(r):
        # print('Case 2\n')
        i = choice(range(cd + 1))

        t.add_subtree_to_id(Tree(), rp[i].my_id)
        return i

    else:
        # print('Case 3\n)')
        c = len(__depth_sequence(r))
        d = [i + s.get_property('depth',compute_level=2) for i in __depth_sequence(s)][c]

        i = choice(range(0, d))

        if i == d - 1:
            t.add_subtree_to_id(Tree(), rp[d - 1].my_id)
            return cd
        else:
            t.add_subtree_to_id(Tree(), rp[i].my_id)
            return i


def tree_random_walk(nb=1, t=None):  # not very interesting, though
    """
    Perform a random trajectory on the enumeration tree defined in [UN03]. Starting from a given tree,
    we choose uniformly at random a successor, and repeat this operation a fixed number of time. The starting tree is
    modified at each step, and returned at the end.

    Parameters
    ----------
    nb: int,optional
        The number of steps to perform. Default is 1.

    t : Tree, optional If a tree is given, it will be the starting point of the trajectory. Note that this tree will
    be modified during the process. If no tree is given, default starting point will be the two-vertices tree.

    :rtype: Tree

    Notes
    -----
    Following the rules depicted in [UN03], if the starting point is the two-vertices tree, and if the
    first step adds a vertex as a child to the root (with probability 0.5), then all steps afterward will do the same,
    leading to a tree of height 1 with only leaves and a root. This function is then very likely to generates only such
    trees.

    """
    if t is None:
        t = Tree()
        t.add_subtree(Tree())
        cd = 0
    else:
        left_heavy_embedding(t)
        cd = __copy_depth(t)

    for i in range(nb):
        cd = __random_successor(t, cd)

    t.get_property('height',compute_level=2)
    return t

# ------------------------------------------------------------------------------


def __successors(t, cd=None):
    if cd is None:
        cd = __copy_depth(t)

    t.get_attribute()
    l = []
    rp = __rightmost_path(t)
    r = rp[cd + 1]
    s = __preceding_child(r)
    if s == 'do not exist':
        # print('Case 3b\n')
        d = len(__rightmost_path(t))  # not trivial to retrieve from the paper
        for i in range(d - 1):
            tp = t.copy()
            rpp = __rightmost_path(tp)
            tp.add_subtree_to_id(Tree(), rpp[i].my_id)
            l.append((tp, i))
        tp = t.copy()
        rpp = __rightmost_path(tp)
        tp.add_subtree_to_id(Tree(), rpp[d - 1].my_id)
        l.append((tp, cd))
    elif __depth_sequence(s) == __depth_sequence(r):
        # print('Case 2\n')
        for i in range(cd + 1):
            tp = t.copy()
            rpp = __rightmost_path(tp)
            tp.add_subtree_to_id(Tree(), rpp[i].my_id)
            l.append((tp, i))
    else:
        # print('Case 3\n)')
        c = len(__depth_sequence(r))
        d = [i + s.get_property('depth',compute_level=2) for i in __depth_sequence(s)][c]
        for i in range(d - 1):
            tp = t.copy()
            rpp = __rightmost_path(tp)
            tp.add_subtree_to_id(Tree(), rpp[i].my_id)
            l.append((tp, i))
        tp = t.copy()
        rpp = __rightmost_path(tp)
        tp.add_subtree_to_id(Tree(), rpp[d - 1].my_id)
        l.append((tp, cd))
    return l


def __check_constraints(t, **constraints):
    dic = t.get_property(compute_level=2)
    if 'size' in constraints.keys():
        if dic['size'] > constraints['size']:
            return False
    if 'height' in constraints.keys():
        if dic['height'] > constraints['height']:
            return False
    if 'outdegree' in constraints.keys():
        if dic['outdegree'] > constraints['outdegree']:
            return False
    return True


def tree_successors(t=None, nb=float('Inf'), **constraints):
    """
    Returns the list of all unordered trees that:

        * are a descendant (in the enumeration tree) in at most `nb` steps of the starting tree
        * satisfy the constraints given (if any).

    Parameters
    ----------
    t: Tree, optional
        If a tree is given, it will be the starting point of the succession (it won't be modified). If no tree is given, default starting point will be the two-vertices tree.

    nb : int, optional
        If given, successors will be investigated up to `nb` steps from the starting tree. Default is ``inf``.

    **constraints: optional
        Trees whose attributes exceeding at least one constraint given will be rejected, and their successors won't be investigated.

        * size: int

        * height: int

        * outdegree: int


    Returns
    -------
    A list of trees satisfying the constraints within at most `nb` steps from the starting tree. The starting tree is the first item of the list.


    Warnings
    --------
    As the enumeration tree is infinite, this function will behave like an endless loop if none of those conditions is fulfilled:

    * a finite parameter `nb` is passed

    * the constraint `size` is present

    * the constraints  `height` and `outdegree` are present

    """
    if t is None:
        t = Tree()
        t.add_subtree(Tree())
        cd = 0
    else:
        left_heavy_embedding(t)
        cd = __copy_depth(t)

    to_explore_now = [(t, cd)]
    to_explore_next = []
    to_return = []

    i = 0
    while i < nb + 1:
        i += 1
        while len(to_explore_now) > 0:
            t, cd = to_explore_now.pop()
            if __check_constraints(t, **constraints):
                to_return.append(t)
                to_explore_next += __successors(t, cd)
        if len(to_explore_next) == 0:
            break
        to_explore_now = to_explore_next
        to_explore_next = []

    return to_return

def tree_predecessor(t):
    """
    This function implements the reduction rule of [UN03].
    :param t: Tree
    :return: The parent of `t` in the enumeration tree defined in [UN03].
    """
    if len(t.my_children)==0:
        return None
    else:
        tp=t.copy()
        rp=__rightmost_path(tp)
        id=rp[-1].my_id
        tp.delete_subtree_at_id(id)
        return tp
# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.edit_distance.zhang
#
#       File author(s):
#           salah eddine habibeche <salah-eddine.habibeche@inria.fr>
#
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------


"""
	This example file shows how to compute the edit distance between two labeled trees
"""

from treex.simulation.random_recursive_trees import gen_random_tree
from treex.analysis.edit_distance.zhang_labeled_trees import zhang_edit_distance


def local_distance(v1=None,v2=None):
	if isinstance(v1,tuple)==True:
		if v2==None:
			cout=0
			v=list(v1)
			for i in range(len(v)):
				cout+=abs(v[i])
			return cout
		else:
			d=len(v1)
			return sum( [ abs(v1[i]-v2[i]) for i in range(0,d) ])  
	else: 
		if v2==None:
			return abs(v1)
		else:
			return abs(v1-v2)


def give_label_dist(tree,value):
	# associate attribute values ​​with the tree	
	# Parameters
	# ----------
	# tree: treex tree
	# value: number vector
	tree.add_attribute_to_id('label_for_distance',value)
	for child in tree.my_children:
		give_label_dist(child,value)

print('Example 1: fast calculation with cost function')

t1 = gen_random_tree(3)
t2 = gen_random_tree(1)
t1.get_attribute()
t1.treeprint(ptype = 'all attributes')
t2.get_attribute()
t2.treeprint(ptype = 'all attributes')
give_label_dist(t1,(3))
give_label_dist(t2,(2))

dist1 = zhang_edit_distance(t1,t2,'label_for_distance',local_distance,verbose=bool(False))

print('Distance between t1 and t2:')
print(dist1)


print('Example 2: detailed calculation with cost function')

dist2 = zhang_edit_distance(t1,t2,'label_for_distance',local_distance,verbose=bool(True))
print(dist2['distance'])

print(dist2['matrix_of_trees_distances'])

print('Example 3: without using cost function')


dist3 = zhang_edit_distance(t1,t2,label_attribute=None,local_distance=None,verbose=bool(False))
print("Distance that does not take account of the label for distance")
print(dist3)

=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* Romain Azais, <romain.azais@inria.fr> (until 07/2021)

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* Farah Ben Naoum <farahbennaoum@yahoo.fr>
* Guillaume Cerutti <guillaume.cerutti@inria.fr>
* Didier Gemmerle <didier.gemmerle@loria.fr>
* Salah Habibeche <habibechesalaheddine1@gmail.com>
* Benoit Henry <benoit.henry@univ-lorraine.fr>
* Florian Ingels <florian.ingels@inria.fr>

.. #}